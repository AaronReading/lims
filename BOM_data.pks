create or replace
PACKAGE BOM_data IS

/*********************************************************************
 * COPYRIGHT:   (c) Intellectual Property of the Dept of EandRM
 *
 * MODULE:	BOM_data
 *
 * CREATED:	24th September 2009
 *
 * PURPOSE:	Library Package containing all the scripts related to
 *          data provided to Bureau of Meteorology under the
 *          water act
 *
 *
 * COMMENTS:
 *
 * VERSION CONTROL:
 * Date        Author Description
 * ----------- ------ -------------------------------------------------
 * 24-SEP-2009  ARead  Created.
 * 15-JAN-2010  ARead  Started module for wdtfv1.0
 *
 *********************************************************************/
 
 /*******************************************************************
 * MODULE:	BOM_data
 * PURPOSE:	table definition used to store and transfer water level 
 *          and elevation data between procedures
 * RETURNS:	nothing
 * NOTES:   
 *********************************************************************/
 TYPE waterlvl_rec IS RECORD (
    rn             gw_wlvdets.rn%TYPE,
    pipe             gw_wlvdets.pipe%TYPE,
    rdate            gw_wlvdets.rdate%TYPE,
    meas_point       gw_wlvdets.meas_point%TYPE,
    measurement      gw_wlvdets.measurement%TYPE,
    remark           gw_wlvdets.remark%TYPE,
    logger          gw_wlvdets.logger%TYPE,
    edate           gw_elvdets.rdate%TYPE,
    elevation       gw_elvdets.elevation%TYPE,
    vdatum_id           VARCHAR2(40),
    gwlm_id             VARCHAR2(40),
    units               VARCHAR2(3),
    written           NUMBER(1));

  TYPE waterlvl_type IS TABLE OF
    waterlvl_rec
    INDEX BY PLS_INTEGER;
    
 /*******************************************************************
 * MODULE:	BOM_data
 * PURPOSE:	table definition used to store and transfer water quality 
 *          data between procedures
 * RETURNS:	nothing
 * NOTES:   
 *********************************************************************/
 TYPE waterqual_rec IS RECORD (
    rn                gw_watanls.rn%TYPE,
    pipe              gw_watanls.pipe%TYPE,
    rdate             gw_watanls.rdate%TYPE,
    field_cond        gw_fieldqs.conduct%TYPE,
    field_ph          gw_fieldqs.ph%TYPE,
    depth             gw_fieldqs.depth%TYPE,
    lab_cond          gw_watanls.conduct%TYPE,
    lab_ph            gw_watanls.ph%TYPE,
    lab_analysis      gw_watanls.analysis_no%TYPE,
    sample_id           VARCHAR2(40),
    bottle_id             VARCHAR2(40));

  TYPE waterqual_type IS TABLE OF
    waterqual_rec
    INDEX BY PLS_INTEGER;

/*********************************************************************
 * MODULE:	BOM_data
 * PURPOSE:	This procedure is the code for the initial data dump
 *		      BoM received.  Now get updates using BOM_data_update below
 * RETURNS:	nothing
 * NOTES:
 *********************************************************************/
PROCEDURE BOM_data;

/*********************************************************************
 * MODULE:	BOM_data_update
 * PURPOSE:	This procedure is the code for the incrimental data update
 *		      BoM receive.
 * RETURNS:	nothing
 * NOTES:   creates several csv files that need to be zipped and sent
 *          to BoM via their ftp site.
 *********************************************************************/
PROCEDURE BOM_data_update;

/*********************************************************************
 * MODULE:	create_header
 * PURPOSE:	This procedure is the code that creates the 8 header lines
 *		      that are part of each csv file sent to BoM
 * RETURNS:	nothing
 * NOTES:
 *********************************************************************/
PROCEDURE create_header (table_type IN VARCHAR2, f_write IN UTL_FILE.FILE_TYPE, udate IN  VARCHAR2);

/*********************************************************************
 * MODULE:	BOM_cond_data
 * PURPOSE:	This procedure is the code for the initial dump of
 *		      conductivity data BoM received.
 * RETURNS:	nothing
 * NOTES:
 *********************************************************************/
PROCEDURE BOM_cond_data;

/*********************************************************************
 * MODULE:	BOM_casing_data
 * PURPOSE:	This procedure is the code for the initial dump of
 *		      casing data BoM received.
 * RETURNS:	nothing
 * NOTES:
 *********************************************************************/
PROCEDURE BOM_casing_data;

/*********************************************************************
 * MODULE:	BOM_update_xml
 * PURPOSE:	This procedure is the code for the incrimental data update
 *		      BoM receive in their xml WDTFv1.0.
 * RETURNS:	nothing
 * NOTES:   creates several xml files that need to be zipped and sent
 *          to BoM via their ftp site.
 *********************************************************************/
PROCEDURE BOM_update_xml;

/*********************************************************************
 * MODULE:	xml_header
 * PURPOSE:	This procedure is the code that creates the header
 *		     information for WDTFv1.0.
 * RETURNS:	nothing
 * NOTES:       inserts the HydroCollection and metadata tags into the current
 *              xml file.
 *********************************************************************/
PROCEDURE xml_header(p_rn IN NUMBER, f_write IN UTL_FILE.FILE_TYPE, f_name IN VARCHAR2);

/*********************************************************************
 * MODULE:	xml_location
 * PURPOSE:	This procedure is the code that creates the location
 *		     information for WDTFv1.0.
 * RETURNS:	nothing
 * NOTES:       inserts the samplingGroup and SamplingPoint tags into the current
 *              xml file.
 *********************************************************************/
PROCEDURE xml_location(p_rn IN NUMBER, f_write IN UTL_FILE.FILE_TYPE, udate IN VARCHAR2);

/*********************************************************************
 * MODULE:	xml_waterlvls
 * PURPOSE:	This procedure is the code that creates the water level
 *		     information for WDTFv1.0.
 * RETURNS:	nothing
 * NOTES:       inserts the procedureMemver and observationMember tags into the current
 *              xml file.
 *********************************************************************/
PROCEDURE xml_waterlvls(p_rn IN NUMBER, f_write IN UTL_FILE.FILE_TYPE, udate IN DATE, p_datum_done IN OUT BOOLEAN);

/*********************************************************************
 * MODULE:	xml_casing
 * PURPOSE:	This procedure is the code that creates the casing
 *		     information for WDTFv1.0.
 * RETURNS:	nothing
 * NOTES:       inserts the wdtf:BorePipeSamplingInterval and gml:VerticalCRS tags
 *              into the current xml file.
 *********************************************************************/
PROCEDURE xml_casing(p_rn IN NUMBER, f_write IN UTL_FILE.FILE_TYPE, udate IN DATE, p_datum_done IN OUT BOOLEAN);

/*********************************************************************
 * MODULE:	xml_quality
 * PURPOSE:	This procedure is the code that creates the water quality
 *		     information for WDTFv1.0.
 * RETURNS:	nothing
 * NOTES:       inserts the wdtf:Measurement tag into the current xml file.
 *********************************************************************/
PROCEDURE xml_quality(p_rn IN NUMBER, f_write IN UTL_FILE.FILE_TYPE, udate IN DATE);

/*********************************************************************
 * MODULE:	xml_elevation
 * PURPOSE:	This procedure is the code that creates the elevation
 *		  information for casing and water levels in WDTFv1.0.
 * RETURNS:	nothing
 * NOTES:       inserts the gml:VerticalDatum tags into the current xml file.
 *********************************************************************/
PROCEDURE xml_elevation(p_rn IN NUMBER, f_write IN UTL_FILE.FILE_TYPE, udate IN DATE, p_datum_done IN OUT BOOLEAN);

/*********************************************************************
 * MODULE:	xml_update2
 * PURPOSE:	This procedure is the code for the incrimental data update
 *		      BoM receive in their xml WDTFv1.0.2.
 * RETURNS:	nothing
 * NOTES:   creates several xml files that need to be zipped and sent
 *          to BoM via their ftp site.
 *********************************************************************/
PROCEDURE xml_update2;

/*********************************************************************
 * MODULE:	xml_waterlvl_data
 * PURPOSE:	This procedure is the code that extracts the water level
 *		     information for WDTFv1.0.2
 * RETURNS:	a table of waterlvl_type that contains all relevant water
 *          levels and elevations
 * NOTES:   waterlvl_type is a temp table declared above
 *********************************************************************/
PROCEDURE xml_waterlvl_data(p_rn IN NUMBER, udate IN DATE, p_wlvl_table IN OUT waterlvl_type);

/*********************************************************************
 * MODULE:	xml_waterQuality_data
 * PURPOSE:	This procedure is the code that extracts the water quality
 *		     information for WDTFv1.0.2
 * RETURNS:	a table of waterq_type that contains all relevant water
 *          quality, conduct and pH
 * NOTES:   waterq_type is a temp table declared above
 *********************************************************************/
PROCEDURE xml_waterQuality_data(p_rn IN NUMBER, udate IN DATE, p_wq_table IN OUT waterqual_type);


/*********************************************************************
 * MODULE:	xml_definitionMember
 * PURPOSE:	This procedure writes the definitionMember xml code for 
 *		      the incrimental data update BoM receive in their xml WDTFv1.0.2.
 * RETURNS:	nothing
 * NOTES:   creates the definitionMember (elevation) tags for a specific
 *          RN.
 *********************************************************************/
--PROCEDURE xml_definitionMember(p_rn IN NUMBER, f_write IN UTL_FILE.FILE_TYPE, udate IN DATE);

END BOM_data;
/
show errors;

create or replace
PACKAGE BODY BOM_data IS

/*********************************************************************
 * MODULE:	BOM_data
 * PURPOSE:	This procedure reads the ASCII log file and inserts the
 *		log data into the gw_wirelog_head and gw_wirelog_data tables
 * RETURNS:	nothing
 * NOTES:
 *********************************************************************/
PROCEDURE BOM_data IS

  CURSOR c_regdets IS
    SELECT rn, gis_lat, gis_lng
    FROM gw_regdets
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE rn IN (
        SELECT rn
        FROM gw_pumtes
        WHERE swl > 0
      )
      AND facility_type <> 'SF'
    UNION
      SELECT DISTINCT rn
      FROM gw_elvdets
      WHERE rn IN (
        SELECT rn
        FROM gw_wlvdets
      )
    )
    AND gis_lat IS NOT NULL
    ORDER BY rn;

  CURSOR c_wlvdets IS
    SELECT rn, pipe, rdate, meas_point, measurement, remark, logger
    FROM gw_wlvdets
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND rn IN (
        SELECT rn
        FROM gw_elvdets
      )
    )
    ORDER BY rn, pipe, rdate;

  CURSOR c_elvdets IS
    SELECT rn, pipe, rdate, meas_point, precision, datum, elevation, survey_source
    FROM gw_elvdets
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND rn IN (
        SELECT rn
        FROM gw_wlvdets
      )
    )
    ORDER BY rn, pipe, rdate;

  CURSOR c_pumtes IS
    SELECT rn, pipe, rdate, zone_top, zone_bottom, swl
    FROM gw_pumtes
    WHERE swl > 0
    AND rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND facility_type <> 'SF'
    )
    ORDER BY rn, pipe, rdate;

  CURSOR c_aquifrs IS
    SELECT rn, rec, top, bottom, contr, form_desc
    FROM gw_aquifrs
    WHERE rn IN (
      SELECT rn
      FROM gw_pumtes
      WHERE swl > 0
      AND rn IN (
        SELECT rn
        FROM gw_regdets
        WHERE gis_lat IS NOT NULL
      	AND facility_type <> 'SF'
      )
    )
    ORDER BY rn, rec;

  v_index NUMBER;
  f_dir  VARCHAR2(20) := 'D:\GW_LOAD';
  f_name	VARCHAR2(40);
  f_w_ref UTL_FILE.FILE_TYPE;

  PROCEDURE create_header (
                    table_type  IN  VARCHAR2,
                    f_write IN  UTL_FILE.FILE_TYPE
                          ) AS
    v_record_count  NUMBER;
  BEGIN
    UTL_FILE.PUT_LINE(f_write, '# HEADER: Agency Id: w00066');
    UTL_FILE.PUT_LINE(f_write, '# HEADER: File Generation Date: '||TO_CHAR(SYSDATE, 'YYYY-MM-DD')||'T'||TO_CHAR(SYSDATE, 'HH24:MI:SS'));
    UTL_FILE.PUT_LINE(f_write, '# HEADER: File Format: csv');
    UTL_FILE.PUT_LINE(f_write, '# HEADER: File Format Version: 1');
    UTL_FILE.PUT_LINE(f_write, '# HEADER: Generated by (system): Oracle');

    --number of lines and data fields depends on the table_type
    CASE table_type
      WHEN 'R' THEN
        SELECT COUNT(rn)
        INTO v_record_count
        FROM gw_regdets
        WHERE rn IN (
          SELECT rn
          FROM gw_regdets
          WHERE rn IN (
            SELECT rn
            FROM gw_pumtes
            WHERE swl > 0
          )
          AND facility_type <> 'SF'
        UNION
          SELECT DISTINCT rn
          FROM gw_elvdets
          WHERE rn IN (
            SELECT rn
            FROM gw_wlvdets
          )
        )
        AND gis_lat IS NOT NULL;

        UTL_FILE.PUT_LINE(f_write, '# HEADER: Number of Records: '||v_record_count);
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Local ObsTime Offset: 10:00');
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Data Fields: IndexNo, RegisteredNo, Latitude, Longitude');
      WHEN 'W' THEN
        SELECT COUNT(rn)
        INTO v_record_count
        FROM gw_wlvdets
        WHERE rn IN (
          SELECT rn
          FROM gw_regdets
          WHERE gis_lat IS NOT NULL
          AND rn IN (
            SELECT rn
            FROM gw_elvdets
          )
        );

        UTL_FILE.PUT_LINE(f_write, '# HEADER: Number of Records: '||v_record_count);
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Local ObsTime Offset: 10:00');
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Data Fields: IndexNo, RegisteredNo, Pipe, MeasureDate, MeasurePoint, Measurement, Remark, Logger');
      WHEN 'E' THEN
        SELECT COUNT(rn)
        INTO v_record_count
        FROM gw_elvdets
        WHERE rn IN (
          SELECT rn
          FROM gw_regdets
          WHERE gis_lat IS NOT NULL
          AND rn IN (
            SELECT rn
            FROM gw_wlvdets
          )
        );

        UTL_FILE.PUT_LINE(f_write, '# HEADER: Number of Records: '||v_record_count);
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Local ObsTime Offset: 10:00');
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Data Fields: IndexNo, RegisteredNo,  Pipe, ElevationDate, MeasurePoint, Precision, Datum, Elevation, SurveySource');
      WHEN 'P' THEN
        SELECT COUNT(rn)
        INTO v_record_count
        FROM gw_pumtes
        WHERE swl > 0
        AND rn IN (
          SELECT rn
          FROM gw_regdets
          WHERE gis_lat IS NOT NULL
          AND facility_type <> 'SF'
        );

        UTL_FILE.PUT_LINE(f_write, '# HEADER: Number of Records: '||v_record_count);
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Local ObsTime Offset: 10:00');
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Data Fields: IndexNo, RegisteredNo, Pipe, PumpTestDate, TestedZoneTop, TestedZoneBottom, StaticHead');
      WHEN 'A' THEN
        SELECT COUNT(rn)
        INTO v_record_count
        FROM gw_aquifrs
        WHERE rn IN (
          SELECT rn
          FROM gw_pumtes
          WHERE swl > 0
          AND rn IN (
            SELECT rn
            FROM gw_regdets
            WHERE gis_lat IS NOT NULL
            AND facility_type <> 'SF'
          )
        );

        UTL_FILE.PUT_LINE(f_write, '# HEADER: Number of Records: '||v_record_count);
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Local ObsTime Offset: 10:00');
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Data Fields: IndexNo, RegisteredNo, Record, AquiferTop, AquiferBottom, ContributingAquifer, FormationName');
    END CASE;
  END;

BEGIN
  -- table to be exported: gw_regdets
  -- open the file
  f_name := 'w00066_'||TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')||'_Locations.csv';
  f_w_ref := UTL_FILE.FOPEN (f_dir, f_name, 'w');

  --reset the index
  v_index := 1;
  -- start inputting header data
  create_header('R', f_w_ref);

  -- grab data
  FOR r_reg IN c_regdets LOOP
    -- write to file
    UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_reg.rn||','||r_reg.gis_lat||','||r_reg.gis_lng);
    v_index := v_index + 1;
  END LOOP;

  UTL_FILE.FCLOSE(f_w_ref);

  --repeat for table: gw_wlvdets
  -- open the file
  f_name := 'w00066_'||TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')||'_WaterLevels.csv';
  f_w_ref := UTL_FILE.FOPEN (f_dir, f_name, 'w');

  --reset the index
  v_index := 1;

  -- start inputting header data
  create_header('W', f_w_ref);

  -- grab data
  FOR r_wlvl IN c_wlvdets LOOP
    -- write to file
    UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_wlvl.rn||',"'||r_wlvl.pipe||'",'||TO_CHAR(r_wlvl.rdate, 'YYYY-MM-DD')||',"'||r_wlvl.meas_point||'",'||r_wlvl.measurement||',"'||r_wlvl.remark||'","'||r_wlvl.logger||'"');
    v_index := v_index + 1;
  END LOOP;

  UTL_FILE.FCLOSE(f_w_ref);

  --repeat for table: gw_elvdets
  -- open the file
  f_name := 'w00066_'||TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')||'_Elevations.csv';
  f_w_ref := UTL_FILE.FOPEN (f_dir, f_name, 'w');

  --reset the index
  v_index := 1;

  -- start inputting header data
  create_header('E', f_w_ref);

  -- grab data
  FOR r_elev IN c_elvdets LOOP
    -- write to file
    UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_elev.rn||',"'||r_elev.pipe||'",'||TO_CHAR(r_elev.rdate, 'YYYY-MM-DD')||',"'||r_elev.meas_point||'","'||r_elev.precision||'","'||r_elev.datum||'",'||r_elev.elevation||',"'||r_elev.survey_source||'"');
    v_index := v_index + 1;
  END LOOP;

  UTL_FILE.FCLOSE(f_w_ref);

  --repeat for table: gw_pumtes
  -- open the file
  f_name := 'w00066_'||TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')||'_ArtesianPump.csv';
  f_w_ref := UTL_FILE.FOPEN (f_dir, f_name, 'w');

  --reset the index
  v_index := 1;

  -- start inputting header data
  create_header('P', f_w_ref);

  -- grab data
  FOR r_pump IN c_pumtes LOOP
    -- write to file
    UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_pump.rn||',"'||r_pump.pipe||'",'||TO_CHAR(r_pump.rdate, 'YYYY-MM-DD')||','||r_pump.zone_top||','||r_pump.zone_bottom||','||r_pump.swl);
    v_index := v_index + 1;
  END LOOP;

  UTL_FILE.FCLOSE(f_w_ref);

  --repeat for table: gw_aquifrs
  -- open the file
  f_name := 'w00066_'||TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')||'_Aquifers.csv';
  f_w_ref := UTL_FILE.FOPEN (f_dir, f_name, 'w');

  --reset the index
  v_index := 1;

  -- start inputting header data
  create_header('A', f_w_ref);

  -- grab data
  FOR r_aqua IN c_aquifrs LOOP
    -- write to file
    UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_aqua.rn||','||r_aqua.rec||','||r_aqua.top||','||r_aqua.bottom||',"'||r_aqua.contr||'","'||r_aqua.form_desc||'"');
    v_index := v_index + 1;
  END LOOP;

  UTL_FILE.FCLOSE(f_w_ref);

EXCEPTION
   WHEN UTL_FILE.INVALID_PATH
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_path'); RAISE;

   WHEN UTL_FILE.INVALID_MODE
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_mode'); RAISE;

   WHEN UTL_FILE.INVALID_FILEHANDLE
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_filehandle'); RAISE;

   WHEN UTL_FILE.INVALID_OPERATION
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_operation'); RAISE;

   WHEN UTL_FILE.READ_ERROR
   THEN
       DBMS_OUTPUT.PUT_LINE ('read_error'); RAISE;

   WHEN UTL_FILE.WRITE_ERROR
   THEN
      DBMS_OUTPUT.PUT_LINE ('write_error'); RAISE;

   WHEN UTL_FILE.INTERNAL_ERROR
   THEN
      DBMS_OUTPUT.PUT_LINE ('internal_error'); RAISE;

END BOM_data;

/*********************************************************************
 * MODULE:	BOM_data_update
 * PURPOSE:	This procedure is the code for the incrimental data update
 *		      BoM receive.
 * RETURNS:	nothing
 * NOTES:   creates several csv files that need to be zipped and sent
 *          to BoM via their ftp site.
 *********************************************************************/
PROCEDURE BOM_data_update IS

  CURSOR c_regdets (udate DATE) IS
    SELECT rn, gis_lat, gis_lng
    FROM gw_regdets
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE rn IN (
        SELECT rn
        FROM gw_pumtes
        WHERE swl > 0
      )
      AND facility_type <> 'SF'
    UNION
      SELECT DISTINCT rn
      FROM gw_elvdets
      WHERE rn IN (
        SELECT rn
        FROM gw_wlvdets
      )
    )
    AND gis_lat IS NOT NULL
    AND created_date > udate
    ORDER BY rn;

  CURSOR c_wlvdets (udate DATE) IS
    SELECT rn, pipe, rdate, meas_point, measurement, remark, logger
    FROM gw_wlvdets
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND rn IN (
        SELECT rn
        FROM gw_elvdets
      )
    )
    AND updated_date > udate
    ORDER BY rn, pipe, rdate;

  CURSOR c_elvdets (udate DATE) IS
    SELECT rn, pipe, rdate, meas_point, precision, datum, elevation, survey_source
    FROM gw_elvdets
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND rn IN (
        SELECT rn
        FROM gw_wlvdets
      )
    )
    AND updated_date > udate
    ORDER BY rn, pipe, rdate;

  CURSOR c_pumtes (udate DATE) IS
    SELECT rn, pipe, rdate, zone_top, zone_bottom, swl
    FROM gw_pumtes
    WHERE swl > 0
    AND rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND facility_type <> 'SF'
    )
    AND updated_date > udate
    ORDER BY rn, pipe, rdate;

  CURSOR c_aquifrs (udate DATE) IS
    SELECT rn, rec, top, bottom, contr, form_desc
    FROM gw_aquifrs
    WHERE rn IN (
      SELECT rn
      FROM gw_pumtes
      WHERE swl > 0
      AND rn IN (
        SELECT rn
        FROM gw_regdets
        WHERE gis_lat IS NOT NULL
      	AND facility_type <> 'SF'
      )
    )
    AND updated_date > udate
    ORDER BY rn, rec;

  CURSOR c_field (udate DATE) IS
    SELECT rn, pipe, rdate, conduct
    FROM gw_fieldqs
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND rn IN (
        SELECT rn
        FROM gw_wlvdets
      )
    )
    AND conduct IS NOT NULL
    AND updated_date > udate
    ORDER BY rn,pipe,rdate;


  CURSOR c_analys (udate DATE) IS
    SELECT rn, pipe, rdate, conduct
    FROM gw_watanls
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND rn IN (
        SELECT rn
        FROM gw_wlvdets
      )
    )
    AND conduct IS NOT NULL
    AND updated_date > udate
    ORDER BY rn,pipe,rdate;

  CURSOR c_casing (udate DATE) IS
    SELECT rn, pipe, rdate, material_desc, material_size, size_desc, top, bottom
    FROM gw_casings
    WHERE rn in (
        SELECT DISTINCT rn
        FROM gw_casings
        WHERE rn IN (
            SELECT rn
            FROM gw_regdets
            WHERE gis_lat IS NOT NULL)
        AND rn IN (
            SELECT rn
            FROM gw_wlvdets
        )
    )
    AND material_desc IN ('PERF','SCRN')
    AND updated_date > udate
    ORDER BY rn,pipe,rdate;

  TYPE reg_type IS RECORD (
    RN          gw_regdets.rn%TYPE,
    GIS_LAT     gw_regdets.gis_lat%TYPE,
    GIS_LNG     gw_regdets.gis_lng%TYPE
  );
  r_reg reg_type;

  TYPE wl_type IS RECORD (
    RN          gw_wlvdets.rn%TYPE,
    PIPE        gw_wlvdets.pipe%TYPE,
    RDATE       gw_wlvdets.rdate%TYPE,
    MEAS_POINT  gw_wlvdets.meas_point%TYPE,
    MEASUREMENT gw_wlvdets.measurement%TYPE,
    REMARK      gw_wlvdets.remark%TYPE,
    LOGGER      gw_wlvdets.logger%TYPE
  );
  r_wlvl wl_type;

  TYPE el_type IS RECORD (
    RN              gw_elvdets.rn%TYPE,
    PIPE            gw_elvdets.pipe%TYPE,
    RDATE           gw_elvdets.rdate%TYPE,
    MEAS_POINT      gw_elvdets.meas_point%TYPE,
    PRECISION       gw_elvdets.precision%TYPE,
    DATUM           gw_elvdets.datum%TYPE,
    ELEVATION       gw_elvdets.elevation%TYPE,
    SURVEY_SOURCE   gw_elvdets.survey_source%TYPE
  );
  r_elev el_type;

  TYPE pt_type IS RECORD (
    RN            gw_pumtes.rn%TYPE,
    PIPE          gw_pumtes.pipe%TYPE,
    RDATE         gw_pumtes.rdate%TYPE,
    ZONE_TOP      gw_pumtes.zone_top%TYPE,
    ZONE_BOTTOM   gw_pumtes.zone_bottom%TYPE,
    SWL           gw_pumtes.swl%TYPE
  );
  r_pump pt_type;

  TYPE aq_type IS RECORD (
    RN          gw_aquifrs.rn%TYPE,
    REC         gw_aquifrs.rec%TYPE,
    TOP         gw_aquifrs.top%TYPE,
    BOTTOM      gw_aquifrs.bottom%TYPE,
    CONTR       gw_aquifrs.contr%TYPE,
    FORM_DESC   gw_aquifrs.form_desc%TYPE
  );
  r_aqua aq_type;

  TYPE fi_type IS RECORD (
    RN          gw_fieldqs.rn%TYPE,
    PIPE        gw_fieldqs.pipe%TYPE,
    RDATE       gw_fieldqs.rdate%TYPE,
    CONDUCT     gw_fieldqs.conduct%TYPE
  );
  r_field fi_type;

  TYPE wa_type IS RECORD (
    RN          gw_watanls.rn%TYPE,
    PIPE        gw_watanls.pipe%TYPE,
    RDATE       gw_watanls.rdate%TYPE,
    CONDUCT     gw_watanls.conduct%TYPE
  );
  r_lab wa_type;

  TYPE ca_type IS RECORD (
    RN              gw_casings.rn%TYPE,
    PIPE            gw_casings.pipe%TYPE,
    RDATE           gw_casings.rdate%TYPE,
    MATERIAL_DESC   gw_casings.material_desc%TYPE,
    MATERIAL_SIZE   gw_casings.material_size%TYPE,
    SIZE_DESC       gw_casings.size_desc%TYPE,
    TOP             gw_casings.top%TYPE,
    BOTTOM          gw_casings.bottom%TYPE
  );
  r_case ca_type;


  v_index NUMBER;
  v_update_date   VARCHAR2(10);
  v_record_count1  NUMBER;
  v_record_count2  NUMBER;
  v_field_count  NUMBER;
  v_lab_count  NUMBER;
  f_dir  VARCHAR2(20) := 'D:\GW_LOAD';
  f_name	VARCHAR2(40);
  f_w_ref UTL_FILE.FILE_TYPE;

BEGIN

  --get the date of the last update from gw_system_variables
  SELECT parameter_value
  INTO v_update_date
  FROM gw_system_variables
  WHERE parameter_code = 'BOM_DATA_UPDATE';
  -- table to be exported: gw_regdets
  -- open the file
  f_name := 'w00066_'||TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')||'_Locations.csv';
  f_w_ref := UTL_FILE.FOPEN (f_dir, f_name, 'w');

  --reset the index
  v_index := 1;
  -- start inputting header data
  create_header('R', f_w_ref, v_update_date);

  -- grab data
  --FOR r_reg IN c_regdets LOOP
  OPEN c_regdets (TO_DATE(v_update_date,'DD/MM/YYYY'));
  FETCH c_regdets INTO r_reg.rn, r_reg.gis_lat, r_reg.gis_lng;
  LOOP
    EXIT WHEN c_regdets%NOTFOUND;
    -- write to file
    UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_reg.rn||','||r_reg.gis_lat||','||r_reg.gis_lng);
    v_index := v_index + 1;
    FETCH c_regdets INTO r_reg.rn, r_reg.gis_lat, r_reg.gis_lng;
  END LOOP;
  CLOSE c_regdets;
  UTL_FILE.FCLOSE(f_w_ref);

  --repeat for table: gw_wlvdets
  -- open the file
  f_name := 'w00066_'||TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')||'_WaterLevels.csv';
  f_w_ref := UTL_FILE.FOPEN (f_dir, f_name, 'w');

  --reset the index
  v_index := 1;

  -- start inputting header data
  create_header('W', f_w_ref, v_update_date);

  -- grab data
  --FOR r_wlvl IN c_wlvdets LOOP
  OPEN c_wlvdets (TO_DATE(v_update_date,'DD/MM/YYYY'));
  FETCH c_wlvdets INTO r_wlvl.rn, r_wlvl.pipe,r_wlvl.rdate,r_wlvl.meas_point,r_wlvl.measurement,r_wlvl.remark,r_wlvl.logger;
  LOOP
    EXIT WHEN c_wlvdets%NOTFOUND;
    -- write to file
    UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_wlvl.rn||',"'||r_wlvl.pipe||'",'||TO_CHAR(r_wlvl.rdate, 'YYYY-MM-DD')||',"'||r_wlvl.meas_point||'",'||r_wlvl.measurement||',"'||r_wlvl.remark||'","'||r_wlvl.logger||'"');
    v_index := v_index + 1;
    FETCH c_wlvdets INTO r_wlvl.rn, r_wlvl.pipe,r_wlvl.rdate,r_wlvl.meas_point,r_wlvl.measurement,r_wlvl.remark,r_wlvl.logger;
  END LOOP;
  CLOSE c_wlvdets;
  UTL_FILE.FCLOSE(f_w_ref);

  --repeat for table: gw_elvdets
  -- open the file
  f_name := 'w00066_'||TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')||'_Elevations.csv';
  f_w_ref := UTL_FILE.FOPEN (f_dir, f_name, 'w');

  --reset the index
  v_index := 1;

  -- start inputting header data
  create_header('E', f_w_ref, v_update_date);

  -- grab data
  --FOR r_elev IN c_elvdets LOOP
  OPEN c_elvdets (TO_DATE(v_update_date,'DD/MM/YYYY'));
  FETCH c_elvdets INTO r_elev.rn,r_elev.pipe,r_elev.rdate,r_elev.meas_point,r_elev.precision,r_elev.datum,r_elev.elevation,r_elev.survey_source;
  LOOP
    EXIT WHEN c_elvdets%NOTFOUND;
    -- write to file
    UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_elev.rn||',"'||r_elev.pipe||'",'||TO_CHAR(r_elev.rdate, 'YYYY-MM-DD')||',"'||r_elev.meas_point||'","'||r_elev.precision||'","'||r_elev.datum||'",'||r_elev.elevation||',"'||r_elev.survey_source||'"');
    v_index := v_index + 1;
  FETCH c_elvdets INTO r_elev.rn,r_elev.pipe,r_elev.rdate,r_elev.meas_point,r_elev.precision,r_elev.datum,r_elev.elevation,r_elev.survey_source;
  END LOOP;
  CLOSE c_elvdets;
  UTL_FILE.FCLOSE(f_w_ref);

  --repeat for table: gw_pumtes
  -- open the file
  f_name := 'w00066_'||TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')||'_ArtesianPump.csv';
  f_w_ref := UTL_FILE.FOPEN (f_dir, f_name, 'w');

  --reset the index
  v_index := 1;

  -- start inputting header data
  create_header('P', f_w_ref, v_update_date);

  -- grab data
  --FOR r_pump IN c_pumtes LOOP
  OPEN c_pumtes (TO_DATE(v_update_date,'DD/MM/YYYY'));
  FETCH c_pumtes INTO r_pump.rn,r_pump.pipe,r_pump.rdate,r_pump.zone_top,r_pump.zone_bottom,r_pump.swl;
  LOOP
    EXIT WHEN c_pumtes%NOTFOUND;
    -- write to file
    UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_pump.rn||',"'||r_pump.pipe||'",'||TO_CHAR(r_pump.rdate, 'YYYY-MM-DD')||','||r_pump.zone_top||','||r_pump.zone_bottom||','||r_pump.swl);
    v_index := v_index + 1;
  FETCH c_pumtes INTO r_pump.rn,r_pump.pipe,r_pump.rdate,r_pump.zone_top,r_pump.zone_bottom,r_pump.swl;
  END LOOP;
  CLOSE c_pumtes;
  UTL_FILE.FCLOSE(f_w_ref);

  --repeat for table: gw_aquifrs
  -- open the file
  f_name := 'w00066_'||TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')||'_Aquifers.csv';
  f_w_ref := UTL_FILE.FOPEN (f_dir, f_name, 'w');

  --reset the index
  v_index := 1;

  -- start inputting header data
  create_header('A', f_w_ref, v_update_date);

  -- grab data
  --FOR r_aqua IN c_aquifrs LOOP
  OPEN c_aquifrs (TO_DATE(v_update_date,'DD/MM/YYYY'));
  FETCH c_aquifrs INTO r_aqua.rn,r_aqua.rec,r_aqua.top,r_aqua.bottom,r_aqua.contr,r_aqua.form_desc;
  LOOP
    EXIT WHEN c_aquifrs%NOTFOUND;
    -- write to file
    UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_aqua.rn||','||r_aqua.rec||','||r_aqua.top||','||r_aqua.bottom||',"'||r_aqua.contr||'","'||r_aqua.form_desc||'"');
    v_index := v_index + 1;
  FETCH c_aquifrs INTO r_aqua.rn,r_aqua.rec,r_aqua.top,r_aqua.bottom,r_aqua.contr,r_aqua.form_desc;
  END LOOP;
  CLOSE c_aquifrs;
  UTL_FILE.FCLOSE(f_w_ref);



  -- repeat for conductivity data
  -- open the file
  f_name := 'w00066_'||TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')||'_Quality.csv';
  f_w_ref := UTL_FILE.FOPEN (f_dir, f_name, 'w');

  --reset the index
  v_index := 1;
  v_field_count := 0;
  v_lab_count := 0;

  -- start inputting header data
  create_header('O', f_w_ref, v_update_date);

  -- grab data
  SELECT count(rn)
  INTO v_record_count1
  FROM gw_fieldqs
  WHERE rn IN (
    SELECT rn
    FROM gw_regdets
    WHERE gis_lat IS NOT NULL
    AND rn IN (
      SELECT rn
      FROM gw_wlvdets
    )
  )
  AND conduct IS NOT NULL
  AND updated_date > TO_DATE(v_update_date,'DD/MM/YYYY');

  SELECT count(rn)
  INTO v_record_count2
  FROM gw_watanls
  WHERE rn IN (
    SELECT rn
    FROM gw_regdets
    WHERE gis_lat IS NOT NULL
    AND rn IN (
      SELECT rn
      FROM gw_wlvdets
    )
  )
  AND conduct IS NOT NULL
  AND updated_date > TO_DATE(v_update_date,'DD/MM/YYYY');

  OPEN c_field (TO_DATE(v_update_date,'DD/MM/YYYY'));
  FETCH c_field INTO r_field.rn, r_field.pipe, r_field.rdate, r_field.conduct;

  OPEN c_analys (TO_DATE(v_update_date,'DD/MM/YYYY'));
  FETCH c_analys INTO r_lab.rn, r_lab.pipe, r_lab.rdate, r_lab.conduct;

  LOOP
    EXIT WHEN v_field_count = v_record_count1 AND v_lab_count = v_record_count2;

    IF v_field_count = v_record_count1 THEN
      r_field.rn := 999999999;
    END IF;
    IF v_lab_count = v_record_count2 THEN
      r_lab.rn := 999999999;
    END IF;

    --if rns are equal
    IF r_field.rn = r_lab.rn THEN

      --if pipes are equal
      IF r_field.pipe = r_lab.pipe THEN

        --if dates are equal
        IF r_field.rdate = r_lab.rdate THEN

          -- write to file
          UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_field.rn||',"'||r_field.pipe||'",'||TO_CHAR(r_field.rdate, 'YYYY-MM-DD')||','||r_field.conduct||',"F"');
          v_index := v_index + 1;
          v_field_count := v_field_count + 1;
          FETCH c_field INTO r_field.rn, r_field.pipe, r_field.rdate, r_field.conduct;

        --else if field.date is lower
        ELSIF r_field.rdate < r_lab.rdate THEN

          -- write to file
          UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_field.rn||',"'||r_field.pipe||'",'||TO_CHAR(r_field.rdate, 'YYYY-MM-DD')||','||r_field.conduct||',"F"');
          v_index := v_index + 1;
          v_field_count := v_field_count + 1;
          FETCH c_field INTO r_field.rn, r_field.pipe, r_field.rdate, r_field.conduct;


        ELSE      --else analys.date is lower
          -- write to file
          UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_lab.rn||',"'||r_lab.pipe||'",'||TO_CHAR(r_lab.rdate, 'YYYY-MM-DD')||','||r_lab.conduct||',"L"');
          v_index := v_index + 1;
          v_lab_count := v_lab_count + 1;
          FETCH c_analys INTO r_lab.rn, r_lab.pipe, r_lab.rdate, r_lab.conduct;

        END IF;

      ELSIF r_field.pipe < r_lab.pipe THEN    --elsif field.pipe is lower

        -- write to file
        UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_field.rn||',"'||r_field.pipe||'",'||TO_CHAR(r_field.rdate, 'YYYY-MM-DD')||','||r_field.conduct||',"F"');
        v_index := v_index + 1;
        v_field_count := v_field_count + 1;
        FETCH c_field INTO r_field.rn, r_field.pipe, r_field.rdate, r_field.conduct;


      ELSE          --else analys.pipe is lower

        -- write to file
        UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_lab.rn||',"'||r_lab.pipe||'",'||TO_CHAR(r_lab.rdate, 'YYYY-MM-DD')||','||r_lab.conduct||',"L"');
        v_index := v_index + 1;
        v_lab_count := v_lab_count + 1;
        FETCH c_analys INTO r_lab.rn, r_lab.pipe, r_lab.rdate, r_lab.conduct;

      END IF;

    ELSIF r_field.rn < r_lab.rn THEN      --else if field lower

      -- write to file
      UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_field.rn||',"'||r_field.pipe||'",'||TO_CHAR(r_field.rdate, 'YYYY-MM-DD')||','||r_field.conduct||',"F"');
      v_index := v_index + 1;
      v_field_count := v_field_count + 1;
      FETCH c_field INTO r_field.rn, r_field.pipe, r_field.rdate, r_field.conduct;

    ELSE            --else analys is lower

      -- write to file
      UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_lab.rn||',"'||r_lab.pipe||'",'||TO_CHAR(r_lab.rdate, 'YYYY-MM-DD')||','||r_lab.conduct||',"L"');
      v_index := v_index + 1;
      v_lab_count := v_lab_count + 1;
      FETCH c_analys INTO r_lab.rn, r_lab.pipe, r_lab.rdate, r_lab.conduct;

    END IF;

  END LOOP;
  CLOSE c_analys;
  CLOSE c_field;
  UTL_FILE.FCLOSE(f_w_ref);

  -- repeat for casing data
  -- open the file
  f_name := 'w00066_'||TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')||'_Casing.csv';
  f_w_ref := UTL_FILE.FOPEN (f_dir, f_name, 'w');

  --reset the index
  v_index := 1;

  -- start inputting header data
  create_header('C', f_w_ref, v_update_date);

  -- grab data
  OPEN c_casing (TO_DATE(v_update_date,'DD/MM/YYYY'));
  FETCH c_casing INTO r_case.rn,r_case.pipe,r_case.rdate,r_case.material_desc,r_case.material_size,r_case.size_desc,r_case.top,r_case.bottom;
  LOOP
    EXIT WHEN c_casing%NOTFOUND;
    -- write to file
    UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_case.rn||',"'||r_case.pipe||'",'||TO_CHAR(r_case.rdate, 'YYYY-MM-DD')||',"'||r_case.material_desc||'","'||r_case.size_desc||'",'||r_case.material_size||','||r_case.top||','||r_case.bottom);
    v_index := v_index + 1;
  FETCH c_casing INTO r_case.rn,r_case.pipe,r_case.rdate,r_case.material_desc,r_case.material_size,r_case.size_desc,r_case.top,r_case.bottom;
  END LOOP;
  CLOSE c_casing;
  UTL_FILE.FCLOSE(f_w_ref);

  --update the date to todays date for next update
  UPDATE gw_system_variables
  SET parameter_value = TO_CHAR(SYSDATE, 'DD/MM/YYYY')
  WHERE parameter_code = 'BOM_DATA_UPDATE';

  commit;

EXCEPTION
   WHEN UTL_FILE.INVALID_PATH
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_path'); RAISE;

   WHEN UTL_FILE.INVALID_MODE
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_mode'); RAISE;

   WHEN UTL_FILE.INVALID_FILEHANDLE
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_filehandle'); RAISE;

   WHEN UTL_FILE.INVALID_OPERATION
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_operation'); RAISE;

   WHEN UTL_FILE.READ_ERROR
   THEN
       DBMS_OUTPUT.PUT_LINE ('read_error'); RAISE;

   WHEN UTL_FILE.WRITE_ERROR
   THEN
      DBMS_OUTPUT.PUT_LINE ('write_error'); RAISE;

   WHEN UTL_FILE.INTERNAL_ERROR
   THEN
      DBMS_OUTPUT.PUT_LINE ('internal_error'); RAISE;

END BOM_data_update;

/*********************************************************************
 * MODULE:	create_header
 * PURPOSE:	This procedure is the code that creates the 8 header lines
 *		      that are part of each csv file sent to BoM
 * RETURNS:	nothing
 * NOTES:
 *********************************************************************/
PROCEDURE create_header (table_type  IN  VARCHAR2,
			f_write IN  UTL_FILE.FILE_TYPE,
			udate   IN  VARCHAR2) IS
    v_record_count  NUMBER;
    v_record_count2  NUMBER;
    cdate            DATE := TO_DATE(udate, 'DD/MM/YYYY');
BEGIN
    UTL_FILE.PUT_LINE(f_write, '# HEADER: Agency Id: w00066');
    UTL_FILE.PUT_LINE(f_write, '# HEADER: File Generation Date: '||TO_CHAR(SYSDATE, 'YYYY-MM-DD')||'T'||TO_CHAR(SYSDATE, 'HH24:MI:SS'));
    UTL_FILE.PUT_LINE(f_write, '# HEADER: File Format: csv');
    UTL_FILE.PUT_LINE(f_write, '# HEADER: File Format Version: 1');
    UTL_FILE.PUT_LINE(f_write, '# HEADER: Generated by (system): Oracle');

    --number of lines and data fields depends on the table_type
    CASE table_type
      WHEN 'R' THEN	--registration (location) header
        SELECT COUNT(rn)
        INTO v_record_count
        FROM gw_regdets
        WHERE rn IN (
          SELECT rn
          FROM gw_regdets
          WHERE rn IN (
            SELECT rn
            FROM gw_pumtes
            WHERE swl > 0
          )
          AND facility_type <> 'SF'
        UNION
          SELECT DISTINCT rn
          FROM gw_elvdets
          WHERE rn IN (
            SELECT rn
            FROM gw_wlvdets
          )
        )
        AND gis_lat IS NOT NULL
        AND created_date > cdate;

        UTL_FILE.PUT_LINE(f_write, '# HEADER: Number of Records: '||v_record_count);
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Local ObsTime Offset: 10:00');
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Data Fields: IndexNo, RegisteredNo, Latitude, Longitude');

      WHEN 'W' THEN	--water level header
        SELECT COUNT(rn)
        INTO v_record_count
        FROM gw_wlvdets
        WHERE rn IN (
          SELECT rn
          FROM gw_regdets
          WHERE gis_lat IS NOT NULL
          AND rn IN (
            SELECT rn
            FROM gw_elvdets
          )
        )
        AND updated_date > cdate;

        UTL_FILE.PUT_LINE(f_write, '# HEADER: Number of Records: '||v_record_count);
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Local ObsTime Offset: 10:00');
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Data Fields: IndexNo, RegisteredNo, Pipe, MeasureDate, MeasurePoint, Measurement, Remark, Logger');

      WHEN 'E' THEN	--elevation header
        SELECT COUNT(rn)
        INTO v_record_count
        FROM gw_elvdets
        WHERE rn IN (
          SELECT rn
          FROM gw_regdets
          WHERE gis_lat IS NOT NULL
          AND rn IN (
            SELECT rn
            FROM gw_wlvdets
          )
        )
        AND updated_date > cdate;

        UTL_FILE.PUT_LINE(f_write, '# HEADER: Number of Records: '||v_record_count);
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Local ObsTime Offset: 10:00');
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Data Fields: IndexNo, RegisteredNo,  Pipe, ElevationDate, MeasurePoint, Precision, Datum, Elevation, SurveySource');

      WHEN 'P' THEN	--pump test data header
        SELECT COUNT(rn)
        INTO v_record_count
        FROM gw_pumtes
        WHERE swl > 0
        AND rn IN (
          SELECT rn
          FROM gw_regdets
          WHERE gis_lat IS NOT NULL
          AND facility_type <> 'SF'
        )
        AND updated_date > cdate;

        UTL_FILE.PUT_LINE(f_write, '# HEADER: Number of Records: '||v_record_count);
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Local ObsTime Offset: 10:00');
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Data Fields: IndexNo, RegisteredNo, Pipe, PumpTestDate, TestedZoneTop, TestedZoneBottom, StaticHead');

      WHEN 'A' THEN	--aquifer header
        SELECT COUNT(rn)
        INTO v_record_count
        FROM gw_aquifrs
        WHERE rn IN (
          SELECT rn
          FROM gw_pumtes
          WHERE swl > 0
          AND rn IN (
            SELECT rn
            FROM gw_regdets
            WHERE gis_lat IS NOT NULL
            AND facility_type <> 'SF'
          )
        )
        AND updated_date > cdate;

        UTL_FILE.PUT_LINE(f_write, '# HEADER: Number of Records: '||v_record_count);
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Local ObsTime Offset: 10:00');
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Data Fields: IndexNo, RegisteredNo, Record, AquiferTop, AquiferBottom, ContributingAquifer, FormationName');

      WHEN 'C' THEN	--casing header

        SELECT count(rn)
        INTO v_record_count
        FROM gw_casings
        WHERE rn in (
            SELECT DISTINCT rn
            FROM gw_casings
            WHERE rn IN (
                SELECT rn
                FROM gw_regdets
                WHERE gis_lat IS NOT NULL)
            AND rn IN (
                SELECT rn
                FROM gw_wlvdets
            )
        )
        AND material_desc IN ('PERF','SCRN')
        AND updated_date > cdate;

        UTL_FILE.PUT_LINE(f_write, '# HEADER: Number of Records: '||v_record_count);
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Local ObsTime Offset: 10:00');
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Data Fields: IndexNo, RegisteredNo, Pipe, ConstructionDate, MaterialDesc, SizeDesc, Size, Top, Bottom');

      WHEN 'O' THEN	--conductivity header

        SELECT count(rn)
        INTO v_record_count
        FROM gw_fieldqs
        WHERE rn IN (
          SELECT rn
          FROM gw_regdets
          WHERE gis_lat IS NOT NULL
          AND rn IN (
            SELECT rn
            FROM gw_wlvdets
          )
        )
        AND conduct IS NOT NULL
        AND updated_date > cdate;

        SELECT count(rn)
        INTO v_record_count2
        FROM gw_watanls
        WHERE rn IN (
          SELECT rn
          FROM gw_regdets
          WHERE gis_lat IS NOT NULL
          AND rn IN (
            SELECT rn
            FROM gw_wlvdets
          )
        )
        AND conduct IS NOT NULL
        AND updated_date > cdate;

        UTL_FILE.PUT_LINE(f_write, '# HEADER: Number of Records: '||TO_CHAR(v_record_count + v_record_count2));
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Local ObsTime Offset: 10:00');
        UTL_FILE.PUT_LINE(f_write, '# HEADER: Data Fields: IndexNo, RegisteredNo, Pipe, MeasureDate, Conductivity, Lab/Field');

    END CASE;
END create_header;

/*********************************************************************
 * MODULE:	BOM_cond_data
 * PURPOSE:	This procedure is the code for the initial dump of
 *		      conductivity data BoM received.
 * RETURNS:	nothing
 * NOTES:
 *********************************************************************/
PROCEDURE BOM_cond_data IS

  CURSOR c_field IS
    SELECT rn, pipe, rdate, conduct
    FROM gw_fieldqs
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND rn IN (
        SELECT rn
        FROM gw_wlvdets
      )
    )
    AND conduct IS NOT NULL
--    AND created_date > TO_DATE(udate,'DD/MM/YYYY')
--    OR updated_date > TO_DATE(udate,'DD/MM/YYYY')
    ORDER BY rn, pipe, rdate;

  CURSOR c_analys IS
    SELECT rn, pipe, rdate, conduct
    FROM gw_watanls
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND rn IN (
        SELECT rn
        FROM gw_wlvdets
      )
    )
    AND conduct IS NOT NULL
--    AND created_date > TO_DATE(udate,'DD/MM/YYYY')
--    OR updated_date > TO_DATE(udate,'DD/MM/YYYY')
    ORDER BY rn, pipe, rdate;

  TYPE fi_type IS RECORD (
    RN          gw_fieldqs.rn%TYPE,
    PIPE        gw_fieldqs.pipe%TYPE,
    RDATE       gw_fieldqs.rdate%TYPE,
    CONDUCT     gw_fieldqs.conduct%TYPE
  );
  r_field fi_type;

  TYPE wa_type IS RECORD (
    RN          gw_watanls.rn%TYPE,
    PIPE        gw_watanls.pipe%TYPE,
    RDATE       gw_watanls.rdate%TYPE,
    CONDUCT     gw_watanls.conduct%TYPE
  );
  r_lab wa_type;

  v_index NUMBER;
  --v_update_date   VARCHAR2(10);
  v_record_count1  NUMBER;
  v_record_count2  NUMBER;
  v_field_count  NUMBER;
  v_lab_count  NUMBER;
  f_dir  VARCHAR2(20) := 'D:\GW_LOAD';
  f_name	VARCHAR2(40);
  f_w_ref UTL_FILE.FILE_TYPE;


BEGIN

  -- open the file
  f_name := 'w00066_'||TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')||'_Quality.csv';
  f_w_ref := UTL_FILE.FOPEN (f_dir, f_name, 'w');

  --reset the index
  v_index := 1;
  v_field_count := 0;
  v_lab_count := 0;

  --find out how many records

  SELECT count(rn)
  INTO v_record_count1
  FROM gw_fieldqs
  WHERE rn IN (
    SELECT rn
    FROM gw_regdets
    WHERE gis_lat IS NOT NULL
    AND rn IN (
      SELECT rn
      FROM gw_wlvdets
    )
  )
  AND conduct IS NOT NULL;

  SELECT count(rn)
  INTO v_record_count2
  FROM gw_watanls
  WHERE rn IN (
    SELECT rn
    FROM gw_regdets
    WHERE gis_lat IS NOT NULL
    AND rn IN (
      SELECT rn
      FROM gw_wlvdets
    )
  )
  AND conduct IS NOT NULL;

  -- input header data
  UTL_FILE.PUT_LINE(f_w_ref, '# HEADER: Agency Id: w00066');
  UTL_FILE.PUT_LINE(f_w_ref, '# HEADER: File Generation Date: '||TO_CHAR(SYSDATE, 'YYYY-MM-DD')||'T'||TO_CHAR(SYSDATE, 'HH24:MI:SS'));
  UTL_FILE.PUT_LINE(f_w_ref, '# HEADER: File Format: csv');
  UTL_FILE.PUT_LINE(f_w_ref, '# HEADER: File Format Version: 1');
  UTL_FILE.PUT_LINE(f_w_ref, '# HEADER: Generated by (system): Oracle');
  UTL_FILE.PUT_LINE(f_w_ref, '# HEADER: Number of Records: '||TO_CHAR(v_record_count1 + v_record_count2));
  UTL_FILE.PUT_LINE(f_w_ref, '# HEADER: Local ObsTime Offset: 10:00');
  UTL_FILE.PUT_LINE(f_w_ref, '# HEADER: Data Fields: IndexNo, RegisteredNo, Pipe, MeasureDate, Conductivity, Lab/Field');

  -- grab data

  OPEN c_field;
  FETCH c_field INTO r_field.rn, r_field.pipe, r_field.rdate, r_field.conduct;

  OPEN c_analys;
  FETCH c_analys INTO r_lab.rn, r_lab.pipe, r_lab.rdate, r_lab.conduct;

  LOOP
    EXIT WHEN v_field_count = v_record_count1 AND v_lab_count = v_record_count2;

    IF v_field_count = v_record_count1 THEN
      r_field.rn := 999999999;
    END IF;
    IF v_lab_count = v_record_count2 THEN
      r_lab.rn := 999999999;
    END IF;
    --if rns are equal
    IF r_field.rn = r_lab.rn THEN

      --if pipes are equal
      IF r_field.pipe = r_lab.pipe THEN

        --if dates are equal
        IF r_field.rdate = r_lab.rdate THEN

          -- write to file
          UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_field.rn||',"'||r_field.pipe||'",'||TO_CHAR(r_field.rdate, 'YYYY-MM-DD')||','||r_field.conduct||',"F"');
          v_index := v_index + 1;
          v_field_count := v_field_count + 1;
          FETCH c_field INTO r_field.rn, r_field.pipe, r_field.rdate, r_field.conduct;

        --else if field.date is lower
        ELSIF r_field.rdate < r_lab.rdate THEN

          -- write to file
          UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_field.rn||',"'||r_field.pipe||'",'||TO_CHAR(r_field.rdate, 'YYYY-MM-DD')||','||r_field.conduct||',"F"');
          v_index := v_index + 1;
          v_field_count := v_field_count + 1;
          FETCH c_field INTO r_field.rn, r_field.pipe, r_field.rdate, r_field.conduct;


        ELSE      --else analys.date is lower
          -- write to file
          UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_lab.rn||',"'||r_lab.pipe||'",'||TO_CHAR(r_lab.rdate, 'YYYY-MM-DD')||','||r_lab.conduct||',"L"');
          v_index := v_index + 1;
          v_lab_count := v_lab_count + 1;
          FETCH c_analys INTO r_lab.rn, r_lab.pipe, r_lab.rdate, r_lab.conduct;

        END IF;

      ELSIF r_field.pipe < r_lab.pipe THEN    --elsif field.pipe is lower

        -- write to file
        UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_field.rn||',"'||r_field.pipe||'",'||TO_CHAR(r_field.rdate, 'YYYY-MM-DD')||','||r_field.conduct||',"F"');
        v_index := v_index + 1;
        v_field_count := v_field_count + 1;
        FETCH c_field INTO r_field.rn, r_field.pipe, r_field.rdate, r_field.conduct;


      ELSE          --else analys.pipe is lower

        -- write to file
        UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_lab.rn||',"'||r_lab.pipe||'",'||TO_CHAR(r_lab.rdate, 'YYYY-MM-DD')||','||r_lab.conduct||',"L"');
        v_index := v_index + 1;
        v_lab_count := v_lab_count + 1;
        FETCH c_analys INTO r_lab.rn, r_lab.pipe, r_lab.rdate, r_lab.conduct;

      END IF;

    ELSIF r_field.rn < r_lab.rn THEN      --else if field lower

      -- write to file
      UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_field.rn||',"'||r_field.pipe||'",'||TO_CHAR(r_field.rdate, 'YYYY-MM-DD')||','||r_field.conduct||',"F"');
      v_index := v_index + 1;
      v_field_count := v_field_count + 1;
      FETCH c_field INTO r_field.rn, r_field.pipe, r_field.rdate, r_field.conduct;

    ELSE            --else analys is lower

      -- write to file
      UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_lab.rn||',"'||r_lab.pipe||'",'||TO_CHAR(r_lab.rdate, 'YYYY-MM-DD')||','||r_lab.conduct||',"L"');
      v_index := v_index + 1;
      v_lab_count := v_lab_count + 1;
      FETCH c_analys INTO r_lab.rn, r_lab.pipe, r_lab.rdate, r_lab.conduct;

    END IF;

  END LOOP;
  CLOSE c_analys;
  CLOSE c_field;
  UTL_FILE.FCLOSE(f_w_ref);

EXCEPTION
   WHEN UTL_FILE.INVALID_PATH
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_path'); RAISE;

   WHEN UTL_FILE.INVALID_MODE
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_mode'); RAISE;

   WHEN UTL_FILE.INVALID_FILEHANDLE
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_filehandle'); RAISE;

   WHEN UTL_FILE.INVALID_OPERATION
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_operation'); RAISE;

   WHEN UTL_FILE.READ_ERROR
   THEN
       DBMS_OUTPUT.PUT_LINE ('read_error'); RAISE;

   WHEN UTL_FILE.WRITE_ERROR
   THEN
      DBMS_OUTPUT.PUT_LINE ('write_error'); RAISE;

   WHEN UTL_FILE.INTERNAL_ERROR
   THEN
      DBMS_OUTPUT.PUT_LINE ('internal_error'); RAISE;

END BOM_cond_data;


/*********************************************************************
 * MODULE:	BOM_casing_data
 * PURPOSE:	This procedure is the code for the initial dump of
 *		      casing data BoM received.
 * RETURNS:	nothing
 * NOTES:
 *********************************************************************/
PROCEDURE BOM_casing_data IS


  CURSOR c_rns IS
    SELECT DISTINCT rn
    FROM gw_casings
    WHERE rn IN (
          SELECT rn
          FROM gw_regdets
          WHERE gis_lat IS NOT NULL)
    AND rn IN (
          SELECT rn
          FROM gw_wlvdets)
    ORDER BY rn;

  CURSOR c_data (regnum NUMBER) IS
    SELECT rn, pipe, rdate, material_desc, material_size, size_desc, top, bottom
    FROM gw_casings
    WHERE rn = regnum
    AND material_desc IN ('PERF','SCRN')
    ORDER BY pipe, rdate;


  v_index NUMBER;
  v_record_count  NUMBER;
  f_dir  VARCHAR2(20) := 'D:\GW_LOAD';
  f_name	VARCHAR2(40);
  f_w_ref UTL_FILE.FILE_TYPE;


BEGIN

  -- open the file
  f_name := 'w00066_'||TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')||'_Casing.csv';
  f_w_ref := UTL_FILE.FOPEN (f_dir, f_name, 'w');

  --reset the index
  v_index := 1;

  --find out how many records
  SELECT count(rn)
  INTO v_record_count
  FROM gw_casings
  WHERE rn in (
      SELECT DISTINCT rn
      FROM gw_casings
      WHERE rn IN (
          SELECT rn
          FROM gw_regdets
          WHERE gis_lat IS NOT NULL)
      AND rn IN (
          SELECT rn
          FROM gw_wlvdets))
  AND material_desc IN ('PERF','SCRN');

  -- input header data
  UTL_FILE.PUT_LINE(f_w_ref, '# HEADER: Agency Id: w00066');
  UTL_FILE.PUT_LINE(f_w_ref, '# HEADER: File Generation Date: '||TO_CHAR(SYSDATE, 'YYYY-MM-DD')||'T'||TO_CHAR(SYSDATE, 'HH24:MI:SS'));
  UTL_FILE.PUT_LINE(f_w_ref, '# HEADER: File Format: csv');
  UTL_FILE.PUT_LINE(f_w_ref, '# HEADER: File Format Version: 1');
  UTL_FILE.PUT_LINE(f_w_ref, '# HEADER: Generated by (system): Oracle');
  UTL_FILE.PUT_LINE(f_w_ref, '# HEADER: Number of Records: '||v_record_count);
  UTL_FILE.PUT_LINE(f_w_ref, '# HEADER: Local ObsTime Offset: 10:00');
  UTL_FILE.PUT_LINE(f_w_ref, '# HEADER: Data Fields: IndexNo, RegisteredNo, Pipe, ConstructionDate, MaterialDesc, SizeDesc, Size, Top, Bottom');

   --rn, pipe, rdate, material_desc, material_size, size_desc, top, bottom

  -- grab data
  FOR r_rns IN c_rns LOOP

    FOR r_data IN c_data (r_rns.rn) LOOP

      UTL_FILE.PUT_LINE(f_w_ref, v_index||','||r_data.rn||',"'||r_data.pipe||'",'||TO_CHAR(r_data.rdate, 'YYYY-MM-DD')||',"'||r_data.material_desc||'","'||r_data.size_desc||'",'||r_data.material_size||','||r_data.top||','||r_data.bottom);
      v_index := v_index + 1;

    END LOOP;

  END LOOP;

  UTL_FILE.FCLOSE(f_w_ref);

EXCEPTION
   WHEN UTL_FILE.INVALID_PATH
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_path'); RAISE;

   WHEN UTL_FILE.INVALID_MODE
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_mode'); RAISE;

   WHEN UTL_FILE.INVALID_FILEHANDLE
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_filehandle'); RAISE;

   WHEN UTL_FILE.INVALID_OPERATION
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_operation'); RAISE;

   WHEN UTL_FILE.READ_ERROR
   THEN
       DBMS_OUTPUT.PUT_LINE ('read_error'); RAISE;

   WHEN UTL_FILE.WRITE_ERROR
   THEN
      DBMS_OUTPUT.PUT_LINE ('write_error'); RAISE;

   WHEN UTL_FILE.INTERNAL_ERROR
   THEN
      DBMS_OUTPUT.PUT_LINE ('internal_error'); RAISE;

END BOM_casing_data;


/*********************************************************************
 * MODULE:	BOM_update_xml
 * PURPOSE:	This procedure is the code for the incrimental data update
 *		      BoM receive in their xml WDTFv1.0.
 * RETURNS:	nothing
 * NOTES:   creates several xml files that need to be zipped and sent
 *          to BoM via their ftp site.
 *********************************************************************/
PROCEDURE BOM_update_xml IS

  CURSOR c_rn (udate DATE) IS
    SELECT rn
    FROM gw_regdets
    WHERE rn IN (42231059,44701,99067,109106,11620,3131)
    ORDER BY rn;
  
  CURSOR c_rns (udate DATE) IS
    SELECT rn
    FROM gw_regdets
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE rn IN (
        SELECT rn
        FROM gw_pumtes
        WHERE swl > 0
      )
      AND facility_type <> 'SF'
      UNION
      SELECT DISTINCT rn
      FROM gw_elvdets
      WHERE rn IN (
        SELECT rn
        FROM gw_wlvdets
      )
    )
    AND gis_lat IS NOT NULL
    AND created_date > udate
    UNION
    SELECT rn
    FROM gw_wlvdets
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND rn IN (
        SELECT rn
        FROM gw_elvdets
      )
    )
    AND updated_date > udate
    UNION
    SELECT rn
    FROM gw_elvdets
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND rn IN (
        SELECT rn
        FROM gw_wlvdets
      )
    )
    AND updated_date > udate
    UNION
    SELECT rn
    FROM gw_pumtes
    WHERE swl > 0
    AND rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND facility_type <> 'SF'
    )
    AND updated_date > udate
    UNION
    SELECT rn
    FROM gw_aquifrs
    WHERE rn IN (
      SELECT rn
      FROM gw_pumtes
      WHERE swl > 0
      AND rn IN (
        SELECT rn
        FROM gw_regdets
        WHERE gis_lat IS NOT NULL
      	AND facility_type <> 'SF'
      )
    )
    AND updated_date > udate
    UNION
    SELECT rn
    FROM gw_fieldqs
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND rn IN (
        SELECT rn
        FROM gw_wlvdets
      )
    )
    AND conduct IS NOT NULL
    AND updated_date > udate;

  f_dir         VARCHAR2(20) := 'D:\GW_LOAD\xml_files';
  f_name	VARCHAR2(50);
  f_w_ref       UTL_FILE.FILE_TYPE;
  v_update_date   VARCHAR2(10);
  v_datum_id      VARCHAR2(40);
  v_datum_done    BOOLEAN;

BEGIN

  --get the date of the last update from gw_system_variables
  SELECT parameter_value
  INTO v_update_date
  FROM gw_system_variables
  WHERE parameter_code = 'BOM_DATA_UPDATE';

--  v_update_date := '21/02/2009';

  --first need to get all the RNs with data to go to BOM
  --loop through these rns
  FOR r_rns IN c_rn (TO_DATE(v_update_date,'DD/MM/YYYY')) LOOP

    --create file and generate file name
    f_name := 'wdtf-v1.w00066.'||TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')||'.'||r_rns.rn||'.xml';
    f_w_ref := UTL_FILE.FOPEN (f_dir, f_name, 'w');
    
    --reset the boolean tracker for the next rn
    v_datum_done := TRUE;

    --generate the hydrocode and metadata tag information (mandatory)
    xml_header(r_rns.rn, f_w_ref, f_name);

    --generate the water level (if necessary)
    --this has to be called before the xml_casing procedure because it uses the xml_elevation procedure to generate all the vertical datum tags at once
    xml_waterlvls(r_rns.rn, f_w_ref, v_update_date, v_datum_done);
    
    --generate the casing tags (if necessary)
    xml_casing(r_rns.rn, f_w_ref, TO_DATE(v_update_date,'DD/MM/YYYY'), v_datum_done);

    --generate the water quality tags (if necessary)
    --this has to be called before the xml_casing procedure because it uses the observationMember tags the same as water levels
    --this is to ensure that all like tags are together
    xml_quality(r_rns.rn, f_w_ref, TO_DATE(v_update_date,'DD/MM/YYYY'));
    
    UTL_FILE.PUT_LINE(f_w_ref, '</wdtf:HydroCollection>');
    UTL_FILE.FCLOSE(f_w_ref);
  END LOOP;

  --update the date to todays date for next update
--  UPDATE gw_system_variables
--  SET parameter_value = TO_CHAR(SYSDATE, 'DD/MM/YYYY')
--  WHERE parameter_code = 'BOM_DATA_UPDATE';

--  commit;

EXCEPTION
   WHEN UTL_FILE.INVALID_PATH
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_path'); RAISE;

   WHEN UTL_FILE.INVALID_MODE
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_mode'); RAISE;

   WHEN UTL_FILE.INVALID_FILEHANDLE
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_filehandle'); RAISE;

   WHEN UTL_FILE.INVALID_OPERATION
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_operation'); RAISE;

   WHEN UTL_FILE.READ_ERROR
   THEN
       DBMS_OUTPUT.PUT_LINE ('read_error'); RAISE;

   WHEN UTL_FILE.WRITE_ERROR
   THEN
      DBMS_OUTPUT.PUT_LINE ('write_error'); RAISE;

   WHEN UTL_FILE.INTERNAL_ERROR
   THEN
      DBMS_OUTPUT.PUT_LINE ('internal_error'); RAISE;

END BOM_update_xml;

/*********************************************************************
 * MODULE:	xml_header
 * PURPOSE:	This procedure is the code that creates the header
 *		     information for WDTFv1.0.
 * RETURNS:	nothing
 * NOTES:       inserts the HydroCollection and metadata tags into the current
 *              xml file.
 *********************************************************************/
PROCEDURE xml_header(p_rn IN NUMBER, f_write IN UTL_FILE.FILE_TYPE, f_name IN VARCHAR2) IS

BEGIN
  UTL_FILE.PUT_LINE(f_write, '<?xml version="1.0" ?> ');
  UTL_FILE.PUT_LINE(f_write, '<wdtf:HydroCollection xmlns:sa="http://www.opengis.net/sampling/1.0/sf1" xmlns:om="http://www.opengis.net/om/1.0/sf1"');
  UTL_FILE.PUT_LINE(f_write, '  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:gml="http://www.opengis.net/gml"');
  UTL_FILE.PUT_LINE(f_write, '  xmlns:wdtf="http://www.bom.gov.au/std/water/xml/wdtf/1.0" xmlns:ahgf="http://www.bom.gov.au/std/water/xml/ahgf/0.2"');
  UTL_FILE.PUT_LINE(f_write, '  xsi:schemaLocation="http://www.opengis.net/sampling/1.0/sf1 ../sampling/sampling.xsd');
  UTL_FILE.PUT_LINE(f_write, '  http://www.bom.gov.au/std/water/xml/wdtf/1.0 ../wdtf/water.xsd http://www.bom.gov.au/std/water/xml/ahgf/0.2 ../ahgf/waterFeatures.xsd"');
  UTL_FILE.PUT_LINE(f_write, '  gml:id="'||f_name||'">');
  UTL_FILE.PUT_LINE(f_write, '    <gml:description>This document encodes an incremental update of Ground Water data relating to RN '||p_rn||'.</gml:description>');
  UTL_FILE.PUT_LINE(f_write, '    <gml:name codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/feature/HydroCollection/w00066/">'||f_name||'</gml:name>');
  UTL_FILE.PUT_LINE(f_write, '    <wdtf:metadata>');
  UTL_FILE.PUT_LINE(f_write, '      <wdtf:DocumentInfo>');
  UTL_FILE.PUT_LINE(f_write, '        <wdtf:version>wdtf-package-v1.0.2</wdtf:version>');
  UTL_FILE.PUT_LINE(f_write, '        <wdtf:dataOwner codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/party/person/bom/">w00066</wdtf:dataOwner>');
  UTL_FILE.PUT_LINE(f_write, '        <wdtf:dataProvider codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/party/person/bom/">w00066</wdtf:dataProvider>');
  UTL_FILE.PUT_LINE(f_write, '        <wdtf:generationDate>'||TO_CHAR(SYSDATE, 'YYYY-MM-DD')||'T'||TO_CHAR(SYSDATE, 'HH24:MI:SS')||'+10:00</wdtf:generationDate>');
  UTL_FILE.PUT_LINE(f_write, '        <wdtf:generationSystem>ORACLE</wdtf:generationSystem>');
  UTL_FILE.PUT_LINE(f_write, '      </wdtf:DocumentInfo>');
  UTL_FILE.PUT_LINE(f_write, '    </wdtf:metadata>');

  RETURN;

END xml_header;

/*********************************************************************
 * MODULE:	xml_location
 * PURPOSE:	This procedure is the code that creates the location
 *		     information for WDTFv1.0.
 * RETURNS:	nothing
 * NOTES:       inserts the samplingGroup and SamplingPoint tags into the current
 *              xml file.
 *********************************************************************/
PROCEDURE xml_location(p_rn IN NUMBER, f_write IN UTL_FILE.FILE_TYPE, udate IN  VARCHAR2) IS

  CURSOR c_regdets IS
    SELECT rn, gis_lat, gis_lng
    FROM gw_regdets
    WHERE rn = p_rn
    AND created_date > TO_DATE(udate,'DD/MM/YYYY');

  v_rn      gw_regdets.rn%TYPE;
  v_lat     gw_regdets.gis_lat%TYPE;
  v_long    gw_regdets.gis_lng%TYPE;

BEGIN

  OPEN c_regdets;
  FETCH c_regdets INTO v_rn, v_lat, v_long;
  --if there is some location information to be sent then add that to the file
  IF c_regdets%FOUND THEN

    UTL_FILE.PUT_LINE(f_write, '    <wdtf:siteMember>');
    UTL_FILE.PUT_LINE(f_write, '      <wdtf:SamplingGroup gml:id="sg'||v_rn||'">');
    UTL_FILE.PUT_LINE(f_write, '        <gml:description>Site of bore known as '||v_rn||'</gml:description>');
    UTL_FILE.PUT_LINE(f_write, '        <gml:name codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/feature/SamplingGroup/w00066/">'||v_rn||'</gml:name>');
    UTL_FILE.PUT_LINE(f_write, '        <sa:sampledFeature xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/feature/Bore/w00066/'||v_rn||'" />');
    UTL_FILE.PUT_LINE(f_write, '        <wdtf:location>');
    UTL_FILE.PUT_LINE(f_write, '          <gml:Point srsName="urn:ogc:def:crs:EPSG::4283">');
    UTL_FILE.PUT_LINE(f_write, '            <gml:pos>'||v_lat||' '||v_long||'</gml:pos>');
    UTL_FILE.PUT_LINE(f_write, '          </gml:Point>');
    UTL_FILE.PUT_LINE(f_write, '        </wdtf:location>');
    UTL_FILE.PUT_LINE(f_write, '        <wdtf:timeZone>+10:00</wdtf:timeZone>');
    UTL_FILE.PUT_LINE(f_write, '      </wdtf:SamplingGroup>');
    UTL_FILE.PUT_LINE(f_write, '    </wdtf:siteMember>');
    UTL_FILE.PUT_LINE(f_write, '    <wdtf:siteMember>');
    UTL_FILE.PUT_LINE(f_write, '      <wdtf:SamplingPoint gml:id="sp'||v_rn||'">');
    UTL_FILE.PUT_LINE(f_write, '        <gml:description>Bore known as '||v_rn||'</gml:description>');
    UTL_FILE.PUT_LINE(f_write, '        <gml:name codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/feature/SamplingGroup/w00066/">'||v_rn||'</gml:name>');
    UTL_FILE.PUT_LINE(f_write, '        <sa:sampledFeature xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/feature/Bore/w00066/'||v_rn||'" xlink:title="Bore" />');
    UTL_FILE.PUT_LINE(f_write, '        <sa:relatedSamplingFeature xlink:href="#sg'||v_rn||'" xlink:arcrole="http://www.bom.gov.au/std/water/xml/wio0.2/definition/arcrole/bom/parent" />');
    UTL_FILE.PUT_LINE(f_write, '        <sa:position>');
    UTL_FILE.PUT_LINE(f_write, '          <gml:Point srsName="urn:ogc:def:crs:EPSG::4283">');
    UTL_FILE.PUT_LINE(f_write, '            <gml:pos>'||v_lat||' '||v_long||'</gml:pos>');
    UTL_FILE.PUT_LINE(f_write, '          </gml:Point>');
    UTL_FILE.PUT_LINE(f_write, '        </sa:position>');
    UTL_FILE.PUT_LINE(f_write, '        <wdtf:timeZone>+10:00</wdtf:timeZone>');
    UTL_FILE.PUT_LINE(f_write, '      </wdtf:SamplingPoint>');
    UTL_FILE.PUT_LINE(f_write, '    </wdtf:siteMember>');

  END IF;

  CLOSE c_regdets;

END xml_location;

/*********************************************************************
 * MODULE:	xml_waterlvls
 * PURPOSE:	This procedure is the code that creates the water level
 *		     information for WDTFv1.0.
 * RETURNS:	nothing
 * NOTES:       inserts the procedureMemver and observationMember tags into the current
 *              xml file.
 *********************************************************************/
PROCEDURE xml_waterlvls(p_rn IN NUMBER, f_write IN UTL_FILE.FILE_TYPE, udate IN  DATE, p_datum_done IN OUT BOOLEAN) IS

  CURSOR c_wl IS
    SELECT DISTINCT rn
    FROM gw_wlvdets
    WHERE rn = p_rn
    AND meas_point <> 'N'
    AND updated_date > udate
    UNION
    SELECT DISTINCT rn
    FROM gw_pumtes
    WHERE rn = p_rn
    AND updated_date > udate;

  CURSOR c_wlvdets IS
    SELECT rn, pipe, rdate, meas_point, measurement, logger, remark
    FROM gw_wlvdets w
    WHERE rn = p_rn
    AND updated_date > udate
    AND pipe <> 'X'
    AND meas_point <> 'N'
    ORDER BY pipe;
  
  CURSOR c_artesian IS
    SELECT rn
    FROM gw_regdets
    WHERE rn = p_rn
    AND facility_type LIKE 'A%';

  CURSOR c_pumtes IS
    SELECT rn, pipe, rdate, swl
    FROM gw_pumtes
    WHERE rn = p_rn
    AND updated_date > udate
    AND pipe <> 'X'
    AND swl IS NOT NULL
    ORDER BY rdate;

  CURSOR c_elvdets IS
    SELECT rn, pipe, rdate, elevation
    FROM gw_elvdets
    WHERE rn = p_rn
    AND meas_point = 'R'
    AND datum = 'AHD'
    AND pipe = 'A'
    ORDER BY rdate ASC;

  TYPE waterlvl_rec IS RECORD (
    rn             gw_wlvdets.rn%TYPE,
    pipe             gw_wlvdets.pipe%TYPE,
    rdate            gw_wlvdets.rdate%TYPE,
    meas_point       gw_wlvdets.meas_point%TYPE,
    measurement      gw_wlvdets.measurement%TYPE,
    remark           gw_wlvdets.remark%TYPE,
    logger          gw_wlvdets.logger%TYPE,
    edate           gw_elvdets.rdate%TYPE,
    elevation       gw_elvdets.elevation%TYPE,
    vdatum_id           VARCHAR2(40),
    gwlm_id             VARCHAR2(40),
    units               VARCHAR2(3),
    written           NUMBER(1));

  e_waterlvl     waterlvl_rec;
  TYPE waterlvl_type IS TABLE OF
    e_waterlvl%TYPE
    INDEX BY PLS_INTEGER;

  t_waterlvl    waterlvl_type;
  v_rn      gw_wlvdets.rn%TYPE;
  v_pipe     gw_wlvdets.pipe%TYPE;
  v_date    gw_wlvdets.rdate%TYPE;
  v_mpoint      gw_wlvdets.meas_point%TYPE;
  v_measurement     gw_wlvdets.measurement%TYPE;
  v_logger    gw_wlvdets.logger%TYPE;
  v_remark    gw_wlvdets.remark%TYPE;
  v_elev_row    c_elvdets%ROWTYPE;
  v_index       NUMBER;
  v_index2       NUMBER;
  v_currdatum      VARCHAR2(40) := NULL;
  v_currgwlm        VARCHAR2(40) := NULL;
  v_currpipe        gw_wlvdets.pipe%TYPE;
  v_count         NUMBER := 1;
  v_write_datum         BOOLEAN := TRUE;  --***
  v_write_gwlm         BOOLEAN := TRUE;   --***
  v_observPoint       VARCHAR2(40);       --***

  v_elev_cnt      NUMBER(1);
  v_time_value    VARCHAR2(500);
  v_pipecount     NUMBER(1);


BEGIN

  OPEN c_wl;
  FETCH c_wl INTO v_rn;
  --if there is some water level data to be sent then add that to the file
  IF c_wl%FOUND THEN
    
    SELECT COUNT(rn)
    INTO v_elev_cnt
    FROM gw_elvdets
    WHERE rn = v_rn
    AND meas_point = 'R'
    AND datum = 'AHD'
    AND pipe = 'A';  --this gets the number of relevant elevation entries

    IF v_elev_cnt > 0 THEN
      --if there are any elevation entries loop through them

      OPEN c_elvdets;
      FOR i IN 1..v_elev_cnt LOOP --for each relevant elevation
        
        --fetch the current elevation data
        FETCH c_elvdets INTO v_elev_row;
        --v_index := 1; --reset the water level index
        
        --for each water level recorded
        FOR r_wlvdets IN c_wlvdets LOOP
          
          v_index := TO_NUMBER(TO_CHAR(r_wlvdets.rdate,'YYYYMMDD'));
          
          --compare the current elevation date with each water level date
          --if the elevation date is less than or equal to then there is an elevation for this water level
          IF v_elev_row.rdate <= r_wlvdets.rdate THEN
          
            --check to see if it already exists in the table
            --if it does then need to compare with this elevation date and update the entry if necessary
            
            IF t_waterlvl.EXISTS(v_index) THEN
              
              IF v_elev_row.rdate <= t_waterlvl(v_index).rdate THEN
                
                t_waterlvl(v_index).edate := v_elev_row.rdate;
                t_waterlvl(v_index).elevation := v_elev_row.elevation;
                t_waterlvl(v_index).vdatum_id := 'ObservationDatum'||r_wlvdets.rn||'-'||i;
                t_waterlvl(v_index).gwlm_id := 'gwMethod'||r_wlvdets.rn||'-'||i;
                
              END IF;
              
            ELSE
              --else an entry does not already exist and needs to be created
              
              t_waterlvl(v_index).rn := r_wlvdets.rn;
              t_waterlvl(v_index).pipe := r_wlvdets.pipe;
              t_waterlvl(v_index).rdate := r_wlvdets.rdate;
              t_waterlvl(v_index).meas_point := r_wlvdets.meas_point;
              t_waterlvl(v_index).measurement := r_wlvdets.measurement;
              t_waterlvl(v_index).units := 'm';
              t_waterlvl(v_index).remark := r_wlvdets.remark;
              t_waterlvl(v_index).logger := r_wlvdets.logger;
              t_waterlvl(v_index).edate := v_elev_row.rdate;
              t_waterlvl(v_index).elevation := v_elev_row.elevation;
              t_waterlvl(v_index).vdatum_id := 'ObservationDatum'||r_wlvdets.rn||'-'||i;
              t_waterlvl(v_index).gwlm_id := 'gwMethod'||r_wlvdets.rn||'-'||i;
              
                            
            END IF;
          ELSE
            --else this water level date does not have an elevation
            
            IF t_waterlvl.EXISTS(v_index) THEN
              NULL;
            ELSE
              
              --there is no elevation for this water level so no vdatum_id just a gwlm_id
              t_waterlvl(v_index).rn := r_wlvdets.rn;
              t_waterlvl(v_index).pipe := r_wlvdets.pipe;
              t_waterlvl(v_index).rdate := r_wlvdets.rdate;
              t_waterlvl(v_index).meas_point := r_wlvdets.meas_point;
              t_waterlvl(v_index).measurement := r_wlvdets.measurement;
              t_waterlvl(v_index).units := 'm';
              t_waterlvl(v_index).remark := r_wlvdets.remark;
              t_waterlvl(v_index).logger := r_wlvdets.logger;
              t_waterlvl(v_index).edate := NULL;
              t_waterlvl(v_index).elevation := NULL;
              t_waterlvl(v_index).vdatum_id := NULL;
              t_waterlvl(v_index).gwlm_id := 'gwTOC'||r_wlvdets.rn;
              
              
            END IF;

          END IF;
        END LOOP;
      END LOOP;

      CLOSE c_elvdets;
      
      --now to do the pump test data
      OPEN c_elvdets;
      FOR i IN 1..v_elev_cnt LOOP --for each relevant elevation
        
        --fetch the current elevation data
        FETCH c_elvdets INTO v_elev_row;
        
        --for each pump test recorded
        FOR r_pumtes IN c_pumtes LOOP
          
          v_index := TO_NUMBER(TO_CHAR(r_pumtes.rdate,'YYYYMMDD'));
          
          --compare the current elevation date with each pump test date
          --if the elevation date is less than or equal to then there is an elevation for this pump test
          IF v_elev_row.rdate <= r_pumtes.rdate THEN
          
            --check to see if it already exists in the table
            --if it does then do nothing because the water level trumps pump test
            
            IF t_waterlvl.EXISTS(v_index) THEN
              NULL;
            ELSE
              
              --else an entry does not already exist and needs to be created
              
              t_waterlvl(v_index).rn := r_pumtes.rn;
              t_waterlvl(v_index).pipe := r_pumtes.pipe;
              t_waterlvl(v_index).rdate := r_pumtes.rdate;
              t_waterlvl(v_index).meas_point := 'R';
              t_waterlvl(v_index).measurement := r_pumtes.swl;
              t_waterlvl(v_index).units := 'm';
              t_waterlvl(v_index).remark := NULL;
              t_waterlvl(v_index).logger := NULL;
              t_waterlvl(v_index).edate := v_elev_row.rdate;
              t_waterlvl(v_index).elevation := v_elev_row.elevation;
              t_waterlvl(v_index).vdatum_id := 'ObservationDatum'||r_pumtes.rn||'-'||i;
              t_waterlvl(v_index).gwlm_id := 'gwMethod'||r_pumtes.rn||'-'||i;
              
              
            END IF;
          ELSE
            --else this water level date does not have an elevation
            
            IF t_waterlvl.EXISTS(v_index) THEN
              NULL;
            ELSE
              
              --there is no elevation for this water level so no vdatum_id just a gwlm_id
              t_waterlvl(v_index).rn := r_pumtes.rn;
              t_waterlvl(v_index).pipe := r_pumtes.pipe;
              t_waterlvl(v_index).rdate := r_pumtes.rdate;
              t_waterlvl(v_index).meas_point := 'R';
              t_waterlvl(v_index).measurement := r_pumtes.swl;
              t_waterlvl(v_index).units := 'm';
              t_waterlvl(v_index).remark := NULL;
              t_waterlvl(v_index).logger := NULL;
              t_waterlvl(v_index).edate := NULL;
              t_waterlvl(v_index).elevation := NULL;
              t_waterlvl(v_index).vdatum_id := NULL;
              t_waterlvl(v_index).gwlm_id := 'gwTOC'||r_pumtes.rn;
              
              
            END IF;

          END IF;
        END LOOP; --pump test table loop
        
      END LOOP; --elevation count loop
      
      CLOSE c_elvdets;
      
      
      --ok, now the info is inserting correctly into t_waterlvl now I need to get it out again.
      --all like tags need to be put together so
      
      --loop through t_waterlvl for all the definitionMember tags using vdatum_id
      IF t_waterlvl.FIRST IS NOT NULL THEN
        v_index := t_waterlvl.FIRST;
        LOOP
          
          IF t_waterlvl(v_index).vdatum_id IS NOT NULL THEN   --possible new definitionMember tag
            IF v_currdatum IS NULL OR t_waterlvl(v_index).vdatum_id <> v_currdatum THEN
              --only if it is a new datum do you get this far
              v_currdatum := t_waterlvl(v_index).vdatum_id;
            
              --loop through t_waterlvl to see if this vdatum_id has been done previously
              --FOR j IN t_waterlvl.FIRST..(v_index-1) LOOP
              
              IF v_index <> t_waterlvl.FIRST THEN
                v_index2 := t_waterlvl.FIRST;
                LOOP
              
                  --if this datum has already been written to the file then don't write to file now
                  IF t_waterlvl(v_index2).vdatum_id = v_currdatum THEN
                    v_write_datum := FALSE;
                  END IF;
                  EXIT WHEN v_index2 = t_waterlvl.PRIOR(v_index);
                  v_index2 := t_waterlvl.NEXT(v_index2);
                END LOOP;
              END IF;
            
              IF v_write_datum THEN
                --now we can output the definitionMember tag
              
                UTL_FILE.PUT_LINE(f_write, '    <wdtf:definitionMember>');
                UTL_FILE.PUT_LINE(f_write, '      <gml:VerticalDatum gml:id="'||t_waterlvl(v_index).vdatum_id||'">');
                UTL_FILE.PUT_LINE(f_write, '        <gml:datumName>urn:ogc:def:datum:bom::TopOfCasing</gml:datumName>');
                UTL_FILE.PUT_LINE(f_write, '        <gml:anchorPoint codeSpace="urn:ogc:def:crs:EPSG::5711">'||t_waterlvl(v_index).elevation||'</gml:anchorPoint>');
                UTL_FILE.PUT_LINE(f_write, '      </gml:VerticalDatum>');
                UTL_FILE.PUT_LINE(f_write, '    </wdtf:definitionMember>');
              
              END IF;
            END IF;
          END IF;
          EXIT WHEN v_index = t_waterlvl.LAST;
          v_index := t_waterlvl.NEXT(v_index);
        END LOOP;--t_waterlvl loop
      END IF;--t_waterlvl not null
      
      --then call xml_elevation to create the definitionMember tags for the casing data
      
      xml_casing(p_rn, f_write, udate, p_datum_done);
          
      --once that's done then loop through t_waterlvl for the procedureMember tags using gwlm_id
      IF t_waterlvl.FIRST IS NOT NULL THEN
        v_index := t_waterlvl.FIRST;
        LOOP
          IF v_currgwlm IS NULL OR t_waterlvl(v_index).gwlm_id <> v_currgwlm THEN
          
            v_currgwlm := t_waterlvl(v_index).gwlm_id;
          
            --loop through t_waterlvl to see if this gwlm has been done previously
            --FOR j IN t_waterlvl.FIRST..(i-1) LOOP
            IF v_index <> t_waterlvl.FIRST THEN
              v_index2 := t_waterlvl.FIRST;
              LOOP
              
                --if this gwlm has already been written to the file then don't write to file now
                IF t_waterlvl(v_index2).gwlm_id = v_currgwlm THEN
                  v_write_gwlm := FALSE;
                END IF;
                EXIT WHEN v_index2 = t_waterlvl.PRIOR(v_index);
                v_index2 := t_waterlvl.NEXT(v_index2);
              END LOOP;
            END IF;
          
            IF t_waterlvl(v_index).vdatum_id IS NOT NULL THEN
              v_observPoint := '#'||t_waterlvl(v_index).vdatum_id;
            ELSE
              v_observPoint := 'urn:ogc:def:datum:bom::TopOfCasing';
            END IF;
          
            IF v_write_gwlm THEN
              UTL_FILE.PUT_LINE(f_write, '    <wdtf:procedureMember>');
              UTL_FILE.PUT_LINE(f_write, '      <wdtf:GroundWaterLevelMethod gml:id="'||t_waterlvl(v_index).gwlm_id||'">');
              UTL_FILE.PUT_LINE(f_write, '        <wdtf:observationPointDatum xlink:href="'||v_observPoint||'" xlink:title="top of casing" />');
              UTL_FILE.PUT_LINE(f_write, '        <wdtf:observationAxis xlink:href="urn:ogc:def:axis:EPSG::9916" xlink:title="Height influenced by the Earths gravity field, positive upwards." />');
              UTL_FILE.PUT_LINE(f_write, '        <wdtf:sensor xlink:href="urn:ogc:def:nil:OGC::unknown" />');
              UTL_FILE.PUT_LINE(f_write, '      </wdtf:GroundWaterLevelMethod>');
              UTL_FILE.PUT_LINE(f_write, '    </wdtf:procedureMember>');
            END IF;
          
          END IF;
          EXIT WHEN v_index = t_waterlvl.LAST;
          v_index := t_waterlvl.NEXT(v_index);
        END LOOP;
      END IF;
      
      --then loop through the table again to output the observationMember tags
        --bare in mind when the pipe and vdatum_id changes to then create a new observationMember tag block
      
--      v_count := 1;   --set the count for use when iterating through the table
      
      --FOR i IN t_waterlvl.FIRST..t_waterlvl.LAST LOOP
      IF t_waterlvl.FIRST IS NOT NULL THEN
        v_index := t_waterlvl.FIRST;
        LOOP
          --EXIT WHEN v_count > t_waterlvl.LAST;
          v_currpipe := t_waterlvl(v_index).pipe;
          v_currgwlm := t_waterlvl(v_index).gwlm_id;
        
          UTL_FILE.PUT_LINE(f_write, '    <wdtf:observationMember>');
          UTL_FILE.PUT_LINE(f_write, '      <wdtf:TimeSeriesObservation gml:id="ts'||v_count||'">');
          UTL_FILE.PUT_LINE(f_write, '        <gml:description>Reading using DTW (Depth To Water) from TOC</gml:description>');
          UTL_FILE.PUT_LINE(f_write, '        <gml:name codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/feature/TimeSeriesObservation/w00066/'||t_waterlvl(v_index).rn||'/'||t_waterlvl(v_index).pipe||'/GroundWaterLevel/">'||v_count||'</gml:name>');
          UTL_FILE.PUT_LINE(f_write, '        <om:procedure xlink:href="#'||t_waterlvl(v_index).gwlm_id||'" />');
          UTL_FILE.PUT_LINE(f_write, '        <om:observedProperty xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/property//bom/GroundWaterLevel_m" />');
          UTL_FILE.PUT_LINE(f_write, '        <om:featureOfInterest xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/feature/BorePipeSamplingInterval/w00066/'||t_waterlvl(v_index).rn||'" />');
          UTL_FILE.PUT_LINE(f_write, '        <wdtf:metadata>');
          UTL_FILE.PUT_LINE(f_write, '          <wdtf:TimeSeriesObservationMetadata>');
          UTL_FILE.PUT_LINE(f_write, '            <wdtf:regulationProperty>Reg200806.s3.2a</wdtf:regulationProperty>');
          UTL_FILE.PUT_LINE(f_write, '            <wdtf:status>validated</wdtf:status>');
          UTL_FILE.PUT_LINE(f_write, '          </wdtf:TimeSeriesObservationMetadata>');
          UTL_FILE.PUT_LINE(f_write, '        </wdtf:metadata>');
          UTL_FILE.PUT_LINE(f_write, '        <wdtf:result>');
          UTL_FILE.PUT_LINE(f_write, '          <wdtf:TimeSeries>');
          UTL_FILE.PUT_LINE(f_write, '            <wdtf:defaultInterpolationType>InstVal</wdtf:defaultInterpolationType>');
          UTL_FILE.PUT_LINE(f_write, '            <wdtf:defaultUnitsOfMeasure>'||t_waterlvl(v_index).units||'</wdtf:defaultUnitsOfMeasure>');
          UTL_FILE.PUT_LINE(f_write, '            <wdtf:defaultQuality>quality-A</wdtf:defaultQuality>');

          --FOR j IN v_count..t_waterlvl.LAST LOOP
          v_index2 := v_index;
          LOOP
            EXIT WHEN t_waterlvl(v_index2).pipe <> v_currpipe OR t_waterlvl(v_index2).gwlm_id <> v_currgwlm;
          
            --start inserting the time value pairs with the time as an attribute
            v_time_value := '<wdtf:timeValuePair time="'||TO_CHAR(t_waterlvl(v_index2).rdate,'yyyy-mm-dd')||'T'||TO_CHAR(t_waterlvl(v_index2).rdate,'hh:mi:ss')||'+10:00"';
          
            -- there is room for a remark so these are also added as attributes however only if there is a remark to begin with
            CASE t_waterlvl(v_index2).remark
              WHEN 'P'
                THEN v_time_value := v_time_value ||' observationConditions="http://www.bom.gov.au/std/water/xml/wio0.2/procedure/ObservationConditions/bom/pumping"';
              WHEN 'D'
                THEN v_time_value := v_time_value ||' observationConditions="http://www.bom.gov.au/std/water/xml/wio0.2/procedure/ObservationConditions/bom/dryBore"';
              ELSE NULL;
            END CASE;
          
            --there are no other attributes so now we add the value to the string then insert into the xml file and loop through the other water levels.
            v_time_value := v_time_value ||'>'||t_waterlvl(v_index2).measurement||'</wdtf:timeValuePair>';
            
            t_waterlvl(v_index2).written := 1;  --this is to provide the proper escape clause
            
            UTL_FILE.PUT_LINE(f_write, '            '||v_time_value);
            --v_count := v_count + 1;
            EXIT WHEN v_index2 = t_waterlvl.LAST;
            v_index2 := t_waterlvl.NEXT(v_index2);
          
          END LOOP;
          
          v_index := v_index2;
          v_count := v_count + 1;
        
          UTL_FILE.PUT_LINE(f_write, '          </wdtf:TimeSeries>');
          UTL_FILE.PUT_LINE(f_write, '        </wdtf:result>');
          UTL_FILE.PUT_LINE(f_write, '      </wdtf:TimeSeriesObservation>');
          UTL_FILE.PUT_LINE(f_write, '    </wdtf:observationMember>');
          
          --need to add up all the written fields and compare to t_waterlvl.COUNT
          IF t_waterlvl(v_index).written = 1 THEN
            EXIT WHEN v_index = t_waterlvl.LAST;
            v_index := t_waterlvl.NEXT(v_index);
          END IF;
        
        END LOOP;
      END IF;
      

    ELSE
      --else there are no elevations in AHD so no definitionMember tags
      
      xml_casing(p_rn, f_write, udate, p_datum_done);
      
      
      FOR r_wlvdets IN c_wlvdets LOOP
      
        v_index := TO_NUMBER(TO_CHAR(r_wlvdets.rdate,'YYYYMMDD'));
        
        IF t_waterlvl.EXISTS(v_index) THEN
          NULL;
        ELSE
          --else an entry does not exist and needs to be entered
          t_waterlvl(v_index).rn := r_wlvdets.rn;
          t_waterlvl(v_index).pipe := r_wlvdets.pipe;
          t_waterlvl(v_index).rdate := r_wlvdets.rdate;
          t_waterlvl(v_index).meas_point := r_wlvdets.meas_point;
          t_waterlvl(v_index).measurement := r_wlvdets.measurement;
          t_waterlvl(v_index).units := 'm';
          t_waterlvl(v_index).remark := r_wlvdets.remark;
          t_waterlvl(v_index).logger := r_wlvdets.logger;
          --t_waterlvl(v_index).edate := v_elev_row.rdate;
          --t_waterlvl(v_index).elevation := v_elev_row.elevation;
          --t_waterlvl(v_index).vdatum_id := 'ObservationDatum'||r_wlvdets.rn||'-'||i;
          --t_waterlvl(v_index).gwlm_id := 'gwMethod'||r_wlvdets.rn||'-'||i;
              
          
        END IF;
      END LOOP;
      
      FOR r_pumtes IN c_pumtes LOOP
        
        v_index := TO_NUMBER(TO_CHAR(r_pumtes.rdate,'YYYYMMDD'));
        
        IF t_waterlvl.EXISTS(v_index) THEN
          NULL;
        ELSE
          --else an entry does not already exist and needs to be created
              
          t_waterlvl(v_index).rn := r_pumtes.rn;
          t_waterlvl(v_index).pipe := r_pumtes.pipe;
          t_waterlvl(v_index).rdate := r_pumtes.rdate;
          t_waterlvl(v_index).meas_point := 'R';
          t_waterlvl(v_index).measurement := r_pumtes.swl;
          t_waterlvl(v_index).units := 'm';
          --t_waterlvl(v_index).remark := NULL;
          --t_waterlvl(v_index).logger := NULL;
          --t_waterlvl(v_index).edate := v_elev_row.rdate;
          --t_waterlvl(v_index).elevation := v_elev_row.elevation;
          --t_waterlvl(v_index).vdatum_id := 'ObservationDatum'||r_pumtes.rn||'-'||i;
          --t_waterlvl(v_index).gwlm_id := 'gwMethod'||r_pumtes.rn||'-'||i;
              
          
        END IF;
      END LOOP;
      
      
      IF t_waterlvl.FIRST IS NOT NULL THEN
        v_index := t_waterlvl.FIRST;
        
        --program has to refer to TOC for observation point
        UTL_FILE.PUT_LINE(f_write, '    <wdtf:procedureMember>');
        UTL_FILE.PUT_LINE(f_write, '      <wdtf:GroundWaterLevelMethod gml:id="gwTOC'||p_rn||'">');
        UTL_FILE.PUT_LINE(f_write, '        <wdtf:observationPointDatum xlink:href="urn:ogc:def:datum:bom::TopOfCasing" xlink:title="top of casing" />'); --as you can see here
        UTL_FILE.PUT_LINE(f_write, '        <wdtf:observationAxis xlink:href="urn:ogc:def:axis:EPSG::9916" xlink:title="Height influenced by the Earths gravity field, positive upwards." />');
        UTL_FILE.PUT_LINE(f_write, '        <wdtf:sensor xlink:href="urn:ogc:def:nil:OGC::unknown" />');
        UTL_FILE.PUT_LINE(f_write, '      </wdtf:GroundWaterLevelMethod>');
        UTL_FILE.PUT_LINE(f_write, '    </wdtf:procedureMember>');

        LOOP
          v_currpipe := t_waterlvl(v_index).pipe;
        
          UTL_FILE.PUT_LINE(f_write, '    <wdtf:observationMember>');
          UTL_FILE.PUT_LINE(f_write, '      <wdtf:TimeSeriesObservation gml:id="ts'||v_count||'">');
          UTL_FILE.PUT_LINE(f_write, '        <gml:description>Reading using DTW (Depth To Water) from TOC</gml:description>');
          UTL_FILE.PUT_LINE(f_write, '        <gml:name codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/feature/TimeSeriesObservation/w00066/'||t_waterlvl(v_index).rn||'/'||t_waterlvl(v_index).pipe||'/GroundWaterLevel/">1</gml:name>');
          UTL_FILE.PUT_LINE(f_write, '        <om:procedure xlink:href="#gwTOC'||t_waterlvl(v_index).rn||'" />');
          UTL_FILE.PUT_LINE(f_write, '        <om:observedProperty xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/property//bom/GroundWaterLevel_m" />');
          UTL_FILE.PUT_LINE(f_write, '        <om:featureOfInterest xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/feature/BorePipeSamplingInterval/w00066/'||t_waterlvl(v_index).rn||'" />');
          UTL_FILE.PUT_LINE(f_write, '        <wdtf:metadata>');
          UTL_FILE.PUT_LINE(f_write, '          <wdtf:TimeSeriesObservationMetadata>');
          UTL_FILE.PUT_LINE(f_write, '            <wdtf:regulationProperty>Reg200806.s3.2a</wdtf:regulationProperty>');
          UTL_FILE.PUT_LINE(f_write, '            <wdtf:status>validated</wdtf:status>');
          UTL_FILE.PUT_LINE(f_write, '          </wdtf:TimeSeriesObservationMetadata>');
          UTL_FILE.PUT_LINE(f_write, '        </wdtf:metadata>');
          UTL_FILE.PUT_LINE(f_write, '        <wdtf:result>');
          UTL_FILE.PUT_LINE(f_write, '          <wdtf:TimeSeries>');
          UTL_FILE.PUT_LINE(f_write, '            <wdtf:defaultInterpolationType>InstVal</wdtf:defaultInterpolationType>');
          UTL_FILE.PUT_LINE(f_write, '            <wdtf:defaultUnitsOfMeasure>'||t_waterlvl(v_index).units||'</wdtf:defaultUnitsOfMeasure>');
          UTL_FILE.PUT_LINE(f_write, '            <wdtf:defaultQuality>quality-A</wdtf:defaultQuality>');

          v_index2 := v_index;
          LOOP
            EXIT WHEN t_waterlvl(v_index2).pipe <> v_currpipe;
            --start inserting the time value pairs with the time as an attribute
            v_time_value := '<wdtf:timeValuePair time="'||TO_CHAR(t_waterlvl(v_index2).rdate,'yyyy-mm-dd')||'T'||TO_CHAR(t_waterlvl(v_index2).rdate,'hh:mi:ss')||'+10:00"';

            -- there is room for a remark so these are also added as attributes however only if there is a remark to begin with
            CASE t_waterlvl(v_index2).remark
              WHEN 'P'
                THEN v_time_value := v_time_value ||' observationConditions="http://www.bom.gov.au/std/water/xml/wio0.2/procedure/ObservationConditions/bom/pumping"';
              WHEN 'D'
                THEN v_time_value := v_time_value ||' observationConditions="http://www.bom.gov.au/std/water/xml/wio0.2/procedure/ObservationConditions/bom/dryBore"';
              ELSE NULL;
            END CASE;

          --there are no other attributes so now we add the value to the string then insert into the xml file and loop through the other water levels.
            v_time_value := v_time_value ||'>'||t_waterlvl(v_index2).measurement||'</wdtf:timeValuePair>';
            
            t_waterlvl(v_index2).written := 1;  --this is to provide the proper escape clause
            
            UTL_FILE.PUT_LINE(f_write, '            '||v_time_value);
            
            EXIT WHEN v_index2 = t_waterlvl.LAST;
            v_index2 := t_waterlvl.NEXT(v_index2);
          
          END LOOP;
          
          v_index := v_index2;
          v_count := v_count + 1;

          UTL_FILE.PUT_LINE(f_write, '          </wdtf:TimeSeries>');
          UTL_FILE.PUT_LINE(f_write, '        </wdtf:result>');
          UTL_FILE.PUT_LINE(f_write, '      </wdtf:TimeSeriesObservation>');
          UTL_FILE.PUT_LINE(f_write, '    </wdtf:observationMember>');
          UTL_FILE.FFLUSH(f_write);
        
          IF t_waterlvl(v_index).written = 1 THEN
            EXIT WHEN v_index = t_waterlvl.LAST;
            v_index := t_waterlvl.NEXT(v_index);
          END IF;
        
        END LOOP;
      
      END IF;
    
    END IF;
    
  END IF;

END xml_waterlvls;


/*********************************************************************
 * MODULE:	xml_casing
 * PURPOSE:	This procedure is the code that creates the casing
 *		     information for WDTFv1.0.
 * RETURNS:	nothing
 * NOTES:       inserts the wdtf:BorePipeSamplingInterval and gml:VerticalCRS tags
 *              into the current xml file.
 *********************************************************************/
PROCEDURE xml_casing(p_rn IN NUMBER, f_write IN UTL_FILE.FILE_TYPE, udate IN DATE, p_datum_done IN OUT BOOLEAN) IS

  CURSOR c_casing (pip VARCHAR2) IS
    SELECT  r.gis_lat, r.gis_lng, MIN(ca.top), MAX(ca.bottom)
    FROM gw_casings ca, gw_regdets r
    WHERE r.rn = ca.rn
    AND ca.material_desc IN ('SCRN','PERF')
    AND ca.updated_date > udate
    AND r.rn = p_rn
    AND ca.pipe = pip
    GROUP BY r.gis_lat, r.gis_lng;

  CURSOR c_elvdets IS
    SELECT rn, pipe, rdate, elevation
    FROM gw_elvdets
    WHERE rn = p_rn
    AND meas_point = 'N'
    AND datum = 'AHD'
    ORDER BY rdate DESC;

  CURSOR c_aquifr (top_min NUMBER, bottom_max NUMBER) IS
    SELECT form_desc
    FROM gw_aquifrs
    WHERE rn = p_rn
    AND top > top_min
    AND bottom < bottom_max;

  CURSOR c_pipes IS
    SELECT DISTINCT pipe
    FROM gw_casings
    WHERE rn = p_rn
    AND pipe <> 'X';

  CURSOR c_check IS
    SELECT DISTINCT aq.rn
    FROM gw_aquifrs aq, gw_casings ca
    WHERE aq.rn = ca.rn
    AND aq.rn = p_rn
    AND ca.material_desc IN ('SCRN','PERF')
    AND (aq.updated_date > udate
      OR ca.updated_date > udate)
    AND aq.form_desc IS NOT NULL
    AND aq.top IS NOT NULL
    AND aq.bottom IS NOT NULL
    AND ca.top IS NOT NULL
    AND ca.bottom IS NOT NULL;

  v_rn            gw_regdets.rn%TYPE;
  v_pipe          gw_elvdets.pipe%TYPE;
  v_edate         gw_elvdets.rdate%TYPE;
  v_elevation     gw_elvdets.elevation%TYPE;
  v_lat           gw_regdets.gis_lat%TYPE;
  v_long          gw_regdets.gis_lng%TYPE;
  v_cas_top       gw_casings.top%TYPE;
  v_cas_bottom    gw_casings.bottom%TYPE;
  v_form_desc     gw_aquifrs.form_desc%TYPE;
  v_aquifer_count   NUMBER(2);


BEGIN

  --need to get the casing 'SCRN' or 'PERF'
  --also the location of the bore from regdets
  --and the aquifer details
  --if any of this is unavailable then cannot include this portion of xml
  OPEN c_check;
  FETCH c_check INTO v_rn;
  IF c_check%FOUND THEN

    IF p_datum_done THEN
    
      OPEN c_elvdets;
      FETCH c_elvdets INTO v_rn, v_pipe, v_edate, v_elevation;

      --need a verticalCRS set to natural surface.  First need to check if there is a natural surface elevation in AHD.
      IF c_elvdets%FOUND THEN

        --if there is then write a vertical datum and verticalCRS
        UTL_FILE.PUT_LINE(f_write, '    <wdtf:definitionMember>');
        UTL_FILE.PUT_LINE(f_write, '      <gml:VerticalDatum gml:id="nsDatum'||v_rn||'">');
        UTL_FILE.PUT_LINE(f_write, '        <gml:datumName>urn:ogc:def:datum:bom::NaturalSurfaceLevel</gml:datumName>');
        UTL_FILE.PUT_LINE(f_write, '        <gml:anchorPoint codeSpace="urn:ogc:def:crs:EPSG::5711">'||v_elevation||'</gml:anchorPoint>');
        UTL_FILE.PUT_LINE(f_write, '      </gml:VerticalDatum>');
        UTL_FILE.PUT_LINE(f_write, '    </wdtf:definitionMember>');

        UTL_FILE.PUT_LINE(f_write, '    <wdtf:definitionMember>');
        UTL_FILE.PUT_LINE(f_write, '      <gml:VerticalCRS gml:id="nsCRS'||v_rn||'">');
        UTL_FILE.PUT_LINE(f_write, '        <gml:srsName />');
        UTL_FILE.PUT_LINE(f_write, '        <gml:usesVerticalCS xlink:href="urn:ogc:def:cs:EPSG::6498" xlink:title="Vertical CS. Axis: depth (D). Orientation: down. UoM: m." />');
        UTL_FILE.PUT_LINE(f_write, '        <gml:usesVerticalDatum xlink:href="#nsDatum'||v_rn||'" />');
        UTL_FILE.PUT_LINE(f_write, '      </gml:VerticalCRS>');
        UTL_FILE.PUT_LINE(f_write, '    </wdtf:definitionMember>');

      ELSE
        --if not then write a verticalCRS to reference natural surface
        UTL_FILE.PUT_LINE(f_write, '    <wdtf:definitionMember>');
        UTL_FILE.PUT_LINE(f_write, '      <gml:VerticalCRS gml:id="nsCRS'||v_rn||'">');
        UTL_FILE.PUT_LINE(f_write, '        <gml:srsName />');
        UTL_FILE.PUT_LINE(f_write, '        <gml:usesVerticalCS xlink:href="urn:ogc:def:cs:EPSG::6498" xlink:title="Vertical CS. Axis: depth (D). Orientation: down. UoM: m." />');
        UTL_FILE.PUT_LINE(f_write, '        <gml:usesVerticalDatum xlink:href="urn:ogc:def:datum:bom::NaturalSurfaceLevel" />');
        UTL_FILE.PUT_LINE(f_write, '      </gml:VerticalCRS>');
        UTL_FILE.PUT_LINE(f_write, '    </wdtf:definitionMember>');

      END IF;
      CLOSE c_elvdets;
    
    

    --because there may be more than one aquifer for this bore need to be sure to get the correct one for the pipe.
    SELECT COUNT(rn)
    INTO v_aquifer_count
    FROM gw_aquifrs
    WHERE rn = p_rn
    AND form_desc IS NOT NULL
    AND top IS NOT NULL
    AND bottom IS NOT NULL;

    --then write the BorePipeSamplingInterval
    --need to know the number of pipes to loop through
    --for each pipe
    OPEN c_pipes;
    FETCH c_pipes INTO v_pipe;
    LOOP

      --need to get the location
      --the screen/perforation min(top) and max(bottom) for the current pipe
      OPEN c_casing (v_pipe);
      FETCH c_casing INTO v_lat, v_long, v_cas_top, v_cas_bottom;
      CLOSE c_casing;


      IF v_aquifer_count = 1 THEN
        --if there is only one suitable aquifer then that is the formation name used for all pipes
        SELECT form_desc
        INTO v_form_desc
        FROM gw_aquifrs
        WHERE rn = p_rn
        AND top IS NOT NULL
        AND bottom IS NOT NULL
        AND form_desc IS NOT NULL;

      ELSE
        --else there are at least 2 suitable aquifer entries and will need to separate out which is the correct one for the current pipe.
        --therefore loop through them until find the closest top and/or bottom to the current pipe
        FOR i IN 0..30 LOOP
          OPEN c_aquifr (v_cas_top-i,v_cas_bottom+i);
          FETCH c_aquifr INTO v_form_desc;
          EXIT WHEN c_aquifr%FOUND;

          CLOSE c_aquifr;

        END LOOP;
        IF c_aquifr%ISOPEN THEN
          CLOSE c_aquifr;
        END IF;

      END IF;

      --all data found now so write the borePipeSamplingInterval xml block now
      UTL_FILE.PUT_LINE(f_write, '    <wdtf:siteMember>');
      UTL_FILE.PUT_LINE(f_write, '      <wdtf:BorePipeSamplingInterval gml:id="SI'||v_rn||'-'||v_pipe||'">');
      UTL_FILE.PUT_LINE(f_write, '        <gml:name codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/feature/BorePipeSamplingInterval/w00066/">'||v_rn||'-'||v_pipe||'</gml:name>');
      UTL_FILE.PUT_LINE(f_write, '        <sa:sampledFeature xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/feature/Aquifer/w00066/'||v_form_desc||'" />');
      UTL_FILE.PUT_LINE(f_write, '        <sa:relatedSamplingFeature xlink:arcrole="http://www.bom.gov.au/std/water/xml/wio0.2/definition/arcrole/bom/parent" xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/feature/SamplingGroup/w00066/'||v_rn||'" />');
      UTL_FILE.PUT_LINE(f_write, '        <sa:relatedSamplingFeature xlink:href="#RN'||v_rn||'" xlink:arcrole="http://www.bom.gov.au/std/water/xml/wio0.2/definition/arcrole/bom/enclosure"/>');
      UTL_FILE.PUT_LINE(f_write, '        <sa:shape>');
      UTL_FILE.PUT_LINE(f_write, '          <gml:LineString gml:id="LS'||v_rn||'-'||v_pipe||'" srsName="#nsCRS'||v_rn||'">');
      UTL_FILE.PUT_LINE(f_write, '            <gml:posList>'||v_cas_top||' '||v_cas_bottom||'</gml:posList>');
      UTL_FILE.PUT_LINE(f_write, '          </gml:LineString>');
      UTL_FILE.PUT_LINE(f_write, '        </sa:shape>');
      UTL_FILE.PUT_LINE(f_write, '        <wdtf:timeZone>+10:00</wdtf:timeZone>');
      UTL_FILE.PUT_LINE(f_write, '        <wdtf:position>');
      UTL_FILE.PUT_LINE(f_write, '          <gml:Point srsName="urn:ogc:def:crs:EPSG::4283">');
      UTL_FILE.PUT_LINE(f_write, '           <gml:pos>'||v_lat||' '||v_long||'</gml:pos>');
      UTL_FILE.PUT_LINE(f_write, '          </gml:Point>');
      UTL_FILE.PUT_LINE(f_write, '        </wdtf:position>');
      UTL_FILE.PUT_LINE(f_write, '      </wdtf:BorePipeSamplingInterval>');
      UTL_FILE.PUT_LINE(f_write, '    </wdtf:siteMember>');

      FETCH c_pipes INTO v_pipe;
      EXIT WHEN c_pipes%NOTFOUND;
    END LOOP;
    CLOSE c_pipes;
    
    p_datum_done := FALSE;
    END IF;--if p_datum_done

  END IF;
  CLOSE c_check;

END xml_casing;


/*********************************************************************
 * MODULE:	xml_quality
 * PURPOSE:	This procedure is the code that creates the water quality
 *		     information for WDTFv1.0.
 * RETURNS:	nothing
 * NOTES:       inserts the wdtf:Measurement tag into the current xml file.
 *********************************************************************/
PROCEDURE xml_quality(p_rn IN NUMBER, f_write IN UTL_FILE.FILE_TYPE, udate IN DATE) IS

  CURSOR c_check IS
    SELECT DISTINCT fi.rn
    FROM gw_fieldqs fi, gw_watanls wa
    WHERE fi.rn = wa.rn
    AND fi.rn = p_rn
    AND fi.pipe <> 'X'
    AND (fi.updated_date > udate
      OR wa.updated_date > udate)
    AND fi.conduct IS NOT NULL
    AND wa.conduct IS NOT NULL;

  CURSOR c_field IS
    SELECT pipe, rdate, conduct
    FROM gw_fieldqs
    WHERE rn = p_rn
    AND conduct IS NOT NULL
    AND pipe <> 'X'
    AND updated_date > udate
    ORDER BY rdate;

  CURSOR c_lab IS
    SELECT pipe, rdate, conduct, analysis_no
    FROM gw_watanls
    WHERE rn = p_rn
    AND conduct IS NOT NULL
    AND pipe <> 'X'
    AND updated_date > udate
    ORDER BY rdate;

  TYPE conduct_rec IS RECORD (
    rn              gw_regdets.rn%TYPE,
    pipe            gw_fieldqs.pipe%TYPE,
    rdate           gw_fieldqs.rdate%TYPE,
    conduct         gw_fieldqs.conduct%TYPE,
    analysis        gw_watanls.analysis_no%TYPE,
    area            VARCHAR2(1));

  TYPE conduct_type IS TABLE OF
    conduct_rec
    INDEX BY PLS_INTEGER;

  t_conduct       conduct_type;
  v_rn            gw_regdets.rn%TYPE;
  v_index         NUMBER(10);
  v_pip_id        NUMBER(1);
  v_count         NUMBER := 1;


BEGIN

  --first check to be sure that there is conductivity readings in either the lab and/or field tables
  OPEN c_check;
  FETCH c_check INTO v_rn;
  IF c_check%FOUND THEN

    --get all the watanls data and add to working table
    FOR r_lab IN c_lab LOOP
      
      v_pip_id := CASE r_lab.pipe
                    WHEN 'A' THEN 1
                    WHEN 'B' THEN 2
                    WHEN 'C' THEN 3
                    WHEN 'D' THEN 4
                    WHEN 'E' THEN 5
                    WHEN 'F' THEN 6
                    ELSE 8
                  END;
      --insert the data into the table
      t_conduct(v_pip_id||TO_CHAR(r_lab.rdate,'yyyymmdd')).rn := v_rn;
      t_conduct(v_pip_id||TO_CHAR(r_lab.rdate,'yyyymmdd')).pipe := r_lab.pipe;
      t_conduct(v_pip_id||TO_CHAR(r_lab.rdate,'yyyymmdd')).rdate := r_lab.rdate;
      t_conduct(v_pip_id||TO_CHAR(r_lab.rdate,'yyyymmdd')).conduct := r_lab.conduct;
      t_conduct(v_pip_id||TO_CHAR(r_lab.rdate,'yyyymmdd')).analysis := r_lab.analysis_no;
      t_conduct(v_pip_id||TO_CHAR(r_lab.rdate,'yyyymmdd')).area := 'L';

    END LOOP;

    --get all the fieldqs data and compare with current lab data.
    --if a lab measurement exists for the same date then do not insert
    FOR r_field IN c_field LOOP
      
      v_pip_id := CASE r_field.pipe
                    WHEN 'A' THEN 1
                    WHEN 'B' THEN 2
                    WHEN 'C' THEN 3
                    WHEN 'D' THEN 4
                    WHEN 'E' THEN 5
                    WHEN 'F' THEN 6
                    ELSE 8
                  END;
      
      IF t_conduct.EXISTS(v_pip_id||TO_CHAR(r_field.rdate,'yyyymmdd')) THEN
        --if there is already a lab measurement for that day and pipe then do nothing
        NULL;
      ELSE
        
        --insert the data into the table
        t_conduct(v_pip_id||TO_CHAR(r_field.rdate,'yyyymmdd')).rn := v_rn;
        t_conduct(v_pip_id||TO_CHAR(r_field.rdate,'yyyymmdd')).pipe := r_field.pipe;
        t_conduct(v_pip_id||TO_CHAR(r_field.rdate,'yyyymmdd')).rdate := r_field.rdate;
        t_conduct(v_pip_id||TO_CHAR(r_field.rdate,'yyyymmdd')).conduct := r_field.conduct;
        t_conduct(v_pip_id||TO_CHAR(r_field.rdate,'yyyymmdd')).analysis := NULL;
        t_conduct(v_pip_id||TO_CHAR(r_field.rdate,'yyyymmdd')).area := 'F';
      END IF;

    END LOOP;
    
    v_index := t_conduct.FIRST;

    --now that all relevant data has been entered into the working table need to output that into the xml format
    LOOP

      UTL_FILE.PUT_LINE(f_write, '    <wdtf:observationMember>');
      UTL_FILE.PUT_LINE(f_write, '      <wdtf:Measurement gml:id="gwCond'||t_conduct(v_index).rn||'-'||v_count||'">');
      v_count := v_count + 1;

      --some output depends on if it is lab or field measured
      IF t_conduct(v_index).area = 'F' THEN
        --if field measurement then
        UTL_FILE.PUT_LINE(f_write, '        <gml:description>an insitu site measurement taken at the bore</gml:description>');--a description of field measurement
        UTL_FILE.PUT_LINE(f_write, '        <gml:name codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/feature/Measurement/w00066/">'||t_conduct(v_index).rn||'/'||t_conduct(v_index).pipe||'</gml:name>');--uses the form rn/?
        UTL_FILE.PUT_LINE(f_write, '        <om:samplingTime>'||TO_CHAR(t_conduct(v_index).rdate,'YYYY-MM-DD')||'T'||TO_CHAR(t_conduct(v_index).rdate,'HH24:MI:SS')||'+10:00</om:samplingTime>');--date/time of sample
        UTL_FILE.PUT_LINE(f_write, '        <om:procedure xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/procedure/QualityMethod/w00066/eMeter" />');--procedure used for field measurement
      ELSE
        --else it is a lab measurement
        UTL_FILE.PUT_LINE(f_write, '        <gml:description>a sample taken from the bore and sent to QHFSS lab</gml:description>');--a description of a lab measurement
        UTL_FILE.PUT_LINE(f_write, '        <gml:name codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/feature/Measurement/w00066/">'||t_conduct(v_index).rn||'/'||t_conduct(v_index).pipe||'/'||t_conduct(v_index).analysis||'</gml:name>');--uses the form rn/analysis_no
        UTL_FILE.PUT_LINE(f_write, '        <om:samplingTime>'||TO_CHAR(t_conduct(v_index).rdate,'YYYY-MM-DD')||'T'||TO_CHAR(t_conduct(v_index).rdate,'HH24:MI:SS')||'+10:00</om:samplingTime>');--date/time of sample
        UTL_FILE.PUT_LINE(f_write, '        <om:procedure xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/procedure/QualityMethod/w00066/electroChemistry" />');--procedure used for lab measurement

      END IF;
      UTL_FILE.PUT_LINE(f_write, '        <om:observedProperty xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/property//bom/ElectricalConductivityAt25C_uScm" xlink:title="Electrical Conductivity @ 25C" />');
      UTL_FILE.PUT_LINE(f_write, '        <om:featureOfInterest xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/feature//w00066/'||t_conduct(v_index).rn||'" />');
      UTL_FILE.PUT_LINE(f_write, '        <wdtf:metadata>');
      UTL_FILE.PUT_LINE(f_write, '          <wdtf:MeasurementObservationMetadata>');
      UTL_FILE.PUT_LINE(f_write, '            <wdtf:regulationProperty>Reg200806.s3.9b</wdtf:regulationProperty>');
      UTL_FILE.PUT_LINE(f_write, '          </wdtf:MeasurementObservationMetadata>');
      UTL_FILE.PUT_LINE(f_write, '        </wdtf:metadata>');
      UTL_FILE.PUT_LINE(f_write, '        <wdtf:result uom="uS/cm">'||t_conduct(v_index).conduct||'</wdtf:result>');
      UTL_FILE.PUT_LINE(f_write, '      </wdtf:Measurement>');
      UTL_FILE.PUT_LINE(f_write, '    </wdtf:observationMember>');
      
      EXIT WHEN v_index = t_conduct.LAST;
      v_index := t_conduct.NEXT(v_index);
    END LOOP;

  END IF;
  CLOSE c_check;

END xml_quality;

/*********************************************************************
 * MODULE:	xml_elevation
 * PURPOSE:	This procedure is the code that creates the elevation
 *		  information for casing and water levels in WDTFv1.0.
 * RETURNS:	nothing
 * NOTES:       inserts the gml:VerticalDatum tags into the current xml file.
 *********************************************************************/
PROCEDURE xml_elevation(p_rn IN NUMBER, f_write IN UTL_FILE.FILE_TYPE, udate IN DATE, p_datum_done IN OUT BOOLEAN) IS
  
  
  CURSOR c_casing_elvdets IS
    SELECT rn, pipe, rdate, elevation
    FROM gw_elvdets
    WHERE rn = p_rn
    AND meas_point = 'N'
    AND datum = 'AHD'
    ORDER BY rdate DESC;
  
  CURSOR c_casing_check IS
    SELECT DISTINCT aq.rn
    FROM gw_aquifrs aq, gw_casings ca
    WHERE aq.rn = ca.rn
    AND aq.rn = p_rn
    AND ca.material_desc IN ('SCRN','PERF')
    AND (aq.updated_date > udate
      OR ca.updated_date > udate)
    AND aq.form_desc IS NOT NULL
    AND aq.top IS NOT NULL
    AND aq.bottom IS NOT NULL
    AND ca.top IS NOT NULL
    AND ca.bottom IS NOT NULL;

  v_rn            gw_regdets.rn%TYPE;
  v_pipe          gw_elvdets.pipe%TYPE;
  v_edate         gw_elvdets.rdate%TYPE;
  v_elevation     gw_elvdets.elevation%TYPE;
  
  
  
  
BEGIN
  
  
  OPEN c_casing_check;
  FETCH c_casing_check INTO v_rn;
  IF c_casing_check%FOUND THEN

    OPEN c_casing_elvdets;
    FETCH c_casing_elvdets INTO v_rn, v_pipe, v_edate, v_elevation;

    --need a verticalCRS set to natural surface.  First need to check if there is a natural surface elevation in AHD.
    IF c_casing_elvdets%FOUND THEN

      --if there is then write a vertical datum and verticalCRS
      UTL_FILE.PUT_LINE(f_write, '    <wdtf:definitionMember>');
      UTL_FILE.PUT_LINE(f_write, '      <gml:VerticalDatum gml:id="nsDatum'||v_rn||'">');
      UTL_FILE.PUT_LINE(f_write, '        <gml:datumName>urn:ogc:def:datum:bom::NaturalSurfaceLevel</gml:datumName>');
      UTL_FILE.PUT_LINE(f_write, '        <gml:anchorPoint codeSpace="urn:ogc:def:crs:EPSG::5711">'||v_elevation||'</gml:anchorPoint>');
      UTL_FILE.PUT_LINE(f_write, '      </gml:VerticalDatum>');
      UTL_FILE.PUT_LINE(f_write, '    </wdtf:definitionMember>');

      UTL_FILE.PUT_LINE(f_write, '    <wdtf:definitionMember>');
      UTL_FILE.PUT_LINE(f_write, '      <gml:VerticalCRS gml:id="nsCRS'||v_rn||'">');
      UTL_FILE.PUT_LINE(f_write, '        <gml:srsName />');
      UTL_FILE.PUT_LINE(f_write, '        <gml:usesVerticalCS xlink:href="urn:ogc:def:cs:EPSG::6498" xlink:title="Vertical CS. Axis: depth (D). Orientation: down. UoM: m." />');
      UTL_FILE.PUT_LINE(f_write, '        <gml:usesVerticalDatum xlink:href="#nsDatum'||v_rn||'" />');
      UTL_FILE.PUT_LINE(f_write, '      </gml:VerticalCRS>');
      UTL_FILE.PUT_LINE(f_write, '    </wdtf:definitionMember>');

    ELSE
      --if not then write a verticalCRS to reference natural surface
      UTL_FILE.PUT_LINE(f_write, '    <wdtf:definitionMember>');
      UTL_FILE.PUT_LINE(f_write, '      <gml:VerticalCRS gml:id="nsCRS'||v_rn||'">');
      UTL_FILE.PUT_LINE(f_write, '        <gml:srsName />');
      UTL_FILE.PUT_LINE(f_write, '        <gml:usesVerticalCS xlink:href="urn:ogc:def:cs:EPSG::6498" xlink:title="Vertical CS. Axis: depth (D). Orientation: down. UoM: m." />');
      UTL_FILE.PUT_LINE(f_write, '        <gml:usesVerticalDatum xlink:href="urn:ogc:def:datum:bom::NaturalSurfaceLevel" />');
      UTL_FILE.PUT_LINE(f_write, '      </gml:VerticalCRS>');
      UTL_FILE.PUT_LINE(f_write, '    </wdtf:definitionMember>');

    END IF;
    CLOSE c_casing_elvdets;

  p_datum_done := FALSE;
  
  END IF;
  
  
END xml_elevation;


/*********************************************************************
 * MODULE:	xml_update2
 * PURPOSE:	This procedure is the code for the incrimental data update
 *		      BoM receive in their xml WDTFv1.0.2.
 * RETURNS:	nothing
 * NOTES:   creates several xml files that need to be zipped and sent
 *          to BoM via their ftp site.
 *********************************************************************/
PROCEDURE xml_update2 IS

  CURSOR c_rn (udate DATE) IS
    SELECT rn
    FROM gw_regdets
    WHERE rn IN (42231059,44701,99067,109106,11620,3131,14310263)
    ORDER BY rn;
  
  CURSOR c_rns (udate DATE) IS
    SELECT rn
    FROM gw_regdets
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE rn IN (
        SELECT rn
        FROM gw_pumtes
        WHERE swl > 0
      )
      AND facility_type <> 'SF'
      UNION
      SELECT DISTINCT rn
      FROM gw_elvdets
      WHERE rn IN (
        SELECT rn
        FROM gw_wlvdets
      )
    )
    AND gis_lat IS NOT NULL
    AND created_date > udate
    UNION
    SELECT rn
    FROM gw_wlvdets
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND rn IN (
        SELECT rn
        FROM gw_elvdets
      )
    )
    AND updated_date > udate
    UNION
    SELECT rn
    FROM gw_elvdets
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND rn IN (
        SELECT rn
        FROM gw_wlvdets
      )
    )
    AND updated_date > udate
    UNION
    SELECT rn
    FROM gw_pumtes
    WHERE swl > 0
    AND rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND facility_type <> 'SF'
    )
    AND updated_date > udate
    UNION
    SELECT rn
    FROM gw_aquifrs
    WHERE rn IN (
      SELECT rn
      FROM gw_pumtes
      WHERE swl > 0
      AND rn IN (
        SELECT rn
        FROM gw_regdets
        WHERE gis_lat IS NOT NULL
      	AND facility_type <> 'SF'
      )
    )
    AND updated_date > udate
    UNION
    SELECT rn
    FROM gw_fieldqs
    WHERE rn IN (
      SELECT rn
      FROM gw_regdets
      WHERE gis_lat IS NOT NULL
      AND rn IN (
        SELECT rn
        FROM gw_wlvdets
      )
    )
    AND conduct IS NOT NULL
    AND updated_date > udate;

  CURSOR c_wl (reg NUMBER, udate DATE) IS
    SELECT DISTINCT rn
    FROM gw_wlvdets
    WHERE rn = reg
    AND meas_point <> 'N'
    AND updated_date > udate
    UNION
    SELECT DISTINCT rn
    FROM gw_pumtes
    WHERE rn = reg
    AND updated_date > udate;
  
  CURSOR c_wq (reg NUMBER, udate DATE) IS
    SELECT DISTINCT fi.rn
    FROM gw_fieldqs fi, gw_watanls wa
    WHERE fi.rn = wa.rn
    AND fi.rn = reg
    AND fi.pipe <> 'X'
    AND (fi.updated_date > udate
      OR wa.updated_date > udate)
    AND (fi.conduct IS NOT NULL OR fi.ph IS NOT NULL)
    AND (wa.conduct IS NOT NULL OR wa.ph IS NOT NULL);

  
  f_dir         VARCHAR2(20) := 'D:\GW_LOAD\xml_files';
  f_name	VARCHAR2(50);
  f_w_ref       UTL_FILE.FILE_TYPE;
  v_rn      gw_wlvdets.rn%TYPE;
  v_wl_check      BOOLEAN;
  v_index       NUMBER;
  v_index2       NUMBER;
  v_currdatum      VARCHAR2(40);
  v_currgwlm        VARCHAR2(40);
  v_currpipe        gw_wlvdets.pipe%TYPE;
  v_count         NUMBER;
  v_update_date   VARCHAR2(10);
  v_write_datum         BOOLEAN;
  v_write_gwlm         BOOLEAN;
  v_observPoint       VARCHAR2(40); 
  v_datum_id      VARCHAR2(40);
  v_datum_done    BOOLEAN;
  v_wq_check    BOOLEAN;
  v_time_value    VARCHAR2(500);
  v_drilled_date  DATE;
  v_related_ref   VARCHAR2(100);
  v_ref_el        gw_elvdets.elevation%TYPE;
  v_nat_el        gw_elvdets.elevation%TYPE;
  t_waterlvls     waterlvl_type;
  t_waterquals    waterqual_type;

BEGIN

  --get the date of the last update from gw_system_variables
--  SELECT parameter_value
--  INTO v_update_date
--  FROM gw_system_variables
--  WHERE parameter_code = 'BOM_DATA_UPDATE';

  v_update_date := '21/02/2009';

  --first need to get all the RNs with data to go to BOM
  --loop through these rns
  FOR r_rns IN c_rn (TO_DATE(v_update_date,'DD/MM/YYYY')) LOOP

    --create file and generate file name
    f_name := 'wdtf-v1.w00066.'||TO_CHAR(SYSDATE, 'YYYYMMDDHH24MISS')||'.'||r_rns.rn||'.xml';
    f_w_ref := UTL_FILE.FOPEN (f_dir, f_name, 'w');
    
    --reset the boolean tracker for the next rn
    v_datum_done := TRUE;

    --generate the hydrocode and metadata tag information (mandatory)
    xml_header(r_rns.rn, f_w_ref, f_name);
    
    --because of the convoluted way wdtf works this bit of code now looks at each xml code block in the order necessary
    --for a complete wdtf xml file.
    
    --so first grouping after the header information is the definition member tags
    --groundwater just uses vertical datum and vertical CRS
    --these are used for water levels mostly
    
    --need to clear t_waterlvls of current entries for new RN
    IF t_waterlvls.FIRST IS NOT NULL THEN
      t_waterlvls.DELETE;
    END IF;
    
    --*************************
    --using the waterlvl_table type extract the water level and elevation table 
    OPEN c_wl(r_rns.rn, TO_DATE(v_update_date,'DD/MM/YYYY'));
    FETCH c_wl INTO v_rn;
    IF c_wl%FOUND THEN
    
      xml_waterlvl_data(r_rns.rn, TO_DATE(v_update_date,'DD/MM/YYYY'), t_waterlvls);
      
      v_wl_check := TRUE;
      
      v_currdatum := NULL;
      v_write_datum := TRUE;
      --output the definitionMember tags necessary for water levels
      --loop through t_waterlvl for all the definitionMember tags using vdatum_id
      IF t_waterlvls.FIRST IS NOT NULL THEN
        v_index := t_waterlvls.FIRST;
        LOOP
          
          IF t_waterlvls(v_index).vdatum_id IS NOT NULL THEN   --possible new definitionMember tag
            IF v_currdatum IS NULL OR t_waterlvls(v_index).vdatum_id <> v_currdatum THEN
              --only if it is a new datum do you get this far
              v_currdatum := t_waterlvls(v_index).vdatum_id;
            
              --loop through t_waterlvl to see if this vdatum_id has been done previously
              --FOR j IN t_waterlvl.FIRST..(v_index-1) LOOP
              
              IF v_index <> t_waterlvls.FIRST THEN
                v_index2 := t_waterlvls.FIRST;
                LOOP
              
                  --if this datum has already been written to the file then don't write to file now
                  IF t_waterlvls(v_index2).vdatum_id = v_currdatum THEN
                    v_write_datum := FALSE;
                  END IF;
                  EXIT WHEN v_index2 = t_waterlvls.PRIOR(v_index);
                  v_index2 := t_waterlvls.NEXT(v_index2);
                END LOOP;
              END IF;
            
              IF v_write_datum THEN
      --****   now we can output the definitionMember tag
              
                UTL_FILE.PUT_LINE(f_w_ref, '    <wdtf:definitionMember>');
                UTL_FILE.PUT_LINE(f_w_ref, '      <gml:VerticalDatum gml:id="'||t_waterlvls(v_index).vdatum_id||'">');
                UTL_FILE.PUT_LINE(f_w_ref, '        <gml:datumName>urn:ogc:def:datum:bom::TopOfCasing</gml:datumName>');
                UTL_FILE.PUT_LINE(f_w_ref, '        <gml:anchorPoint codeSpace="urn:ogc:def:crs:EPSG::5711">'||t_waterlvls(v_index).elevation||'</gml:anchorPoint>');
                UTL_FILE.PUT_LINE(f_w_ref, '      </gml:VerticalDatum>');
                UTL_FILE.PUT_LINE(f_w_ref, '    </wdtf:definitionMember>');
              
              END IF;
            END IF;
          END IF;
          EXIT WHEN v_index = t_waterlvls.LAST;
          v_index := t_waterlvls.NEXT(v_index);
        END LOOP;--t_waterlvls loop
      END IF;--t_waterlvls not null
      
    ELSE
      --else there are no water levels to report so make sure they don't get written to file
      v_wl_check := FALSE;
    END IF;
    
    CLOSE c_wl;
    
    --then do the site/casing data which as definitionMember tags and siteMember tags ** set a boolean true for featureMember tags
    xml_casing(r_rns.rn, f_w_ref, TO_DATE(v_update_date,'DD/MM/YYYY'), v_datum_done);
    
    --********************************
    --need to clear t_waterquals of current entries for new RN
    IF t_waterquals.FIRST IS NOT NULL THEN
      t_waterquals.DELETE;
    END IF;
    
    v_related_ref := NULL;
    v_wq_check := FALSE;
    
    OPEN c_wq(r_rns.rn, TO_DATE(v_update_date,'DD/MM/YYYY'));
    FETCH c_wq INTO v_rn;
    IF c_wq%FOUND THEN
    
      --then get water quality data into another table
      xml_waterQuality_data(r_rns.rn, TO_DATE(v_update_date,'DD/MM/YYYY'), t_waterquals);
    
      --output the specimenMember tags
      --loop through to find the specimenMember using sample_id and bottle_id
      v_index := t_waterquals.FIRST;
      LOOP
        --have to make sure the sample_id field is populated before outputting the xml
        IF t_waterquals(v_index).sample_id IS NOT NULL THEN
          
          UTL_FILE.PUT_LINE(f_w_ref, '    <wdtf:specimenMember>');
          UTL_FILE.PUT_LINE(f_w_ref, '      <wdtf:Specimen gml:id="'||t_waterquals(v_index).sample_id||'">');
          
          IF t_waterquals(v_index).bottle_id IS NOT NULL THEN
            UTL_FILE.PUT_LINE(f_w_ref, '        <gml:description>A sample taken for field measurements and bottle sent to lab.</gml:description>');
          ELSE
            UTL_FILE.PUT_LINE(f_w_ref, '        <gml:description>A sample taken for field measurements.</gml:description>');
          END IF;
          
          UTL_FILE.PUT_LINE(f_w_ref, '        <gml:name codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/feature/Specimen/w00066/">'||t_waterquals(v_index).rn||'/'||t_waterquals(v_index).pipe||'</gml:name>');
          UTL_FILE.PUT_LINE(f_w_ref, '        <sa:sampledFeature xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/feature/Bore/w00066/'||t_waterquals(v_index).rn||'" xlink:title="Bore" />');
          UTL_FILE.PUT_LINE(f_w_ref, '        <sa:relatedSamplingFeature xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/feature/SamplingPoint/w00066/'||t_waterquals(v_index).rn||'" xlink:arcrole="http://www.bom.gov.au/std/water/xml/wio0.2/definition/arcrole/bom/parent" />');
          UTL_FILE.PUT_LINE(f_w_ref, '        <sa:surveyDetails>');
          UTL_FILE.PUT_LINE(f_w_ref, '          <sa:SurveyProcedure gml:id="survey'||t_waterquals(v_index).rn||'-'||TO_CHAR(t_waterquals(v_index).rdate,'YYYYMMDD')||'">');
          UTL_FILE.PUT_LINE(f_w_ref, '            <sa:elevationDatum xlink:href="urn:ogc:def:datum:bom::TopOfCasing" />');
          UTL_FILE.PUT_LINE(f_w_ref, '            <sa:positionMethod xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/procedure/positionMethod/bom/inherited" />');
          UTL_FILE.PUT_LINE(f_w_ref, '          </sa:SurveyProcedure>');
          UTL_FILE.PUT_LINE(f_w_ref, '        </sa:surveyDetails>');
          UTL_FILE.PUT_LINE(f_w_ref, '        <sa:parameter type="samplingDepth" uom="m">'||t_waterquals(v_index).depth||'</sa:parameter>');
          UTL_FILE.PUT_LINE(f_w_ref, '        <sa:materialClass>water</sa:materialClass>');
          UTL_FILE.PUT_LINE(f_w_ref, '        <sa:samplingTime>'||TO_CHAR(t_waterquals(v_index).rdate,'YYYY-MM-DD')||'T'||TO_CHAR(t_waterquals(v_index).rdate,'HH24:MI:SS')||'+10:00</sa:samplingTime>');
          UTL_FILE.PUT_LINE(f_w_ref, '      </wdtf:Specimen>');
          UTL_FILE.PUT_LINE(f_w_ref, '    </wdtf:specimenMember>');
          
        END IF;
        
        --have to make sure the bottle_id field is populated before outputting the xml
        IF t_waterquals(v_index).bottle_id IS NOT NULL THEN
          
          UTL_FILE.PUT_LINE(f_w_ref, '    <wdtf:specimenMember>');
          UTL_FILE.PUT_LINE(f_w_ref, '      <wdtf:Specimen gml:id="'||t_waterquals(v_index).bottle_id||'">');
          
          IF t_waterquals(v_index).sample_id IS NOT NULL THEN
            UTL_FILE.PUT_LINE(f_w_ref, '        <gml:description>A bottle sent to lab and field measurements taken at the same time.</gml:description>');
            v_related_ref := '#'||t_waterquals(v_index).sample_id;
          ELSE
            UTL_FILE.PUT_LINE(f_w_ref, '        <gml:description>This sample has only one bottle and so sample and bottle properties are merged</gml:description>');
            v_related_ref := 'http://www.bom.gov.au/std/water/xml/wio0.2/feature/SamplingGroup/w00066/'||t_waterquals(v_index).rn;
          END IF;
          
          UTL_FILE.PUT_LINE(f_w_ref, '        <gml:name codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/feature/Specimen/w00066/">'||t_waterquals(v_index).rn||'/'||t_waterquals(v_index).lab_analysis||'</gml:name>');
          UTL_FILE.PUT_LINE(f_w_ref, '        <sa:sampledFeature xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/feature/Bore/w00066/'||t_waterquals(v_index).rn||'" xlink:title="Bore" />');
          UTL_FILE.PUT_LINE(f_w_ref, '        <sa:relatedSamplingFeature xlink:href="'||v_related_ref||'" xlink:arcrole="http://www.bom.gov.au/std/water/xml/wio0.2/definition/arcrole/bom/parent" />');
          UTL_FILE.PUT_LINE(f_w_ref, '        <sa:materialClass>water</sa:materialClass>');
          UTL_FILE.PUT_LINE(f_w_ref, '        <sa:samplingTime>'||TO_CHAR(t_waterquals(v_index).rdate,'YYYY-MM-DD')||'T'||TO_CHAR(t_waterquals(v_index).rdate,'HH24:MI:SS')||'+10:00</sa:samplingTime>');
          UTL_FILE.PUT_LINE(f_w_ref, '      </wdtf:Specimen>');
          UTL_FILE.PUT_LINE(f_w_ref, '    </wdtf:specimenMember>');
          
        END IF;
        
        EXIT WHEN v_index = t_waterquals.LAST;
        v_index := t_waterquals.NEXT(v_index);
      END LOOP;
      
      v_wq_check := TRUE;
    END IF;
    CLOSE c_wq;
    
    
    --using the waterlvl_table output the procedureMember tags
    v_currgwlm := NULL;
    v_write_gwlm := TRUE;
    
    IF v_wl_check THEN
      --once that's done then loop through t_waterlvl for the procedureMember tags using gwlm_id
      IF t_waterlvls.FIRST IS NOT NULL THEN
        v_index := t_waterlvls.FIRST;
        LOOP
          IF v_currgwlm IS NULL OR t_waterlvls(v_index).gwlm_id <> v_currgwlm THEN
          
            v_currgwlm := t_waterlvls(v_index).gwlm_id;
          
            --loop through t_waterlvl to see if this gwlm has been done previously
            --FOR j IN t_waterlvl.FIRST..(i-1) LOOP
            IF v_index <> t_waterlvls.FIRST THEN
              v_index2 := t_waterlvls.FIRST;
              LOOP
              
                --if this gwlm has already been written to the file then don't write to file now
                IF t_waterlvls(v_index2).gwlm_id = v_currgwlm THEN
                  v_write_gwlm := FALSE;
                END IF;
                EXIT WHEN v_index2 = t_waterlvls.PRIOR(v_index);
                v_index2 := t_waterlvls.NEXT(v_index2);
              END LOOP;
            END IF;
          
            IF t_waterlvls(v_index).vdatum_id IS NOT NULL THEN
              v_observPoint := '#'||t_waterlvls(v_index).vdatum_id;
            ELSE
              v_observPoint := 'urn:ogc:def:datum:bom::TopOfCasing';
            END IF;
          
            IF v_write_gwlm THEN
              UTL_FILE.PUT_LINE(f_w_ref, '    <wdtf:procedureMember>');
              UTL_FILE.PUT_LINE(f_w_ref, '      <wdtf:GroundWaterLevelMethod gml:id="'||t_waterlvls(v_index).gwlm_id||'">');
              UTL_FILE.PUT_LINE(f_w_ref, '        <wdtf:observationPointDatum xlink:href="'||v_observPoint||'" xlink:title="top of casing" />');
              UTL_FILE.PUT_LINE(f_w_ref, '        <wdtf:observationAxis xlink:href="urn:ogc:def:axis:EPSG::9916" xlink:title="Height influenced by the Earths gravity field, positive upwards." />');
              UTL_FILE.PUT_LINE(f_w_ref, '        <wdtf:sensor xlink:href="urn:ogc:def:nil:OGC::unknown" />');
              UTL_FILE.PUT_LINE(f_w_ref, '      </wdtf:GroundWaterLevelMethod>');
              UTL_FILE.PUT_LINE(f_w_ref, '    </wdtf:procedureMember>');
            END IF;
          
          END IF;
          EXIT WHEN v_index = t_waterlvls.LAST;
          v_index := t_waterlvls.NEXT(v_index);
        END LOOP;
      END IF;
      
      --then loop through the table again to output the observationMember tags
      --then timeSeriesObservation member tags
        --bare in mind when the pipe and vdatum_id changes to then create a new observationMember tag block
      
      v_count := 1;   --set the count for use when iterating through the table
      
      --FOR i IN t_waterlvl.FIRST..t_waterlvl.LAST LOOP
      IF t_waterlvls.FIRST IS NOT NULL THEN
        v_index := t_waterlvls.FIRST;
        LOOP
          --EXIT WHEN v_count > t_waterlvl.LAST;
          v_currpipe := t_waterlvls(v_index).pipe;
          v_currgwlm := t_waterlvls(v_index).gwlm_id;
        
          UTL_FILE.PUT_LINE(f_w_ref, '    <wdtf:observationMember>');
          UTL_FILE.PUT_LINE(f_w_ref, '      <wdtf:TimeSeriesObservation gml:id="ts'||v_count||'">');
          UTL_FILE.PUT_LINE(f_w_ref, '        <gml:description>Reading using DTW (Depth To Water) from TOC</gml:description>');
          UTL_FILE.PUT_LINE(f_w_ref, '        <gml:name codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/feature/TimeSeriesObservation/w00066/'||t_waterlvls(v_index).rn||'/'||t_waterlvls(v_index).pipe||'/GroundWaterLevel/">'||v_count||'</gml:name>');
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:procedure xlink:href="#'||t_waterlvls(v_index).gwlm_id||'" />');
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:observedProperty xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/property//bom/GroundWaterLevel_m" />');
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:featureOfInterest xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/feature/BorePipeSamplingInterval/w00066/'||t_waterlvls(v_index).rn||'" />');
          UTL_FILE.PUT_LINE(f_w_ref, '        <wdtf:metadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '          <wdtf:TimeSeriesObservationMetadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '            <wdtf:regulationProperty>Reg200806.s3.2a</wdtf:regulationProperty>');
          UTL_FILE.PUT_LINE(f_w_ref, '            <wdtf:status>validated</wdtf:status>');
          UTL_FILE.PUT_LINE(f_w_ref, '          </wdtf:TimeSeriesObservationMetadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '        </wdtf:metadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '        <wdtf:result>');
          UTL_FILE.PUT_LINE(f_w_ref, '          <wdtf:TimeSeries>');
          UTL_FILE.PUT_LINE(f_w_ref, '            <wdtf:defaultInterpolationType>InstVal</wdtf:defaultInterpolationType>');
          UTL_FILE.PUT_LINE(f_w_ref, '            <wdtf:defaultUnitsOfMeasure>'||t_waterlvls(v_index).units||'</wdtf:defaultUnitsOfMeasure>');
          UTL_FILE.PUT_LINE(f_w_ref, '            <wdtf:defaultQuality>quality-A</wdtf:defaultQuality>');

          --FOR j IN v_count..t_waterlvl.LAST LOOP
          v_index2 := v_index;
          LOOP
            EXIT WHEN t_waterlvls(v_index2).pipe <> v_currpipe OR t_waterlvls(v_index2).gwlm_id <> v_currgwlm;
          
            --start inserting the time value pairs with the time as an attribute
            v_time_value := '<wdtf:timeValuePair time="'||TO_CHAR(t_waterlvls(v_index2).rdate,'yyyy-mm-dd')||'T'||TO_CHAR(t_waterlvls(v_index2).rdate,'hh:mi:ss')||'+10:00"';
          
            -- there is room for a remark so these are also added as attributes however only if there is a remark to begin with
            CASE t_waterlvls(v_index2).remark
              WHEN 'P'
                THEN v_time_value := v_time_value ||' observationConditions="http://www.bom.gov.au/std/water/xml/wio0.2/procedure/ObservationConditions/bom/pumping"';
              WHEN 'D'
                THEN v_time_value := v_time_value ||' observationConditions="http://www.bom.gov.au/std/water/xml/wio0.2/procedure/ObservationConditions/bom/dryBore"';
              ELSE NULL;
            END CASE;
          
            --there are no other attributes so now we add the value to the string then insert into the xml file and loop through the other water levels.
            v_time_value := v_time_value ||'>'||t_waterlvls(v_index2).measurement||'</wdtf:timeValuePair>';
            
            t_waterlvls(v_index2).written := 1;  --this is to provide the proper escape clause
            
            UTL_FILE.PUT_LINE(f_w_ref, '            '||v_time_value);
            --v_count := v_count + 1;
            EXIT WHEN v_index2 = t_waterlvls.LAST;
            v_index2 := t_waterlvls.NEXT(v_index2);
          
          END LOOP;
          
          v_index := v_index2;
          v_count := v_count + 1;
        
          UTL_FILE.PUT_LINE(f_w_ref, '          </wdtf:TimeSeries>');
          UTL_FILE.PUT_LINE(f_w_ref, '        </wdtf:result>');
          UTL_FILE.PUT_LINE(f_w_ref, '      </wdtf:TimeSeriesObservation>');
          UTL_FILE.PUT_LINE(f_w_ref, '    </wdtf:observationMember>');
          
          --need to add up all the written fields and compare to t_waterlvl.COUNT
          IF t_waterlvls(v_index).written = 1 THEN
            EXIT WHEN v_index = t_waterlvls.LAST;
            v_index := t_waterlvls.NEXT(v_index);
          END IF;
        
        END LOOP;
      END IF;
      
    END IF;
    
    --******************
    --then the conductivity and pH observationMember tags
    IF v_wq_check THEN
      --v_wq_check is only true if there are specimenMember tag/s meaning a water quality reading
      v_index := t_waterquals.FIRST;
      v_count := 1;
      
      LOOP
        
        --first do lab conductivity
        IF t_waterquals(v_index).lab_cond IS NOT NULL THEN
          
          UTL_FILE.PUT_LINE(f_w_ref, '    <wdtf:observationMember>');
          UTL_FILE.PUT_LINE(f_w_ref, '      <wdtf:Measurement gml:id="gwLabCond'||t_waterquals(v_index).rn||'-'||v_count||'">');
          UTL_FILE.PUT_LINE(f_w_ref, '        <gml:description>a sample taken from the bore and sent to QHFSS lab</gml:description>');--a description of a lab measurement
          UTL_FILE.PUT_LINE(f_w_ref, '        <gml:name codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/feature/Measurement/w00066/'||t_waterquals(v_index).rn||'/'||t_waterquals(v_index).pipe||'/">conduct/'||t_waterquals(v_index).lab_analysis||'</gml:name>');--uses the form rn/analysis_no
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:samplingTime>'||TO_CHAR(t_waterquals(v_index).rdate,'YYYY-MM-DD')||'T'||TO_CHAR(t_waterquals(v_index).rdate,'HH24:MI:SS')||'+10:00</om:samplingTime>');
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:procedure xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/procedure/QualityMethod/w00066/electroChemistry" />');--procedure used for lab measurement
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:observedProperty xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/property//bom/ElectricalConductivityAt25C_uScm" xlink:title="Electrical Conductivity @ 25C" />');
          --featureOfInterest has changed in 1.0.2 to point to a specimenMember, for lab that is bottle
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:featureOfInterest xlink:href="#'||t_waterquals(v_index).bottle_id||'" />');
          UTL_FILE.PUT_LINE(f_w_ref, '        <wdtf:metadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '          <wdtf:MeasurementObservationMetadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '            <wdtf:regulationProperty>Reg201205.s3.9b</wdtf:regulationProperty>');
          UTL_FILE.PUT_LINE(f_w_ref, '          </wdtf:MeasurementObservationMetadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '        </wdtf:metadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '        <wdtf:result uom="uS/cm">'||t_waterquals(v_index).lab_cond||'</wdtf:result>');
          UTL_FILE.PUT_LINE(f_w_ref, '      </wdtf:Measurement>');
          UTL_FILE.PUT_LINE(f_w_ref, '    </wdtf:observationMember>');
        END IF;
        
        --then do lab pH
        IF t_waterquals(v_index).lab_ph IS NOT NULL THEN
          
          UTL_FILE.PUT_LINE(f_w_ref, '    <wdtf:observationMember>');
          UTL_FILE.PUT_LINE(f_w_ref, '      <wdtf:Measurement gml:id="gwLabpH'||t_waterquals(v_index).rn||'-'||v_count||'">');
          UTL_FILE.PUT_LINE(f_w_ref, '        <gml:description>pH of a sample taken from the bore and sent to QHFSS lab</gml:description>');--a description of a lab measurement
          UTL_FILE.PUT_LINE(f_w_ref, '        <gml:name codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/feature/Measurement/w00066/'||t_waterquals(v_index).rn||'/'||t_waterquals(v_index).pipe||'/">pH/'||t_waterquals(v_index).lab_analysis||'</gml:name>');--uses the form rn/analysis_no
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:samplingTime>'||TO_CHAR(t_waterquals(v_index).rdate,'YYYY-MM-DD')||'T'||TO_CHAR(t_waterquals(v_index).rdate,'HH24:MI:SS')||'+10:00</om:samplingTime>');
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:procedure xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/procedure/QualityMethod/w00066/pH" />');
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:observedProperty xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/property//bom/WaterpH_pH" xlink:title="pH" />');
          --featureOfInterest has changed in 1.0.2 to point to a specimenMember, for lab that is bottle
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:featureOfInterest xlink:href="#'||t_waterquals(v_index).bottle_id||'" />');
          UTL_FILE.PUT_LINE(f_w_ref, '        <wdtf:metadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '          <wdtf:MeasurementObservationMetadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '            <wdtf:regulationProperty>Reg201205.s3.9ga</wdtf:regulationProperty>');
          UTL_FILE.PUT_LINE(f_w_ref, '          </wdtf:MeasurementObservationMetadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '        </wdtf:metadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '        <wdtf:result uom="[pH]">'||t_waterquals(v_index).lab_ph||'</wdtf:result>');
          UTL_FILE.PUT_LINE(f_w_ref, '      </wdtf:Measurement>');
          UTL_FILE.PUT_LINE(f_w_ref, '    </wdtf:observationMember>');
        END IF;
        
        --then do field conductivity
        IF t_waterquals(v_index).field_cond IS NOT NULL THEN
          
          UTL_FILE.PUT_LINE(f_w_ref, '    <wdtf:observationMember>');
          UTL_FILE.PUT_LINE(f_w_ref, '      <wdtf:Measurement gml:id="gwFieldCond'||t_waterquals(v_index).rn||'-'||v_count||'">');
          UTL_FILE.PUT_LINE(f_w_ref, '        <gml:description>an insitu site measurement taken at the bore</gml:description>');--a description of a lab measurement
          UTL_FILE.PUT_LINE(f_w_ref, '        <gml:name codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/feature/Measurement/w00066/'||t_waterquals(v_index).rn||'/'||t_waterquals(v_index).pipe||'/">conduct/'||v_count||'</gml:name>');--uses the form rn/analysis_no
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:samplingTime>'||TO_CHAR(t_waterquals(v_index).rdate,'YYYY-MM-DD')||'T'||TO_CHAR(t_waterquals(v_index).rdate,'HH24:MI:SS')||'+10:00</om:samplingTime>');
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:procedure xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/procedure/QualityMethod/w00066/electroChemistry" />');--procedure used for lab measurement
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:observedProperty xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/property//bom/ElectricalConductivityAt25C_uScm" xlink:title="Electrical Conductivity @ 25C" />');
          --featureOfInterest has changed in 1.0.2 to point to a specimenMember, for field that is sample
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:featureOfInterest xlink:href="#'||t_waterquals(v_index).sample_id||'" />');
          UTL_FILE.PUT_LINE(f_w_ref, '        <wdtf:metadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '          <wdtf:MeasurementObservationMetadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '            <wdtf:regulationProperty>Reg201205.s3.9b</wdtf:regulationProperty>');
          UTL_FILE.PUT_LINE(f_w_ref, '          </wdtf:MeasurementObservationMetadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '        </wdtf:metadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '        <wdtf:result uom="uS/cm">'||t_waterquals(v_index).field_cond||'</wdtf:result>');
          UTL_FILE.PUT_LINE(f_w_ref, '      </wdtf:Measurement>');
          UTL_FILE.PUT_LINE(f_w_ref, '    </wdtf:observationMember>');
          
        END IF;
        
        --lastly field pH
        IF t_waterquals(v_index).field_ph IS NOT NULL THEN
          
          UTL_FILE.PUT_LINE(f_w_ref, '    <wdtf:observationMember>');
          UTL_FILE.PUT_LINE(f_w_ref, '      <wdtf:Measurement gml:id="gwFieldpH'||t_waterquals(v_index).rn||'-'||v_count||'">');
          UTL_FILE.PUT_LINE(f_w_ref, '        <gml:description>an insitu site measurement taken at the bore</gml:description>');--a description of a lab measurement
          UTL_FILE.PUT_LINE(f_w_ref, '        <gml:name codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/feature/Measurement/w00066/'||t_waterquals(v_index).rn||t_waterquals(v_index).pipe||'/">pH/'||v_count||'</gml:name>');--uses the form rn/analysis_no
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:samplingTime>'||TO_CHAR(t_waterquals(v_index).rdate,'YYYY-MM-DD')||'T'||TO_CHAR(t_waterquals(v_index).rdate,'HH24:MI:SS')||'+10:00</om:samplingTime>');
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:procedure xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/procedure/QualityMethod/w00066/pH" />');
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:observedProperty xlink:href="http://www.bom.gov.au/std/water/xml/wio0.2/property//bom/WaterpH_pH" xlink:title="pH" />');
          --featureOfInterest has changed in 1.0.2 to point to a specimenMember, for field that is sample
          UTL_FILE.PUT_LINE(f_w_ref, '        <om:featureOfInterest xlink:href="#'||t_waterquals(v_index).sample_id||'" />');
          UTL_FILE.PUT_LINE(f_w_ref, '        <wdtf:metadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '          <wdtf:MeasurementObservationMetadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '            <wdtf:regulationProperty>Reg201205.s3.9ga</wdtf:regulationProperty>');
          UTL_FILE.PUT_LINE(f_w_ref, '          </wdtf:MeasurementObservationMetadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '        </wdtf:metadata>');
          UTL_FILE.PUT_LINE(f_w_ref, '        <wdtf:result uom="[pH]">'||t_waterquals(v_index).field_ph||'</wdtf:result>');
          UTL_FILE.PUT_LINE(f_w_ref, '      </wdtf:Measurement>');
          UTL_FILE.PUT_LINE(f_w_ref, '    </wdtf:observationMember>');
          
        END IF;
        
        v_count := v_count + 1;
        
        EXIT WHEN v_index = t_waterquals.LAST;
        v_index := t_waterquals.NEXT(v_index);
      END LOOP;
      
    END IF;
    
    
    --then lastly the featureMember tag
    
    v_drilled_date := NULL;
    v_ref_el := NULL;
    v_nat_el := NULL;
    
    --and feature member
    --for some reason this is necessary when providing the borePipeSamplingInterval tag block
    --v_datum_done is only set to FALSE in the xml_casing procedure once a borePipeSamplingInterval as been created
    IF NOT v_datum_done THEN
      
      
      UTL_FILE.PUT_LINE(f_w_ref, '    <wdtf:featureMember>');
      UTL_FILE.PUT_LINE(f_w_ref, '    	<ahgf:Bore gml:id="RN'||r_rns.rn||'">');
      UTL_FILE.PUT_LINE(f_w_ref, '    		<gml:description>Bore details for RN '||r_rns.rn||'</gml:description>');
      UTL_FILE.PUT_LINE(f_w_ref, '    		<gml:name codeSpace="http://www.bom.gov.au/std/water/xml/wio0.2/feature/Bore/w00066/">'||r_rns.rn||'</gml:name>');
      
      SELECT drilled_date
      INTO v_drilled_date
      FROM gw_regdets
      WHERE rn = r_rns.rn;
      
      IF v_drilled_date IS NOT NULL THEN
      
        UTL_FILE.PUT_LINE(f_w_ref, '    		<ahgf:constructionDate>'||TO_CHAR(v_drilled_date,'yyyy-mm-dd')||'T'||TO_CHAR(v_drilled_date,'hh:mi:ss')||'+10:00</ahgf:constructionDate>');
      END IF;
      
      SELECT elevation
      INTO v_ref_el
      FROM gw_elvdets
      WHERE rn = r_rns.rn
      AND rdate = (SELECT MAX(rdate) FROM gw_elvdets WHERE rn = r_rns.rn AND meas_point = 'R' AND datum = 'AHD')
      AND meas_point = 'R'
      AND datum = 'AHD'
      AND pipe = 'A';
      
      IF v_ref_el IS NOT NULL THEN
        UTL_FILE.PUT_LINE(f_w_ref, '    		<ahgf:topOfCasing>');
        UTL_FILE.PUT_LINE(f_w_ref, '    		  <gml:Point srsName="urn:ogc:def:crs:EPSG::5711">');
        UTL_FILE.PUT_LINE(f_w_ref, '    		    <gml:pos>'||v_ref_el||'</gml:pos>');
        UTL_FILE.PUT_LINE(f_w_ref, '    		  </gml:Point>');
        UTL_FILE.PUT_LINE(f_w_ref, '    		</ahgf:topOfCasing>');
        
      END IF;
      
      SELECT elevation
      INTO v_nat_el
      FROM gw_elvdets
      WHERE rn = r_rns.rn
      AND rdate = (SELECT MAX(rdate) FROM gw_elvdets WHERE rn = r_rns.rn AND meas_point = 'N' AND datum = 'AHD')
      AND meas_point = 'N'
      AND datum = 'AHD';
      
      IF v_nat_el IS NOT NULL THEN
        UTL_FILE.PUT_LINE(f_w_ref, '    		<ahgf:naturalSurfaceLevel>');
        UTL_FILE.PUT_LINE(f_w_ref, '    		  <gml:Point srsName="urn:ogc:def:crs:EPSG::5711">');
        UTL_FILE.PUT_LINE(f_w_ref, '    		    <gml:pos>'||v_nat_el||'</gml:pos>');
        UTL_FILE.PUT_LINE(f_w_ref, '    		  </gml:Point>');
        UTL_FILE.PUT_LINE(f_w_ref, '    		</ahgf:naturalSurfaceLevel>');
        
      END IF;
      
      UTL_FILE.PUT_LINE(f_w_ref, '    	</ahgf:Bore>');
      UTL_FILE.PUT_LINE(f_w_ref, '    </wdtf:featureMember>');
      
    END IF;
    

    --generate the water quality tags (if necessary)
    --this has to be called before the xml_casing procedure because it uses the observationMember tags the same as water levels
    --this is to ensure that all like tags are together
--    xml_quality(r_rns.rn, f_w_ref, TO_DATE(v_update_date,'DD/MM/YYYY'));
    
    UTL_FILE.PUT_LINE(f_w_ref, '</wdtf:HydroCollection>');
    UTL_FILE.FCLOSE(f_w_ref);
  END LOOP;

  --update the date to todays date for next update
--  UPDATE gw_system_variables
--  SET parameter_value = TO_CHAR(SYSDATE, 'DD/MM/YYYY')
--  WHERE parameter_code = 'BOM_DATA_UPDATE';

--  commit;

EXCEPTION
   WHEN UTL_FILE.INVALID_PATH
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_path'); RAISE;

   WHEN UTL_FILE.INVALID_MODE
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_mode'); RAISE;

   WHEN UTL_FILE.INVALID_FILEHANDLE
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_filehandle'); RAISE;

   WHEN UTL_FILE.INVALID_OPERATION
   THEN
       DBMS_OUTPUT.PUT_LINE ('invalid_operation'); RAISE;

   WHEN UTL_FILE.READ_ERROR
   THEN
       DBMS_OUTPUT.PUT_LINE ('read_error'); RAISE;

   WHEN UTL_FILE.WRITE_ERROR
   THEN
      DBMS_OUTPUT.PUT_LINE ('write_error'); RAISE;

   WHEN UTL_FILE.INTERNAL_ERROR
   THEN
      DBMS_OUTPUT.PUT_LINE ('internal_error'); RAISE;

END xml_update2;


/*********************************************************************
 * MODULE:	xml_waterlvl_data
 * PURPOSE:	This procedure is the code that extracts the water level
 *		     information for WDTFv1.0.2
 * RETURNS:	a table of waterlvl_type that contains all relevant water
 *          levels and elevations
 * NOTES:   waterlvl_type is a temp table declared above
 *********************************************************************/
PROCEDURE xml_waterlvl_data(p_rn IN NUMBER, udate IN DATE, p_wlvl_table IN OUT waterlvl_type) IS
  
  CURSOR c_wlvdets IS
    SELECT rn, pipe, rdate, meas_point, measurement, logger, remark
    FROM gw_wlvdets w
    WHERE rn = p_rn
    AND updated_date > udate
    AND pipe <> 'X'
    AND meas_point <> 'N'
    ORDER BY pipe;
  
  CURSOR c_artesian IS
    SELECT rn
    FROM gw_regdets
    WHERE rn = p_rn
    AND facility_type LIKE 'A%';

  CURSOR c_pumtes IS
    SELECT rn, pipe, rdate, swl
    FROM gw_pumtes
    WHERE rn = p_rn
    AND updated_date > udate
    AND pipe <> 'X'
    AND swl IS NOT NULL
    ORDER BY rdate;

  CURSOR c_elvdets IS
    SELECT rn, pipe, rdate, elevation
    FROM gw_elvdets
    WHERE rn = p_rn
    AND meas_point = 'R'
    AND datum = 'AHD'
    AND pipe = 'A'
    ORDER BY rdate ASC;


  v_elev_cnt      NUMBER(1);
  v_elev_row    c_elvdets%ROWTYPE;
  v_index       NUMBER;
  
BEGIN
  
    
    SELECT COUNT(rn)
    INTO v_elev_cnt
    FROM gw_elvdets
    WHERE rn = p_rn
    AND meas_point = 'R'
    AND datum = 'AHD'
    AND pipe = 'A';  --this gets the number of relevant elevation entries

    IF v_elev_cnt > 0 THEN
      --if there are any elevation entries loop through them

      OPEN c_elvdets;
      FOR i IN 1..v_elev_cnt LOOP --for each relevant elevation
        
        --fetch the current elevation data
        FETCH c_elvdets INTO v_elev_row;
        --v_index := 1; --reset the water level index
        
        --for each water level recorded
        FOR r_wlvdets IN c_wlvdets LOOP
          
          v_index := TO_NUMBER(TO_CHAR(r_wlvdets.rdate,'YYYYMMDD'));
          
          --compare the current elevation date with each water level date
          --if the elevation date is less than or equal to then there is an elevation for this water level
          IF v_elev_row.rdate <= r_wlvdets.rdate THEN
          
            --check to see if it already exists in the table
            --if it does then need to compare with this elevation date and update the entry if necessary
            
            IF p_wlvl_table.EXISTS(v_index) THEN
              
              IF v_elev_row.rdate <= p_wlvl_table(v_index).rdate THEN
                
                p_wlvl_table(v_index).edate := v_elev_row.rdate;
                p_wlvl_table(v_index).elevation := v_elev_row.elevation;
                p_wlvl_table(v_index).vdatum_id := 'ObservationDatum'||r_wlvdets.rn||'-'||i;
                p_wlvl_table(v_index).gwlm_id := 'gwMethod'||r_wlvdets.rn||'-'||i;
                
              END IF;
              
            ELSE
              --else an entry does not already exist and needs to be created
              
              p_wlvl_table(v_index).rn := r_wlvdets.rn;
              p_wlvl_table(v_index).pipe := r_wlvdets.pipe;
              p_wlvl_table(v_index).rdate := r_wlvdets.rdate;
              p_wlvl_table(v_index).meas_point := r_wlvdets.meas_point;
              p_wlvl_table(v_index).measurement := r_wlvdets.measurement;
              p_wlvl_table(v_index).units := 'm';
              p_wlvl_table(v_index).remark := r_wlvdets.remark;
              p_wlvl_table(v_index).logger := r_wlvdets.logger;
              p_wlvl_table(v_index).edate := v_elev_row.rdate;
              p_wlvl_table(v_index).elevation := v_elev_row.elevation;
              p_wlvl_table(v_index).vdatum_id := 'ObservationDatum'||r_wlvdets.rn||'-'||i;
              p_wlvl_table(v_index).gwlm_id := 'gwMethod'||r_wlvdets.rn||'-'||i;
              
                            
            END IF;
          ELSE
            --else this water level date does not have an elevation
            
            IF p_wlvl_table.EXISTS(v_index) THEN
              NULL;
            ELSE
              
              --there is no elevation for this water level so no vdatum_id just a gwlm_id
              p_wlvl_table(v_index).rn := r_wlvdets.rn;
              p_wlvl_table(v_index).pipe := r_wlvdets.pipe;
              p_wlvl_table(v_index).rdate := r_wlvdets.rdate;
              p_wlvl_table(v_index).meas_point := r_wlvdets.meas_point;
              p_wlvl_table(v_index).measurement := r_wlvdets.measurement;
              p_wlvl_table(v_index).units := 'm';
              p_wlvl_table(v_index).remark := r_wlvdets.remark;
              p_wlvl_table(v_index).logger := r_wlvdets.logger;
              p_wlvl_table(v_index).edate := NULL;
              p_wlvl_table(v_index).elevation := NULL;
              p_wlvl_table(v_index).vdatum_id := NULL;
              p_wlvl_table(v_index).gwlm_id := 'gwTOC'||r_wlvdets.rn;
              
              
            END IF;

          END IF;
        END LOOP;
      END LOOP;

      CLOSE c_elvdets;
      
      --now to do the pump test data
      OPEN c_elvdets;
      FOR i IN 1..v_elev_cnt LOOP --for each relevant elevation
        
        --fetch the current elevation data
        FETCH c_elvdets INTO v_elev_row;
        
        --for each pump test recorded
        FOR r_pumtes IN c_pumtes LOOP
          
          v_index := TO_NUMBER(TO_CHAR(r_pumtes.rdate,'YYYYMMDD'));
          
          --compare the current elevation date with each pump test date
          --if the elevation date is less than or equal to then there is an elevation for this pump test
          IF v_elev_row.rdate <= r_pumtes.rdate THEN
          
            --check to see if it already exists in the table
            --if it does then do nothing because the water level trumps pump test
            
            IF p_wlvl_table.EXISTS(v_index) THEN
              NULL;
            ELSE
              
              --else an entry does not already exist and needs to be created
              
              p_wlvl_table(v_index).rn := r_pumtes.rn;
              p_wlvl_table(v_index).pipe := r_pumtes.pipe;
              p_wlvl_table(v_index).rdate := r_pumtes.rdate;
              p_wlvl_table(v_index).meas_point := 'R';
              p_wlvl_table(v_index).measurement := r_pumtes.swl;
              p_wlvl_table(v_index).units := 'm';
              p_wlvl_table(v_index).remark := NULL;
              p_wlvl_table(v_index).logger := NULL;
              p_wlvl_table(v_index).edate := v_elev_row.rdate;
              p_wlvl_table(v_index).elevation := v_elev_row.elevation;
              p_wlvl_table(v_index).vdatum_id := 'ObservationDatum'||r_pumtes.rn||'-'||i;
              p_wlvl_table(v_index).gwlm_id := 'gwMethod'||r_pumtes.rn||'-'||i;
              
              
            END IF;
          ELSE
            --else this water level date does not have an elevation
            
            IF p_wlvl_table.EXISTS(v_index) THEN
              NULL;
            ELSE
              
              --there is no elevation for this water level so no vdatum_id just a gwlm_id
              p_wlvl_table(v_index).rn := r_pumtes.rn;
              p_wlvl_table(v_index).pipe := r_pumtes.pipe;
              p_wlvl_table(v_index).rdate := r_pumtes.rdate;
              p_wlvl_table(v_index).meas_point := 'R';
              p_wlvl_table(v_index).measurement := r_pumtes.swl;
              p_wlvl_table(v_index).units := 'm';
              p_wlvl_table(v_index).remark := NULL;
              p_wlvl_table(v_index).logger := NULL;
              p_wlvl_table(v_index).edate := NULL;
              p_wlvl_table(v_index).elevation := NULL;
              p_wlvl_table(v_index).vdatum_id := NULL;
              p_wlvl_table(v_index).gwlm_id := 'gwTOC'||r_pumtes.rn;
              
              
            END IF;

          END IF;
        END LOOP; --pump test table loop
        
      END LOOP; --elevation count loop
      
      CLOSE c_elvdets;
    
    ELSE
      --else there are no elevations in AHD so no definitionMember tags
      
      FOR r_wlvdets IN c_wlvdets LOOP
      
        v_index := TO_NUMBER(TO_CHAR(r_wlvdets.rdate,'YYYYMMDD'));
        
        IF p_wlvl_table.EXISTS(v_index) THEN
          NULL;
        ELSE
          --else an entry does not exist and needs to be entered
          p_wlvl_table(v_index).rn := r_wlvdets.rn;
          p_wlvl_table(v_index).pipe := r_wlvdets.pipe;
          p_wlvl_table(v_index).rdate := r_wlvdets.rdate;
          p_wlvl_table(v_index).meas_point := r_wlvdets.meas_point;
          p_wlvl_table(v_index).measurement := r_wlvdets.measurement;
          p_wlvl_table(v_index).units := 'm';
          p_wlvl_table(v_index).remark := r_wlvdets.remark;
          p_wlvl_table(v_index).logger := r_wlvdets.logger;
          --p_wlvl_table(v_index).edate := v_elev_row.rdate;
          --p_wlvl_table(v_index).elevation := v_elev_row.elevation;
          --p_wlvl_table(v_index).vdatum_id := 'ObservationDatum'||r_wlvdets.rn||'-'||i;
          p_wlvl_table(v_index).gwlm_id := 'gwTOC'||r_wlvdets.rn;
              
          
        END IF;
      END LOOP;
      
      FOR r_pumtes IN c_pumtes LOOP
        
        v_index := TO_NUMBER(TO_CHAR(r_pumtes.rdate,'YYYYMMDD'));
        
        IF p_wlvl_table.EXISTS(v_index) THEN
          NULL;
        ELSE
          --else an entry does not already exist and needs to be created
              
          p_wlvl_table(v_index).rn := r_pumtes.rn;
          p_wlvl_table(v_index).pipe := r_pumtes.pipe;
          p_wlvl_table(v_index).rdate := r_pumtes.rdate;
          p_wlvl_table(v_index).meas_point := 'R';
          p_wlvl_table(v_index).measurement := r_pumtes.swl;
          p_wlvl_table(v_index).units := 'm';
          --p_wlvl_table(v_index).remark := NULL;
          --p_wlvl_table(v_index).logger := NULL;
          --p_wlvl_table(v_index).edate := v_elev_row.rdate;
          --p_wlvl_table(v_index).elevation := v_elev_row.elevation;
          --p_wlvl_table(v_index).vdatum_id := 'ObservationDatum'||r_pumtes.rn||'-'||i;
          p_wlvl_table(v_index).gwlm_id := 'gwTOC'||r_pumtes.rn;
              
          
        END IF;
      END LOOP;
      
    END IF;
  
END xml_waterlvl_data;


/*********************************************************************
 * MODULE:	xml_waterQuality_data
 * PURPOSE:	This procedure is the code that extracts the water quality
 *		     information for WDTFv1.0.2
 * RETURNS:	a table of waterqual_type that contains all relevant water
 *          quality, conduct and pH
 * NOTES:   
 *********************************************************************/
PROCEDURE xml_waterQuality_data(p_rn IN NUMBER, udate IN DATE, p_wq_table IN OUT waterqual_type) IS
  
  
  CURSOR c_field IS
    SELECT pipe, rdate, conduct, ph, depth
    FROM gw_fieldqs
    WHERE rn = p_rn
    AND (conduct IS NOT NULL OR ph IS NOT NULL)
    AND pipe <> 'X'
    AND updated_date > udate
    ORDER BY rdate;

  CURSOR c_lab IS
    SELECT pipe, rdate, conduct, analysis_no, ph, depth
    FROM gw_watanls
    WHERE rn = p_rn
    AND (conduct IS NOT NULL OR ph IS NOT NULL)
    AND pipe <> 'X'
    AND updated_date > udate
    ORDER BY rdate;

  
  v_pip_id        NUMBER(1);
  v_rn            gw_regdets.rn%TYPE;
  v_index         NUMBER(10);
  v_field_count   NUMBER(2);
  v_lab_count     NUMBER(2);
  
  
BEGIN
  
  v_field_count := 1;
  
  --get the field data first because it uses the higher code block (specimen-sample)
  --field measurements are for specimen-sample
  FOR r_field IN c_field LOOP
    
    v_pip_id := CASE r_field.pipe
                    WHEN 'A' THEN 1
                    WHEN 'B' THEN 2
                    WHEN 'C' THEN 3
                    WHEN 'D' THEN 4
                    WHEN 'E' THEN 5
                    WHEN 'F' THEN 6
                    ELSE 8
                  END;
    
    v_index := v_pip_id||TO_NUMBER(TO_CHAR(r_field.rdate,'yyyymmdd'));
    
    IF p_wq_table.EXISTS(v_index) THEN
      NULL; --this means repeat data which should be impossible but good practice to eliminate
    ELSE
      --insert the field wq data into the table
      p_wq_table(v_index).rn := p_rn;
      p_wq_table(v_index).pipe := r_field.pipe;
      p_wq_table(v_index).rdate := r_field.rdate;
      p_wq_table(v_index).field_cond := r_field.conduct;
      p_wq_table(v_index).field_ph := r_field.ph;
      p_wq_table(v_index).sample_id := 'sample'||p_rn||'-'||v_field_count;
      
      IF r_field.depth IS NULL THEN
        p_wq_table(v_index).depth := 0.0;
      ELSE
        p_wq_table(v_index).depth := r_field.depth;
      END IF;
      
      v_field_count := v_field_count + 1;
      
    END IF;
    
  END LOOP;
  
  --lab measurements are for specimen-bottle
  --if on the same date as a field measurement then needs to reference the specimen-sample
  FOR r_lab IN c_lab LOOP
    
    v_pip_id := CASE r_lab.pipe
                    WHEN 'A' THEN 1
                    WHEN 'B' THEN 2
                    WHEN 'C' THEN 3
                    WHEN 'D' THEN 4
                    WHEN 'E' THEN 5
                    WHEN 'F' THEN 6
                    ELSE 8
                  END;
    
    v_index := v_pip_id||TO_NUMBER(TO_CHAR(r_lab.rdate,'yyyymmdd'));
    
    IF p_wq_table.EXISTS(v_index) THEN
      --means there is already a field measurement so just add the lab data
      
      p_wq_table(v_index).lab_cond := r_lab.conduct;
      p_wq_table(v_index).lab_ph := r_lab.ph;
      p_wq_table(v_index).lab_analysis := r_lab.analysis_no;
      p_wq_table(v_index).bottle_id := 'bottle'||p_rn||'-'||r_lab.analysis_no;
      
      IF r_lab.depth IS NOT NULL AND p_wq_table(v_index).depth = 0.0 THEN
        p_wq_table(v_index).depth := r_lab.depth;
      END IF;
      
      
    ELSE
      --insert the lab wq data into the table
      p_wq_table(v_index).rn := p_rn;
      p_wq_table(v_index).pipe := r_lab.pipe;
      p_wq_table(v_index).rdate := r_lab.rdate;
      p_wq_table(v_index).lab_cond := r_lab.conduct;
      p_wq_table(v_index).lab_ph := r_lab.ph;
      p_wq_table(v_index).lab_analysis := r_lab.analysis_no;
      p_wq_table(v_index).bottle_id := 'bottle'||p_rn||'-'||r_lab.analysis_no;
      
      IF r_lab.depth IS NULL THEN
        p_wq_table(v_index).depth := 0.0;
      ELSE
        p_wq_table(v_index).depth := r_lab.depth;
      END IF;
      
      
    END IF;
    
  END LOOP;
  
END xml_waterQuality_data;



END BOM_data;
/

show errors;