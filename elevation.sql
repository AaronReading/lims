**returns elevations for all water levels**
with q_rns as (
  select rn off_rn,
          pipe off_pipe
  from gw_off_rn_lists
  where code = 'CBGMA_ROP_AE_LOW_ASSESS_SITES'
),
q_elevation as (
  select  w.rn bore_no, 
        w.pipe bore_pipe, 
        w.rdate wl_date, 
        w.measurement wl, 
        e.datum el_datum, 
        e.elevation elev, 
        e.rdate el_date, 
        e.pipe el_pipe, 
        e.elevation + w.measurement elevation,
        e.rdate - w.rdate date_diff
  from gw_wlvdets w, gw_elvdets e, q_rns r
  where w.rn = e.rn
  and w.rn = r.off_rn
  and w.pipe = r.off_pipe
  and e.pipe = 'A'
  and e.meas_point = w.meas_point
  order by w.rn,w.pipe,w.rdate
  )
select bore_no, 
        bore_pipe,
        wl_date,
        wl,
        elev,
        el_date,
        elevation
from q_elevation el
where date_diff < 0
and date_diff in (select max(date_diff)
                           from q_elevation
                           where date_diff < 0
                           and wl_date = el.wl_date
                           and bore_no = el.bore_no
                          );


**elevation for latest water level***
with q_rns as (
  select rn off_rn,
          pipe off_pipe
  from gw_off_rn_lists
  where code = 'CBGMA_ROP_AE_LOW_ASSESS_SITES'
),
q_elevation as (
  select  w.rn bore_no, 
        w.pipe bore_pipe, 
        w.rdate wl_date, 
        w.measurement wl, 
        e.datum el_datum, 
        e.elevation elev, 
        e.rdate el_date, 
        e.pipe el_pipe, 
        e.elevation + w.measurement elevation,
        e.rdate - w.rdate date_diff
  from gw_wlvdets w, gw_elvdets e, q_rns r
  where w.rn = e.rn
  and w.rn = r.off_rn
  and w.pipe = r.off_pipe
  and e.pipe = 'A'
  and e.meas_point = w.meas_point
  and w.rdate in (select max(rdate)
                         from gw_wlvdets
                         where rn = w.rn
                         and pipe = w.pipe)
  order by w.rn,w.pipe,w.rdate
  )
select bore_no, 
        bore_pipe,
        wl_date,
        wl,
        elev,
        el_date,
        elevation
from q_elevation el
where date_diff < 0
and date_diff in (select max(date_diff)
                           from q_elevation
                           where date_diff < 0
                           and wl_date = el.wl_date
                           and bore_no = el.bore_no
                          );


select bore_no, 
        bore_pipe,
        wl_date,
        wl,
        elev,
        el_date,
        elevation
from (
  select  w.rn as bore_no, 
        w.pipe as bore_pipe, 
        w.rdate as wl_date, 
        w.measurement as wl, 
        e.datum as el_datum, 
        e.elevation as elev, 
        e.rdate as el_date, 
        e.pipe as el_pipe, 
        e.elevation + w.measurement as elevation,
        e.rdate - w.rdate as date_diff
  from gw_wlvdets w, gw_elvdets e, (
  select rn as off_rn,
          pipe as off_pipe
  from gw_off_rn_lists
  where code = 'CBGMA_ROP_AE_LOW_ASSESS_SITES'
  ) r
  where w.rn = e.rn
  and w.rn = r.off_rn
  and w.pipe = r.off_pipe
  and e.pipe = 'A'
  and e.meas_point = w.meas_point
  and w.rdate in (select max(rdate)
                         from gw_wlvdets
                         where rn = w.rn
                         and pipe = w.pipe)
  order by w.rn,w.pipe,w.rdate
  ) el
where date_diff < 0
and date_diff in (select max(date_diff)
                           from (
  select  w.rn as bore_no, 
        w.pipe as bore_pipe, 
        w.rdate as wl_date, 
        w.measurement as wl, 
        e.datum as el_datum, 
        e.elevation as elev, 
        e.rdate as el_date, 
        e.pipe as el_pipe, 
        e.elevation + w.measurement as elevation,
        e.rdate - w.rdate as date_diff
  from gw_wlvdets w, gw_elvdets e, (
  select rn as off_rn,
          pipe as off_pipe
  from gw_off_rn_lists
  where code = 'CBGMA_ROP_AE_LOW_ASSESS_SITES'
  ) r
  where w.rn = e.rn
  and w.rn = r.off_rn
  and w.pipe = r.off_pipe
  and e.pipe = 'A'
  and e.meas_point = w.meas_point
  and w.rdate in (select max(rdate)
                         from gw_wlvdets
                         where rn = w.rn
                         and pipe = w.pipe)
  order by w.rn,w.pipe,w.rdate
  )
                           where date_diff < 0
                           and wl_date = el.wl_date
                           and bore_no = el.bore_no
                  );


************************
select bore_no, 
        bore_pipe,
        wl_date,
        wl,
        elev,
        el_date,
        elevation
from (select  w.rn bore_no, 
        w.pipe bore_pipe, 
        w.rdate wl_date, 
        w.measurement wl, 
        e.datum el_datum, 
        e.elevation elev, 
        e.rdate el_date, 
        e.pipe el_pipe, 
        e.elevation + w.measurement elevation,
        e.rdate - w.rdate date_diff
  from gw_wlvdets w, gw_elvdets e, (
      select rn off_rn,
          pipe off_pipe
      from gw_off_rn_lists
      where code = 'CBGMA_ROP_AE_LOW_ASSESS_SITES'
      ) r
  where w.rn = e.rn
  and w.rn = r.off_rn
  and w.pipe = r.off_pipe
  and e.pipe = 'A'
  and e.meas_point = w.meas_point
  order by w.rn,w.pipe,w.rdate
  ) el
where date_diff < 0
and date_diff in (select max(date_diff)
                  from (select  w.rn bore_no, 
        w.pipe bore_pipe, 
        w.rdate wl_date, 
        w.measurement wl, 
        e.datum el_datum, 
        e.elevation elev, 
        e.rdate el_date, 
        e.pipe el_pipe, 
        e.elevation + w.measurement elevation,
        e.rdate - w.rdate date_diff
  from gw_wlvdets w, gw_elvdets e, (
      select rn off_rn,
          pipe off_pipe
      from gw_off_rn_lists
      where code = 'CBGMA_ROP_AE_LOW_ASSESS_SITES'
      ) r
  where w.rn = e.rn
  and w.rn = r.off_rn
  and w.pipe = r.off_pipe
  and e.pipe = 'A'
  and e.meas_point = w.meas_point
  order by w.rn,w.pipe,w.rdate
  ) el
                           where date_diff < 0
                           and wl_date = el.wl_date
                           and bore_no = el.bore_no
                          );






**latest water level**
select rn, pipe, rdate, meas_point, measurement, remark, logger
from gw_wlvdets w
where rdate in (select max(rdate)
                         from gw_wlvdets
                         where rn = w.rn
                         and pipe = w.pipe)






SELECT rn AS off_rn, pipe AS off_pipe
FROM gw_gw_off_rn_lists
WHERE code='CBGMA_ROP_AE_LOW_ASSESS_SITES';

SELECT w.rn AS bore_no, w.pipe AS bore_pipe, w.rdate AS wl_date, w.measurement AS wl, e.datum AS el_datum, e.elevation AS elev, e.rdate AS el_date, e.pipe AS el_pipe, e.elevation+w.measurement AS elevation, e.rdate-w.rdate AS date_diff
FROM gw_gw_wlvdets AS w, gw_gw_elvdets AS e, q_rns AS r
WHERE w.rn = e.rn   
and w.rn = r.off_rn   
and w.pipe = r.off_pipe   
and e.pipe = 'A'   
and e.meas_point = w.meas_point   
and w.rdate in (select max(rdate)
                          from gw_gw_wlvdets
                          where rn = w.rn
                          and pipe = w.pipe)
ORDER BY w.rn, w.pipe, w.rdate;


SELECT el.bore_no, el.bore_pipe, el.wl_date, el.wl, el.elev, el.elevation
FROM q_elevation AS el
WHERE date_diff < 0
and date_diff in (select max(date_diff)
                           from q_elevation
                           where date_diff < 0
                           and wl_date = el.wl_date
                           and bore_no = el.bore_no
                          );



***we have a winner***

SELECT rn AS off_rn, pipe AS off_pipe
FROM gw_gw_off_rn_lists
WHERE code='CBGMA_ROP_AE_LOW_ASSESS_SITES';


SELECT w.rn AS bore_no, w.pipe AS bore_pipe, w.rdate AS wl_date, w.measurement AS wl, e.datum AS el_datum, e.elevation AS elev, e.rdate AS el_date, e.pipe AS el_pipe, e.elevation+w.measurement AS elevation
FROM gw_gw_wlvdets AS w, gw_gw_elvdets AS e, q_rns AS r
WHERE w.rn = e.rn   
and w.rn = r.off_rn   
and w.pipe = r.off_pipe   
and e.pipe = r.off_pipe  
and e.meas_point = w.meas_point   
and w.rdate in (select max(rdate)
                          from gw_gw_wlvdets
                          where rn = w.rn
                          and pipe = w.pipe)
and e.rdate in (select max(rdate)
			from gw_gw_elvdets
			where rn = e.rn
			and pipe = e.pipe
			and meas_point = e.meas_point)
ORDER BY w.rn, w.pipe, w.rdate;

**** and now elevations for all water levels not just the latest one ****

SELECT w.rn, w.pipe, w.rdate, w.meas_point, w.measurement, w.measurement+e.elevation as adj, e.rdate, e.meas_point, e.elevation, e.datum
FROM gw_gw_wlvdets w, gw_gw_elvdets e
WHERE w.rn = e.rn
AND w.rn = 12100054
and w.meas_point = e.meas_point
and e.rdate in (select max(rdate) from gw_gw_elvdets where rn = e.rn and rdate <= w.rdate and meas_point = 'R')
order by w.rdate;