--grabs the analysis information for currently used analyses


DECLARE
  
    CURSOR c_test IS
        SELECT DISTINCT analysis, analysis_version
        FROM test
        WHERE  analysis NOT LIKE '%_WT'
        --and analysis = 'P_NITM_ICP'
        ORDER BY analysis, analysis_version;
    
    CURSOR c_component (anal VARCHAR2, anal_ver VARCHAR2) IS
        SELECT analysis, analysis_version, name, order_number, units, minimum_pql
        FROM versioned_component
        WHERE analysis = anal
        AND analysis_version = anal_ver
        ORDER BY order_number;
    
    CURSOR c_uncert (anal VARCHAR2, anal_ver VARCHAR2, comp_name VARCHAR2) IS
        SELECT uncertainty
        FROM anal_map
        WHERE analysis = anal
        AND analysis_version = anal_ver
        AND component_name = comp_name;
    
    
    v_uncertainty       anal_map.uncertainty%TYPE;
    v_units             VARCHAR2(20);
    v_analysis_ver      VARCHAR2(4);
    
    f_dir         VARCHAR2(40) := 'AARON_DIR';
    f_w_name	    VARCHAR2(40) := 'september_analysis_info.csv';
    f_w_ref       UTL_FILE.FILE_TYPE;
  
  
BEGIN
  
    --first open the file
    f_w_ref := UTL_FILE.FOPEN (f_dir, f_w_name, 'w');
    UTL_FILE.PUT_LINE (f_w_ref, 'ANALYSIS,ANALYSIS_VERSION,COMPONENT,UNITS,MIN PQL,MEASURE UNCERTAINTY');
    
    --need to go through each analysis/version
    FOR r_test IN c_test LOOP
        
        --get the remaining info from versioned_component
        FOR r_component IN c_component (r_test.analysis, r_test.analysis_version) LOOP
            
            
            --get the uncertainty from analysis_map
            OPEN c_uncert (r_component.analysis, r_component.analysis_version, r_component.name);
            FETCH c_uncert INTO v_uncertainty;
            CLOSE c_uncert;
    
            v_analysis_ver := TRIM( r_test.analysis_version );
            v_units := TRIM(r_component.units);
--            DBMS_OUTPUT.PUT_LINE(r_test.analysis ||','||v_analysis_ver||','||r_component.name||','||v_units||','||r_component.minimum_pql||','||v_uncertainty);
            UTL_FILE.PUT_LINE(f_w_ref, r_test.analysis||','||v_analysis_ver||','||r_component.name||','||v_units||','||r_component.minimum_pql||','||v_uncertainty);
    
        END LOOP;
    END LOOP;
  
    UTL_FILE.FCLOSE (f_w_ref);
  
END;
/
