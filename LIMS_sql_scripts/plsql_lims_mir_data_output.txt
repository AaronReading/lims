--this script is to extract all the samples that have been analysed for S_MIR1&2
--then the information is to be imported into an access database.
--this will then be converted into an access script to be auto updated.


DECLARE
  
  CURSOR c_sample_id IS
    SELECT DISTINCT sample, 'A' AS blah
    FROM test
    WHERE analysis IN ('S_MIR1','S_MIR2')
    UNION
    SELECT DISTINCT sample, 'C' AS blah
    FROM c_test
    WHERE analysis IN ('S_MIR1','S_MIR2');
  
  CURSOR c_tests_a (sample_id VARCHAR2) IS
    SELECT test_number, analysis, date_completed, analysis_version, test_count
    FROM test
    WHERE analysis NOT IN ('S_MIR1','S_MIR2')
    AND sample = sample_id;
  
  CURSOR c_tests_c (sample_id VARCHAR2) IS
    SELECT test_number, analysis, date_completed, analysis_version, test_count
    FROM c_test
    WHERE analysis NOT IN ('S_MIR1','S_MIR2')
    AND sample = sample_id;
  
  CURSOR c_results_a (test_no VARCHAR2) IS
    SELECT name, text, value
    FROM result
    WHERE test_number = test_no
    ORDER BY order_number;
  
  CURSOR c_results_c (test_no VARCHAR2) IS
    SELECT name, text, value
    FROM c_result
    WHERE test_number = test_no
    ORDER BY order_number;
    
  CURSOR c_sample_name (sample_id VARCHAR2) IS
    SELECT id_text
    FROM sample
    WHERE id_numeric = sample_id;
    
  CURSOR c_sample_name_c (sample_id VARCHAR2) IS
    SELECT id_text
    FROM c_sample
    WHERE id_numeric = sample_id;
  
  CURSOR c_sample_a (sample_id VARCHAR2) IS
    SELECT * --id_text, project_id, sali_export, sample_type, site, upper_depth, lower_depth, observation, horizon, sample_number, bulk, datum, zone, easting, northing, latitude_dec_deg, longitude_dec_deg, aus_soil_class
    FROM sample
    WHERE id_numeric = sample_id;
  
  CURSOR c_sample_c (sample_id VARCHAR2) IS
    SELECT * --id_text, project_id, sali_export, sample_type, site, upper_depth, lower_depth, observation, horizon, sample_number, bulk, datum, zone, easting, northing, latitude_dec_deg, longitude_dec_deg, aus_soil_class
    FROM c_sample
    WHERE id_numeric = sample_id;
    
  CURSOR c_units (method_id VARCHAR2, method_version VARCHAR2, component_id VARCHAR2) IS
    SELECT units
    FROM versioned_component
    WHERE analysis = method_id
    AND analysis_version = method_version
    AND name = component_id;
    
  CURSOR c_alhs (method_id VARCHAR2, method_version VARCHAR2, component_id VARCHAR2) IS
    SELECT alhs
    FROM anal_map
    WHERE analysis = method_id
    AND analysis_version = method_version
    AND component_name = component_id;
  
  CURSOR c_desc (method_id VARCHAR2, method_version VARCHAR2) IS
    SELECT description
    FROM versioned_analysis
    WHERE identity = method_id
    AND analysis_version = method_version;
  
  TYPE samples_record IS RECORD(
    sample_id     sample.id_text%TYPE,
    project_id    sample.project_id%TYPE,
    sali_export   sample.sali_export%TYPE,
    sample_type   sample.sample_type%TYPE,
    site          sample.site%TYPE,
    upper_depth   sample.upper_depth%TYPE,
    lower_depth   sample.lower_depth%TYPE,
    observation   sample.observation%TYPE,
    horizon       sample.horizon%TYPE,
    sample_number   sample.sample_number%TYPE,
    bulk          sample.bulk%TYPE,
    datum         sample.datum%TYPE,
    zone          sample.zone%TYPE,
    easting       sample.easting%TYPE,
    northing      sample.northing%TYPE,
    latitude_dec_deg    sample.latitude_dec_deg%TYPE,
    longitude_dec_deg   sample.longitude_dec_deg%TYPE,
    aus_soil_class    sample.aus_soil_class%TYPE
  );
  
  --methods record is about the components
  TYPE methods_record IS RECORD(
    method_id         versioned_component.analysis%TYPE,
    method_version    versioned_component.analysis_version%TYPE,
    component_id      versioned_component.name%TYPE,
    units             versioned_component.units%TYPE,
    green_book_code   anal_map.alhs%TYPE,
    method_desc       versioned_analysis.description%TYPE
  );
  
  TYPE results_record IS RECORD(
    sample_id         sample.id_text%TYPE,
    lab_id            VARCHAR2(6),
    method_id         versioned_component.analysis%TYPE,
    method_version    versioned_component.analysis_version%TYPE,
    component_id      versioned_component.name%TYPE,
    date_completed    test.date_completed%TYPE,
    instrument_result   result.value%TYPE,
    formatted_result    result.text%TYPE,
    test_count        test.test_count%TYPE,
    test_number         NUMBER(7)
  );
  
  TYPE sample_table IS TABLE OF
    samples_record
    INDEX BY PLS_INTEGER; --index is sample.id_numeric (e.g. '    900003')
  
  TYPE methods_table IS TABLE OF
    methods_record
    INDEX BY VARCHAR2(52); --index is analysis id and version and component (10+2+40)
  
  TYPE results_table IS TABLE OF
    results_record
    INDEX BY VARCHAR2(47); --index is test_number and component (e.g.'   4000354'||'TPA')
  
  f_dir         VARCHAR2(40) := 'D:\LIMS_DB\Data\readinga';
  f_w1_name	    VARCHAR2(30) := 'results_'||TO_CHAR(SYSDATE,'yyyymmdd')||'.csv';
  f_w2_name	    VARCHAR2(30) := 'methods_'||TO_CHAR(SYSDATE,'yyyymmdd')||'.csv';
  f_w3_name	    VARCHAR2(30) := 'samples_'||TO_CHAR(SYSDATE,'yyyymmdd')||'.csv';
  f_w1_ref       UTL_FILE.FILE_TYPE;
  f_w2_ref       UTL_FILE.FILE_TYPE;
  f_w3_ref       UTL_FILE.FILE_TYPE;
  
  t_samples       sample_table;
  t_methods       methods_table;
  t_results       results_table;
  v_sample_data   sample%ROWTYPE;
  v_sample_index    NUMBER(7);
  v_results_index   VARCHAR2(47);
  v_methods_index   VARCHAR2(52);
  
BEGIN
  
  --need to query test and c_test where analysis in ('S_MIR1','S_MIR2')
  --to supply a distinct list of sample id's
  FOR r_sample_id IN c_sample_id LOOP
  
    IF r_sample_id.blah = 'A' THEN --if it is in the active area
      
      v_sample_index := TO_NUMBER(LTRIM(r_sample_id.sample));
      
      IF NOT t_samples.EXISTS(v_sample_index) THEN
        --have to get all the sample data from the sample table using the sample IDs
        OPEN c_sample_a (r_sample_id.sample);
        FETCH c_sample_a INTO v_sample_data;
        CLOSE c_sample_a;
      
      
        -- if there is not already a record for this sample then insert one into the table
        t_samples(v_sample_index).sample_id := v_sample_data.id_text;
        t_samples(v_sample_index).project_id := v_sample_data.project_id;
        t_samples(v_sample_index).sali_export := v_sample_data.sali_export;
        t_samples(v_sample_index).sample_type := v_sample_data.sample_type;
        t_samples(v_sample_index).site := v_sample_data.site;
        t_samples(v_sample_index).upper_depth := v_sample_data.upper_depth;
        t_samples(v_sample_index).lower_depth := v_sample_data.lower_depth;
        t_samples(v_sample_index).observation := v_sample_data.observation;
        t_samples(v_sample_index).horizon := v_sample_data.horizon;
        t_samples(v_sample_index).sample_number := v_sample_data.sample_number;
        t_samples(v_sample_index).bulk := v_sample_data.bulk;
        t_samples(v_sample_index).datum := v_sample_data.datum;
        t_samples(v_sample_index).zone := v_sample_data.zone;
        t_samples(v_sample_index).easting := v_sample_data.easting;
        t_samples(v_sample_index).northing := v_sample_data.northing;
        t_samples(v_sample_index).latitude_dec_deg := v_sample_data.latitude_dec_deg;
        t_samples(v_sample_index).longitude_dec_deg := v_sample_data.longitude_dec_deg;
        t_samples(v_sample_index).aus_soil_class := v_sample_data.aus_soil_class;
        
      END IF;
      
      --loop through this list of sample IDs to get all the tests assigned to that sample
      --excluding S_MIR1 & S_MIR2
      FOR r_tests IN c_tests_a (r_sample_id.sample) LOOP
        --SELECT test_number, analysis, date_completed, analysis_version
        --have to get the results data  --will result in multiple results from a test_number therefore loop!
        --SELECT name, text, value
        FOR r_results IN c_results_a (r_tests.test_number) LOOP
          --index is test_number and component (e.g.'   4000354'||'TPA')
          v_results_index := TO_NUMBER(LTRIM(r_tests.test_number))||r_results.name;
          
          --check if already in the results table, if not then add to table
          IF NOT t_results.EXISTS(v_results_index) THEN
            t_results(v_results_index).lab_id := 'DSITIA';
            t_results(v_results_index).method_id := r_tests.analysis;
            t_results(v_results_index).method_version := LTRIM(r_tests.analysis_version);
            t_results(v_results_index).component_id := r_results.name;
            t_results(v_results_index).date_completed := r_tests.date_completed;
            t_results(v_results_index).instrument_result := r_results.value;
            t_results(v_results_index).formatted_result := r_results.text;
            t_results(v_results_index).test_number := TO_NUMBER(LTRIM(r_tests.test_number));
            t_results(v_results_index).test_count := LTRIM(r_tests.test_count);
            
            OPEN c_sample_name (r_sample_id.sample);
            FETCH c_sample_name INTO t_results(v_results_index).sample_id;
            CLOSE c_sample_name;
            
          END IF;
          
          --have to get the method data using the list of methods
          --index is analysis id and version and component (10+2+40)
          v_methods_index := r_tests.analysis||LTRIM(r_tests.analysis_version)||r_results.name;
          
          IF NOT t_methods.EXISTS(v_methods_index) THEN
            t_methods(v_methods_index).method_id := r_tests.analysis;
            t_methods(v_methods_index).method_version := LTRIM(r_tests.analysis_version);
            t_methods(v_methods_index).component_id := r_results.name;
            
            OPEN c_units (r_tests.analysis, r_tests.analysis_version, r_results.name);
            FETCH c_units INTO t_methods(v_methods_index).units;
            CLOSE c_units;
            
            OPEN c_alhs (r_tests.analysis, r_tests.analysis_version, r_results.name);
            FETCH c_alhs INTO t_methods(v_methods_index).green_book_code;
            CLOSE c_alhs;
            
            OPEN c_desc (r_tests.analysis, r_tests.analysis_version);
            FETCH c_desc INTO t_methods(v_methods_index).method_desc;
            CLOSE c_desc;
            
          END IF;
          
        END LOOP;
        
      END LOOP;
      
    ELSE
      --else it is in the committed area
      
      v_sample_index := TO_NUMBER(LTRIM(r_sample_id.sample));
      
      IF NOT t_samples.EXISTS(v_sample_index) THEN
        --have to get all the sample data from the sample table using the sample IDs
        OPEN c_sample_c (r_sample_id.sample);
        FETCH c_sample_c INTO v_sample_data;
        CLOSE c_sample_c;
      
      
        -- if there is not already a record for this sample then insert one into the table
        t_samples(v_sample_index).sample_id := v_sample_data.id_text;
        t_samples(v_sample_index).project_id := v_sample_data.project_id;
        t_samples(v_sample_index).sali_export := v_sample_data.sali_export;
        t_samples(v_sample_index).sample_type := v_sample_data.sample_type;
        t_samples(v_sample_index).site := v_sample_data.site;
        t_samples(v_sample_index).upper_depth := v_sample_data.upper_depth;
        t_samples(v_sample_index).lower_depth := v_sample_data.lower_depth;
        t_samples(v_sample_index).observation := v_sample_data.observation;
        t_samples(v_sample_index).horizon := v_sample_data.horizon;
        t_samples(v_sample_index).sample_number := v_sample_data.sample_number;
        t_samples(v_sample_index).bulk := v_sample_data.bulk;
        t_samples(v_sample_index).datum := v_sample_data.datum;
        t_samples(v_sample_index).zone := v_sample_data.zone;
        t_samples(v_sample_index).easting := v_sample_data.easting;
        t_samples(v_sample_index).northing := v_sample_data.northing;
        t_samples(v_sample_index).latitude_dec_deg := v_sample_data.latitude_dec_deg;
        t_samples(v_sample_index).longitude_dec_deg := v_sample_data.longitude_dec_deg;
        t_samples(v_sample_index).aus_soil_class := v_sample_data.aus_soil_class;
        
      END IF;
      
      --loop through this list of sample IDs to get all the tests assigned to that sample
      --excluding S_MIR1 & S_MIR2
      FOR r_tests IN c_tests_c (r_sample_id.sample) LOOP
        --SELECT test_number, analysis, date_completed, analysis_version
        --have to get the results data  --will result in multiple results from a test_number therefore loop!
        --SELECT name, text, value
        FOR r_results IN c_results_c (r_tests.test_number) LOOP
          --index is test_number and component (e.g.'   4000354'||'TPA')
          v_results_index := TO_NUMBER(LTRIM(r_tests.test_number))||r_results.name;
          
          --check if already in the results table, if not then add to table
          IF NOT t_results.EXISTS(v_results_index) THEN
            t_results(v_results_index).lab_id := 'DSITIA';
            t_results(v_results_index).method_id := r_tests.analysis;
            t_results(v_results_index).method_version := LTRIM(r_tests.analysis_version);
            t_results(v_results_index).component_id := r_results.name;
            t_results(v_results_index).date_completed := r_tests.date_completed;
            t_results(v_results_index).instrument_result := r_results.value;
            t_results(v_results_index).formatted_result := r_results.text;
            t_results(v_results_index).test_number := TO_NUMBER(LTRIM(r_tests.test_number));
            t_results(v_results_index).test_count := LTRIM(r_tests.test_count);
            
            OPEN c_sample_name_c (r_sample_id.sample);
            FETCH c_sample_name_c INTO t_results(v_results_index).sample_id;
            CLOSE c_sample_name_c;
            
          END IF;
          
          --have to get the method data using the list of methods
          --index is analysis id and version and component (10+2+40)
          v_methods_index := r_tests.analysis||LTRIM(r_tests.analysis_version)||r_results.name;
          
          IF NOT t_methods.EXISTS(v_methods_index) THEN
            t_methods(v_methods_index).method_id := r_tests.analysis;
            t_methods(v_methods_index).method_version := LTRIM(r_tests.analysis_version);
            t_methods(v_methods_index).component_id := r_results.name;
            
            OPEN c_units (r_tests.analysis, r_tests.analysis_version, r_results.name);
            FETCH c_units INTO t_methods(v_methods_index).units;
            CLOSE c_units;
            
            OPEN c_alhs (r_tests.analysis, r_tests.analysis_version, r_results.name);
            FETCH c_alhs INTO t_methods(v_methods_index).green_book_code;
            CLOSE c_alhs;
            
            OPEN c_desc (r_tests.analysis, r_tests.analysis_version);
            FETCH c_desc INTO t_methods(v_methods_index).method_desc;
            CLOSE c_desc;
            
          END IF;
          
        END LOOP;
        
      END LOOP;
      
      
    END IF;
    
  END LOOP;
  
  --now to output the tables into the files
  f_w1_ref := UTL_FILE.FOPEN (f_dir, f_w1_name, 'w'); --results
  f_w2_ref := UTL_FILE.FOPEN (f_dir, f_w2_name, 'w'); --methods
  f_w3_ref := UTL_FILE.FOPEN (f_dir, f_w3_name, 'w'); --samples
  UTL_FILE.PUT_LINE (f_w1_ref, 'SAMPLE_ID,LIMS_TEST_NUMBER,TEST_COUNT,LABORATORY_ID,METHOD_ID,METHOD_VERSION,COMPONENT_ID,DATE_COMPLETED,INSTRUMENT_RESULT,FORMATTED_RESULT');
  UTL_FILE.PUT_LINE (f_w2_ref, 'METHOD_ID,METHOD_VERISON,COMPONENT_ID,UNITS,GREEN_BOOK_CODE,METHOD_DESCRIPTION');
  UTL_FILE.PUT_LINE (f_w3_ref, 'SAMPLE_ID,LIMS_SAMPLE_NUMBER,PROJECT_ID,SALI_EXPORT,SAMPLE_TYPE,SITE,UPPER_DEPTH,LOWER_DEPTH,OBSERVATION,HORIZON,SAMPLE_NUMBER,BULK,DATUM,ZONE,EASTING,NORTHING,LATITUDE_DEC_DEG,LONGITUDE_DEC_DEG,AUS_SOIL_CLASS');
 
  --first results
  IF t_results.FIRST IS NOT NULL THEN
    
    v_results_index := t_results.FIRST;
    LOOP
      UTL_FILE.PUT_LINE (f_w1_ref, t_results(v_results_index).sample_id ||','||
                                   t_results(v_results_index).test_number ||','||
                                   t_results(v_results_index).test_count ||','||
                                   t_results(v_results_index).lab_id ||','||
                                   t_results(v_results_index).method_id ||','||
                                   t_results(v_results_index).method_version ||','||
                                   t_results(v_results_index).component_id ||','||
                                   TO_CHAR(t_results(v_results_index).date_completed, 'DD/MM/YYYY') ||','||
                                   t_results(v_results_index).instrument_result ||','||
                                   t_results(v_results_index).formatted_result);
    
    
      EXIT WHEN v_results_index = t_results.LAST;
      v_results_index := t_results.NEXT(v_results_index);
    END LOOP;
  
  END IF;
  
  --then methods
  IF t_methods.FIRST IS NOT NULL THEN
    
    v_methods_index := t_methods.FIRST;
    LOOP
      UTL_FILE.PUT_LINE (f_w2_ref, t_methods(v_methods_index).method_id ||','||
                                   t_methods(v_methods_index).method_version ||','||
                                   t_methods(v_methods_index).component_id ||','||
                                   TRIM(t_methods(v_methods_index).units) ||','||
                                   TRIM(t_methods(v_methods_index).green_book_code) ||','||
                                   t_methods(v_methods_index).method_desc);
                                   
      EXIT WHEN v_methods_index = t_methods.LAST;
      v_methods_index := t_methods.NEXT(v_methods_index);
    END LOOP;
  
  END IF;
  
  --then samples
  IF t_samples.FIRST IS NOT NULL THEN
    
    v_sample_index := t_samples.FIRST;
    LOOP
      UTL_FILE.PUT_LINE (f_w3_ref, t_samples(v_sample_index).sample_id ||','||
                                   v_sample_index ||','||
                                   TRIM(t_samples(v_sample_index).project_id) ||','||
                                   t_samples(v_sample_index).sali_export ||','||
                                   TRIM(t_samples(v_sample_index).sample_type) ||','||
                                   TRIM(t_samples(v_sample_index).site) ||','||
                                   t_samples(v_sample_index).upper_depth ||','||
                                   t_samples(v_sample_index).lower_depth ||','||
                                   t_samples(v_sample_index).observation ||','||
                                   t_samples(v_sample_index).horizon ||','||
                                   t_samples(v_sample_index).sample_number ||','||
                                   t_samples(v_sample_index).bulk ||','||
                                   t_samples(v_sample_index).datum ||','||
                                   t_samples(v_sample_index).zone ||','||
                                   t_samples(v_sample_index).easting ||','||
                                   t_samples(v_sample_index).northing ||','||
                                   TRIM(t_samples(v_sample_index).latitude_dec_deg) ||','||
                                   TRIM(t_samples(v_sample_index).longitude_dec_deg) ||','||
                                   TRIM(t_samples(v_sample_index).aus_soil_class));
                                   
      EXIT WHEN v_sample_index = t_samples.LAST;
      v_sample_index := t_samples.NEXT(v_sample_index);
    END LOOP;
  
  END IF;
  
  
  UTL_FILE.FCLOSE (f_w1_ref);
  UTL_FILE.FCLOSE (f_w2_ref);
  UTL_FILE.FCLOSE (f_w3_ref);
  
END;
/




