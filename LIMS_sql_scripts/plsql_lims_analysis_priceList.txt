
DECLARE
  
  CURSOR c_tests IS
    SELECT *
    FROM iqm_period_charges;
  
  
  f_dir         VARCHAR2(40) := 'D:\LIMS_DB\Data\Rob_Data_Output';
  f_w_name	    VARCHAR2(30) := 'prices_08-14.csv';
  f_w_ref       UTL_FILE.FILE_TYPE;
  
  TYPE prices_list IS RECORD(
    yr0809   iqm_period_charges.value%TYPE,
    yr0910   iqm_period_charges.value%TYPE,
    yr1011   iqm_period_charges.value%TYPE,
    yr1112   iqm_period_charges.value%TYPE,
    yr1213   iqm_period_charges.value%TYPE,
    yr1314   iqm_period_charges.value%TYPE
  );
  
  TYPE analysis_table IS TABLE OF
    prices_list
    INDEX BY VARCHAR2(10);
  
  t_prices          analysis_table;
  v_index             VARCHAR2(10);
  
BEGIN
  
  --first open the file
  f_w_ref := UTL_FILE.FOPEN (f_dir, f_w_name, 'w');
  UTL_FILE.PUT_LINE (f_w_ref, 'TEST,2008-2009,2009-2010,2010-2011,2011-2012,2012-2013,2013-2014');
  
  --for each charge_id
  FOR r_tests IN c_tests LOOP
    
    --check that the entry exists
--    IF t_prices.EXISTS(r_tests.charge_id) THEN
      
      --it does so check the period_id and insert the value into the table where appropriate
      CASE r_tests.period_id
        WHEN '2008-2009' THEN t_prices(r_tests.charge_id).yr0809 := r_tests.value;
        WHEN '2009-2010' THEN t_prices(r_tests.charge_id).yr0910 := r_tests.value;
        WHEN '2010-2011' THEN t_prices(r_tests.charge_id).yr1011 := r_tests.value;
        WHEN '2011-2012' THEN t_prices(r_tests.charge_id).yr1112 := r_tests.value;
        WHEN '2012-2013' THEN t_prices(r_tests.charge_id).yr1213 := r_tests.value;
        WHEN '2013-2014' THEN t_prices(r_tests.charge_id).yr1314 := r_tests.value;
        ELSE NULL;
      END CASE;
      
--    ELSE
      --it does not have an entry
      
--    END IF;
    
    
  END LOOP;
  
  v_index := t_prices.FIRST;
      LOOP
        --DBMS_OUTPUT.PUT_LINE(v_index||','||t_prices(v_index).yr1112||','||t_prices(v_index).yr1213||','||t_prices(v_index).yr1314);
        
        UTL_FILE.PUT_LINE(f_w_ref, v_index||','||t_prices(v_index).yr0809||','||t_prices(v_index).yr0910||','||t_prices(v_index).yr1011||','||t_prices(v_index).yr1112||','||t_prices(v_index).yr1213||','||t_prices(v_index).yr1314);
        
        EXIT WHEN v_index = t_prices.LAST;
        v_index := t_prices.NEXT(v_index);
      END LOOP;
      
      
  UTL_FILE.FCLOSE (f_w_ref);
  
END;
/
  