{*****************************************************************************}
{                                                                             }
{ IIIIII  QQQQQQ    MM      MM  ********************************************* }
{   II   QQ    QQ   MMMM  MMMM  *                SAMPLEMANAGER              * }
{   II   QQ    QQ   MM MMMM MM  *        INVOICE AND QUOTATION MODULE       * }
{   II   QQ  q QQ   MM  MM  MM  *                                           * }
{   II   QQ   qq    MM      MM  *        � Thermo Electron 2005             * }
{ IIIIII  QQQQQ qq  WW      WW  ********************************************* }
{                                                                             }
{*****************************************************************************}

{*****************************************************************************
*                                                                             
*       MODULE  : IQM_CHARGES                                                   
*       VERSION : 4.0                                                         
*       AUTHOR  : Thermo Electron                                            
*       DATE    : 08 April 2005                                                 
*   DESCRIPTION : Charges handling routines to support IQM                
*                                                                             
******************************************************************************
* Modification History                                                        
*                                                                             
******************************************************************************}

join library $LIB_UTILS

join library IQM_LIB
join library IQM_DEBUG

ENABLE WINDOWS
SET COMPILE_OPTION DECLARE
SET NAME "defer/"
SET NOTPROTECTED



{==============================================================================}
{                                  ROUTINES                                    }
{==============================================================================}

{******************************************************************************}

GLOBAL ROUTINE collate_charges (theinvoice , VALUE counter)

declare new_jobname , new_project

	new_jobname = SELECT sample . job_name
		WHERE id_numeric = theinvoice . sample_set[counter]

	new_project = SELECT sample . project_id

	IF (new_jobname != theinvoice . last_job) AND !blank(new_jobname) THEN

		IF GLOBAL("IQM_GET_DATA_FROM_JOB") AND blank(new_project) THEN

			new_project = SELECT job_header . project_id
					WHERE job_name = new_jobname

		ENDIF

	ENDIF


		

	IF GLOBAL("IQM_CHARGE_FOR_PROJECT") THEN

		IF !blank(new_project) AND (new_project != theinvoice . last_project) THEN

			charges_for_project (theinvoice)

			theinvoice . last_project = new_project

			IF GLOBAL ("IQM_DEBUG") AND 
				(index(GLOBAL("IQM_DEBUG_FLAG") , "C") > 0) THEN

				debug_C (theinvoice.items, "Project Charges")

			ENDIF

		ENDIF

	ENDIF

	IF GLOBAL("IQM_CHARGE_FOR_JOB") THEN

		IF !blank(new_jobname) AND (new_jobname != theinvoice . last_job) THEN

			charges_for_job (theinvoice , new_jobname , new_project)

			theinvoice . last_job = new_jobname

			IF GLOBAL ("IQM_DEBUG") AND 
				(index(GLOBAL("IQM_DEBUG_FLAG") , "C") > 0) THEN

				debug_C (theinvoice.items, "Job Charges")

			ENDIF

		ENDIF

	ENDIF

	IF GLOBAL("IQM_CHARGE_FOR_SAMPLE") THEN

		IF !blank(theinvoice . sampleset[counter]) THEN

			charges_for_sample (theinvoice , counter , new_jobname , new_project)

		ENDIF

	ENDIF

	IF GLOBAL("IQM_CHARGE_FOR_TEST") AND GLOBAL("IQM_CHARGE_FOR_SCHEDULE") THEN

		collate_schedule (theinvoice , counter , new_jobname , new_project)

	ELSEIF GLOBAL("IQM_CHARGE_FOR_SCHEDULE") THEN

		collate_schedule (theinvoice , counter , new_jobname , new_project)

	ELSEIF GLOBAL("IQM_CHARGE_FOR_TEST") THEN

		collate_test_charges (theinvoice , counter , new_jobname , new_project)

	ENDIF


	IF GLOBAL ("IQM_DEBUG") AND 
		(index(GLOBAL("IQM_DEBUG_FLAG") , "C") > 0) THEN

			debug_C (theinvoice.items, "Sample Charges")

	ENDIF

ENDROUTINE

{******************************************************************************}

ROUTINE charges_for_project (theinvoice)

declare invoiced , mischarge

    	invoiced = SELECT project_info . iqm_invoice
		WHERE project_id = theinvoice . header . project_id

    	IF (invoiced != EMPTY) THEN

		theinvoice . tmplitem . job_name = EMPTY
		theinvoice . tmplitem . sample_id = EMPTY
		theinvoice . tmplitem . test_number = EMPTY
		theinvoice . tmplitem . analysis_id = EMPTY
		
		theinvoice . tmplitem . project_id = theinvoice . header . project_id
		theinvoice . tmplitem . invoice_text = select project_info . description
       theinvoice . tmplitem . pricing = select project_info . iqm_pricing 

		IF theinvoice . tmplitem . invoice_text = "" THEN

			theinvoice . tmplitem . invoice_text = "Project : " : theinvoice . header . project_id

		ENDIF

		IF (invoiced = "") THEN

			theinvoice . tmplitem . iqm_code = select project_info . iqm_code

			book_line_item (theinvoice , "R")

		ENDIF

		IF GLOBAL("IQM_CHARGE_FOR_MISC")THEN

			theinvoice . tmplitem . iqm_code = EMPTY

			mischarge = SELECT iqm_misc_charges . entry_code 
					WHERE (table_type = "R")
					  AND (entity = strip(theinvoice . header . project_id))
					  AND (iqm_invoice = "")

			WHILE mischarge != EMPTY DO

				theinvoice . lineitem . miscellaneous = mischarge 
				theinvoice . lineitem . cost_centre =
					     SELECT iqm_misc_charges . group_id
				theinvoice . tmplitem . invoice_text = "Miscellaneous charge"

					book_line_item (theinvoice , "M")

				NEXT iqm_misc_charges
				mischarge = SELECT iqm_misc_charges . entry_code 

			ENDWHILE

		ENDIF

	ENDIF


	
ENDROUTINE

{******************************************************************************}

ROUTINE charges_for_job (theinvoice , thejob , theproject )

declare invoiced , mischarge

    	invoiced = SELECT job_header . iqm_invoice
		WHERE job_name = thejob

    	IF (invoiced != EMPTY) THEN

		theinvoice . tmplitem . sample_id = EMPTY
		theinvoice . tmplitem . test_number = EMPTY
		theinvoice . tmplitem . analysis_id = EMPTY
		
		theinvoice . tmplitem . project_id = theproject
		theinvoice . tmplitem . job_name = thejob
		theinvoice . tmplitem . invoice_text = select job_header . description
       theinvoice . tmplitem . pricing = select project_info . iqm_pricing 


		IF theinvoice . tmplitem . invoice_text = "" THEN

			theinvoice . tmplitem . invoice_text = "Job : ":thejob

		ENDIF

		IF (invoiced = "") THEN

			theinvoice . tmplitem . iqm_code = select job_header . iqm_code

			book_line_item (theinvoice , "J")

		ENDIF

		IF GLOBAL("IQM_CHARGE_FOR_MISC")THEN

			theinvoice . tmplitem . iqm_code = EMPTY

			mischarge = SELECT iqm_misc_charges . entry_code 
					WHERE (table_type = "J")
					  AND (entity = thejob)
					  AND (iqm_invoice = "")

			WHILE mischarge != EMPTY DO

				theinvoice . lineitem . miscellaneous = mischarge 
				theinvoice . lineitem . cost_centre =
					     SELECT iqm_misc_charges . group_id
				theinvoice . tmplitem . invoice_text = "Miscellaneous charge"

					book_line_item (theinvoice , "M")

				NEXT iqm_misc_charges
				mischarge = SELECT iqm_misc_charges . entry_code 

			ENDWHILE

		ENDIF

	ENDIF

ENDROUTINE

{******************************************************************************}

ROUTINE charges_for_sample (theinvoice , VALUE counter , thejob , theproject)

declare invoiced , mischarge 

    	invoiced = SELECT sample . iqm_invoice
		WHERE id_numeric = theinvoice . sampleset [counter]

	IF (select sample. standard) then

		RETURN

	ENDIF

    	IF (invoiced != EMPTY) THEN

		theinvoice . tmplitem . test_number = EMPTY
		theinvoice . tmplitem . analysis_id = EMPTY
		
		theinvoice . tmplitem . project_id = theproject
		theinvoice . tmplitem . job_name = thejob
		theinvoice . tmplitem . sample_id = theinvoice . sampleset [counter]
		theinvoice . tmplitem . invoice_text = SELECT sample . id_text 
						: " " :SELECT sample . sample_name

		IF (invoiced = "") THEN

			theinvoice . tmplitem . iqm_code = SELECT sample . iqm_code

			book_line_item (theinvoice , "S")

		ENDIF



		IF GLOBAL("IQM_CHARGE_FOR_PREP") THEN

			theinvoice . lineitem . preparation = select sample . preparation_id

			IF (!blank(theinvoice . lineitem . preparation)) AND (theinvoice . lineitem . preparation <> EMPTY) THEN	

				theinvoice . tmplitem . iqm_code = SELECT preparation . iqm_code
						WHERE identity = theinvoice . lineitem . preparation

				IF !blank(theinvoice . tmplitem . iqm_code) THEN

					theinvoice . lineitem . cost_centre =
						SELECT preparation . group_id
						
					theinvoice . tmplitem . invoice_text = 
						  SELECT preparation.description	

					book_line_item (theinvoice , "P")
				ENDIF

			ENDIF

		ENDIF


		IF GLOBAL("IQM_CHARGE_FOR_MISC")THEN

			theinvoice . tmplitem . iqm_code = EMPTY

			mischarge = SELECT iqm_misc_charges . entry_code 
					WHERE (table_type = "S")
					  AND (entity = strip(theinvoice . sampleset [ counter]))
					  AND (iqm_invoice = "")

			WHILE mischarge != EMPTY DO

				theinvoice . lineitem . miscellaneous = mischarge 
				theinvoice . lineitem . cost_centre =
					     SELECT iqm_misc_charges . group_id
				theinvoice . tmplitem . invoice_text = "Miscellaneous charge"

					book_line_item (theinvoice , "M")

				NEXT iqm_misc_charges
				mischarge = SELECT iqm_misc_charges . entry_code 

			ENDWHILE

		ENDIF

	ENDIF

ENDROUTINE

{******************************************************************************}

ROUTINE collate_test_charges (theinvoice , VALUE counter , thejob , theproject )


declare atest

	IF theinvoice . for_real THEN

		atest = SELECT test . test_number
			WHERE sample_id = theinvoice . sampleset[counter]
			AND status = "A"
			ORDER ON analysis_id
	
	ELSE

		atest = SELECT test . test_number
			WHERE sample_id = theinvoice . sampleset[counter]
			AND status != "X"
			ORDER ON analysis_id

	ENDIF

	WHILE atest != EMPTY DO

		charges_for_test ( theinvoice , atest , counter , thejob , theproject  )

		NEXT test
		atest = SELECT test . test_number

	ENDWHILE

ENDROUTINE

{******************************************************************************}

ROUTINE charges_for_test ( theinvoice , VALUE atest , VALUE counter , thejob , theproject  )

declare invoiced 

    	invoiced = SELECT test . iqm_invoice

    	IF (invoiced != EMPTY) THEN

		
		theinvoice . tmplitem . project_id = theproject
		theinvoice . tmplitem . job_name = thejob
		theinvoice . tmplitem . sample_id = theinvoice . sampleset [counter]
		theinvoice . tmplitem . test_number = atest
		theinvoice . tmplitem . analysis_id = SELECT test . analysis_id
		theinvoice . tmplitem . invoice_text = SELECT analysis.description 
				WHERE identity = theinvoice . tmplitem . analysis_id

		IF (invoiced = "") THEN

			theinvoice . tmplitem . iqm_code = SELECT analysis . iqm_code
			theinvoice . lineitem . cost_centre = SELECT analysis . group_id

			book_line_item (theinvoice , "A")

		ENDIF



		IF GLOBAL("IQM_CHARGE_FOR_PREP") THEN

			theinvoice . lineitem . preparation = select analysis . preparation_id

			IF !blank(theinvoice . lineitem . preparation) THEN

				theinvoice . tmplitem . iqm_code = SELECT preparation . iqm_code
						WHERE identity = theinvoice . lineitem . preparation

				IF !blank(theinvoice . tmplitem . iqm_code) THEN

					theinvoice . lineitem . cost_centre =
						SELECT preparation . group_id
						
					theinvoice . tmplitem . invoice_text = 
						  SELECT preparation.description	

					book_line_item (theinvoice , "P")
				ENDIF

			ENDIF

		ENDIF

	ENDIF

ENDROUTINE

{******************************************************************************}

ROUTINE collate_schedule (theinvoice , VALUE counter , thejob , theproject )


declare asched , invoiced

	invoiced = SELECT sample . iqm_invoice
			WHERE id_numeric = theinvoice . sampleset[counter]

    	IF (invoiced != EMPTY) THEN

		theinvoice . tmplitem . test_number = EMPTY
		theinvoice . tmplitem . analysis_id = EMPTY
		
		theinvoice . tmplitem . project_id = theproject
		theinvoice . tmplitem . job_name = thejob
		theinvoice . tmplitem . sample_id = theinvoice . sampleset [counter]
		theinvoice . tmplitem . invoice_text = SELECT sample . id_text 
						: " " :SELECT sample . sample_name

		IF (invoiced = "") THEN

			asched = SELECT sample . test_schedule

			IF !blank(asched) THEN

				IF GLOBAL ("IQM_CHARGE_FOR_TEST") THEN

					consolidate_schedule ( theinvoice , asched , counter  ,
								 thejob , theproject )

				ELSE

					charges_for_schedule ( theinvoice , asched )

				ENDIF

			ELSEIF GLOBAL ("IQM_CHARGE_FOR_TEST")

				collate_test_charges ( theinvoice , counter , thejob , theproject  )
							
			ENDIF

		ENDIF

	ENDIF

ENDROUTINE

{******************************************************************************}

ROUTINE charges_for_schedule ( theinvoice , asched )

declare subsched

	asched = SELECT test_sched_header . identity
			WHERE identity = asched

	IF asched != EMPTY THEN

		theinvoice . lineitem . schedule = asched
		theinvoice . tmplitem . iqm_code = SELECT test_sched_header . iqm_code
		theinvoice . tmplitem . invoice_text = SELECT test_sched_header . description
		theinvoice . lineitem . cost_centre  = SELECT test_sched_header . group_id

		book_line_item (theinvoice , "T")
	
		subsched = SELECT test_sched_entry . analysis_id
			WHERE identity = asched
			AND   is_analysis = FALSE

		WHILE subsched != EMPTY DO

			call_routine "charges_for_schedule" NEW CONTEXT
				using theinvoice , subsched

			NEXT test_sched_entry
			subsched = SELECT test_sched_entry . analysis_id

		ENDWHILE
	
	ENDIF
			
ENDROUTINE

{******************************************************************************}

ROUTINE consolidate_schedule ( theinvoice , VALUE thesched , VALUE counter , thejob , theproject )

declare testitem , chargeable 

	chargeable = FALSE

	init_sched_list (theinvoice . schedtest , testitem)

	consolidate_tests(theinvoice , thesched , testitem , chargeable )

	IF chargeable THEN

		theinvoice . schedtest . consolidate ()

		reconcile_tests (theinvoice , counter , thejob , theproject )
 
	ELSE

		collate_test_charges ( theinvoice , counter , thejob , theproject )

	ENDIF

ENDROUTINE

{******************************************************************************}

ROUTINE consolidate_tests (theinvoice , thesched , item , chargeable)

declare ananal , isanal , theent 

	theent = SELECT test_sched_header . iqm_code
			WHERE identity = thesched

	chargeable = chargeable OR !blank(theent)

   	ananal = SELECT test_sched_entry . std_test
			WHERE identity = thesched

	WHILE ananal != EMPTY DO

		IF ananal THEN

			isanal = SELECT test_sched_entry . is_analysis
			theent = SELECT test_sched_entry . analysis_id

	   		IF (isanal) AND (chargeable) THEN

				item = item . reset ()

				item . schedule_id = thesched
				item . analysis_id = theent
				item . rep_count = SELECT test_sched_entry . replicate_count

				theinvoice . schedtest . add (item)

	   		ELSEIF !(isanal)
	
				call_routine "consolidate_tests" NEW CONTEXT 
					using theinvoice , theent , item , chargeable

	   		ENDIF

		ENDIF

		NEXT test_sched_entry
		ananal = SELECT test_sched_entry . std_test

	endwhile

ENDROUTINE

{******************************************************************************}

ROUTINE reconcile_tests (theinvoice , VALUE counter , thejob , theproject )

declare theanals , testitem , cnt , ptr , base_sched 

 
	ARRAY theinvoice . testset ARRAYSIZE (0) = ""
	cnt = 0

	IF theinvoice . for_real THEN

		theanals = select test . analysis_id
				where (sample_id = theinvoice . sampleset[counter])
				  and (status = "A")   
				order on analysis_id

	ELSE  { not for real - preview mode }

		theanals = SELECT test . analysis_id
				WHERE (sample_id = theinvoice . sampleset[counter])
				  AND (status != "X")   
				ORDER ON analysis_id

	ENDIF

	WHILE theanals != EMPTY DO

		testitem = theinvoice . schedtest . get_by_index ("analysis_id" , theanals)

		IF testitem = EMPTY then      {an extra test - not scheduled}

			cnt = cnt + 1
			theinvoice . testset[cnt] = SELECT test . test_number

		ELSE

			IF testitem . rep_count > 0 then

				testitem . rep_count = testitem . rep_count - 1
				testitem . used = TRUE

			ELSE        { an additional test - beyond scheduled } 

				cnt = cnt + 1
				theinvoice . testset[cnt] = SELECT test . test_number

	
			ENDIF

		ENDIF

		NEXT test
		theanals = SELECT test . analysis_id

	ENDWHILE

	ptr = 1
	base_sched = " " 

	testitem = theinvoice . schedtest . get_by_index_number ("schedule_id" , ptr )
	
	WHILE testitem != EMPTY DO

		IF (testitem . schedule_id != base_sched) AND
			(testitem . used ) THEN
	
			base_sched = testitem . schedule_id

			charges_for_schedule (theinvoice , base_sched)

		ENDIF

		ptr = ptr + 1
		testitem = theinvoice . schedtest . get_by_index_number ("schedule_id",ptr)

	ENDWHILE

	ptr = 0

	WHILE ptr < cnt DO

		ptr = ptr + 1

		theanals = SELECT test . test_number
			WHERE test_number = theinvoice . testset[ptr]

		charges_for_test (theinvoice , theanals , counter , thejob , theproject )

	ENDWHILE
	
ENDROUTINE
{******************************************************************************}

GLOBAL ROUTINE book_line_item (theinvoice, VALUE source)


	theinvoice . lineitem . source          = source 
	theinvoice . lineitem . project_id 	= theinvoice . tmplitem . project_id
	theinvoice . lineitem . job_name 	= theinvoice . tmplitem . job_name
	theinvoice . lineitem . sample_id 	= theinvoice . tmplitem . sample_id
	theinvoice . lineitem . test_number 	= theinvoice . tmplitem . test_number
	theinvoice . lineitem . analysis_id 	= theinvoice . tmplitem . analysis_id
	theinvoice . lineitem . invoice_text    = theinvoice . tmplitem . invoice_text
	theinvoice . lineitem . iqm_code 	= theinvoice . tmplitem . iqm_code








	IF (theinvoice . lineitem . cost_centre = EMPTY)
	  AND  (theinvoice . lineitem . iqm_code != "") THEN

		theinvoice . lineitem . cost_centre = SELECT iqm_charges . group_id
			WHERE identity = theinvoice . lineitem . iqm_code

	ENDIF

	theinvoice . ordering = theinvoice . ordering + 1


	IF blank(theinvoice . tmplitem . entry_order) THEN

		theinvoice . lineitem . entry_order
			= rightstring ("0000":strip (theinvoice . ordering) , 4 )

	ELSE

		theinvoice . lineitem . entry_order
			= theinvoice . tmplitem . entry_order

	ENDIF

	theinvoice . items . add (theinvoice . lineitem)

	theinvoice . last_item = theinvoice . lineitem . entry_order

	theinvoice . lineitem = theinvoice . lineitem . reset ()

ENDROUTINE



