{******************************************************************************
*
* Module Name   : NRM_DB_LINK_GC
*
* Purpose       : DB Link global constants
*
* Specification : LIMS_Review_2009_Specification
*
* Author        : Peter Welsby  20 July 2010
*                 Copyright (C) Department of Environment and Resource Management Queensland 2012
*
*******************************************************************************
* Modification History  :
*
*   Ver   By     Date         Modifications
*  -----  ----  ---------  ----------------------------------------------------
*
******************************************************************************}

SET COMPILE_OPTION DECLARE
ENABLE WINDOWS
SET NOTPROTECTED

SET NAME "DEFER/"


{--- DB LINK database globals ------------------------------------------------}

GLOBAL CONSTANT DB_SOURCE_WASP          = "WASP"
GLOBAL CONSTANT WASP_LAB_ID_CODE        = "ERSc"            { Laboratory ID code }
GLOBAL CONSTANT WASP_IMPORT_LIBRARY     = "NRM_DB_LINK_WASP"
GLOBAL CONSTANT WASP_IMPORT_ROUTINE     = "WASP_IMPORT"
GLOBAL CONSTANT WASP_EXPORT_LIBRARY     = "NRM_DB_LINK_WASP"
GLOBAL CONSTANT WASP_EXPORT_ROUTINE     = "WASP_EXPORT"

GLOBAL CONSTANT DB_SOURCE_SALI          = "SALI"


{--- EVENT globals -----------------------------------------------------------}

{-- Event types --}
{   Event type is used to 1) identify the event and 2) as part of the report file name }
GLOBAL CONSTANT DB_IMPORT               = "IMPORT"          { Import event type }
GLOBAL CONSTANT DB_EXPORT               = "EXPORT"          { Export event type }
GLOBAL CONSTANT DB_REORDER              = "REORDER"         { Add additional reorder tests event type }
GLOBAL CONSTANT DB_MAINT                = "MAINT"           { Maintain action }

{-- Message sequence - use to order log messages --}
GLOBAL CONSTANT SEQ_EVENT               =  1                { Event level - top level }
GLOBAL CONSTANT SEQ_FILE                =  2                { File level }
GLOBAL CONSTANT SEQ_PROJECT             =  3                { Project check }
GLOBAL CONSTANT SEQ_MAND_JOB            = 11                { Mandatory job check }
GLOBAL CONSTANT SEQ_MAND_SAMPLE         = 12                { Mandatory sample check }
GLOBAL CONSTANT SEQ_MAND_BOTTLE         = 13                { Mandatory bottle check }
GLOBAL CONSTANT SEQ_MAND_SCHED          = 14                { Mandatory test schedule check }
GLOBAL CONSTANT SEQ_MAND_SUMMARY        = 15                { Mandatory file summary }
GLOBAL CONSTANT SEQ_ADD_JOB             = 21                { Add job error }
GLOBAL CONSTANT SEQ_ADD_SAMPLE          = 22                { Add sample error }
GLOBAL CONSTANT SEQ_ADD_TEST            = 23                { Add test error }
GLOBAL CONSTANT SEQ_SUMMARY             = 99                { End of event messages }

{-- Message severity - use to identify errors --}
GLOBAL CONSTANT DB_SEV_INFO             = "INFORMATION"     { Informational message }
GLOBAL CONSTANT DB_SEV_WARN             = "WARNING"         { Warning message }
GLOBAL CONSTANT DB_SEV_ERROR            = "ERROR"           { Error message }

{-- Message ID --}
GLOBAL CONSTANT DB_LINK_MESSAGE         = "NRM_DB_LINK_"    { Group name in message file }


{--- GENERAL globals ----------------------------------------------------------}

{-- Run modes --}
GLOBAL CONSTANT MODE_INTERACTIVE        = "INT"             { Interactive mode }
GLOBAL CONSTANT MODE_BACKGROUND         = "BKG"             { Background mode }
GLOBAL CONSTANT MODE_BATCH              = "BAT"             { Batch mode - call smp /report }

{-- Login data --}
GLOBAL CONSTANT JOB_DESC_AUTO           = " auto login"     { Job description }
GLOBAL CONSTANT JOB_DESC_MANUAL         = " interactive login by "

GLOBAL CONSTANT BILL_TYPE_NIL           = "NIL"
GLOBAL CONSTANT BILL_TYPE_JOURNAL       = "JOURNAL"
GLOBAL CONSTANT NO_TEST_SCHED           = "NO_TEST"         { Legitimate NO_TEST test schedule }


{--- SAMPLE globals ----------------------------------------------------------}

{-- Sample types --}
GLOBAL CONSTANT SAMPLE_TYPE_PLA         = "PLA"             { Sample type PLANT }
GLOBAL CONSTANT SAMPLE_TYPE_SOI         = "SOI"             { Sample type SOIL }
GLOBAL CONSTANT SAMPLE_TYPE_SOL         = "SOL"             { Sample type SOLUTION/EXTRACT }
GLOBAL CONSTANT SAMPLE_TYPE_WAT         = "WAT"             { Sample type WATER }


{--- FILE globals ------------------------------------------------------------}

{-- Export file delivery --}
GLOBAL CONSTANT DB_EXP_DELIV_MODE_FILE  = "FILE"             { Deliver data file by file system directory }
GLOBAL CONSTANT DB_EXP_DELIV_MODE_MAIL  = "MAIL"             { Deliver data file by email }


{--- REPORT globals ----------------------------------------------------------}

{-- Report types --}
{   Report type is used to 1) identify the report and 2) as part of the report file name }
GLOBAL CONSTANT REP_TYP_EVENT           = "EVE"             { Event report }
GLOBAL CONSTANT REP_TYP_ERROR           = "ERE"             { Event Error report }
GLOBAL CONSTANT REP_TYP_SUMMARY         = "SUM"             { Summary report }
GLOBAL CONSTANT REP_TYP_SAMPLE          = "SAM"             { Sample report }
GLOBAL CONSTANT REP_TYP_EXPORT          = "EXP"             { Export report }
GLOBAL CONSTANT REP_TYP_EXP_DELIVERY    = "DEL"             { Export report with data file }
GLOBAL CONSTANT REP_TYP_DATA_ERROR      = "ERR"             { Data Error report }
GLOBAL CONSTANT REP_TYP_DATA_WARN       = "WAR"             { Data Warning report }

{-- Import sample report line headers --}
GLOBAL CONSTANT REP_SAM_REORDER         = "Reorder"         { Line header - Additional test reorder }
GLOBAL CONSTANT REP_SAM_NEW             = "Login"           { Line header - New sample, login }
GLOBAL CONSTANT REP_SAM_ERROR           = "ERROR"           { Line header - Data error detected, not logged in }

{-- Error report line headers --}
GLOBAL CONSTANT REP_LINE_INFO           = "Information"
GLOBAL CONSTANT REP_LINE_WARN           = "Warning"
GLOBAL CONSTANT REP_LINE_ERROR          = "ERROR"

{-- Report messages --}
GLOBAL CONSTANT REP_ABORT_IMP           = "Import action aborted."
GLOBAL CONSTANT REP_ABORT_EXP           = "Export action aborted."
GLOBAL CONSTANT REP_LINE                = "#--------------------------------------"


{*****************************************************************************}
{*** END OF FILE *************************************************************}
{*****************************************************************************}
