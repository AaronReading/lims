{******************************************************************************
***   Filename : nrm_lib_latlong.rpf
***   Purpose  : Convert Latitude/Longitude & Easting/Northing to Degrees Latitude/Longitude
***
***   Notes    : Format of Latitude & Longitude string = S-123-12-12
***              . delimitors may be any of < > <.> <-> </>
***              . delimitor is not required after cardinal point
******************************************************************************
***   GLOBAL routines included:
***
***   get_latitude_degree  - Convert Latitude to decimal degrees
***   get_longitude_degree - Convert Longitude to decimal degrees
***
******************************************************************************
***   History:
***   Ver By   Date        Details
***   --- ---- ----------  ---------------------------------------------------
***       PJW  04-09-2004  Created
******************************************************************************}

ENABLE WINDOWS
SET NAME "DEFER/"

SET COMPILE_OPTION DECLARE
SET NOT_PROTECTED
SET STATUS_CHECK_SUPPRESS

{
JOIN LIBRARY $lib_utils
CONSTANT CRLF = ascii(13) : ascii(10)
{
declare l, d1, d2
l  = "N 90 20 30"
d1 = get_latitude_degree(l)
d2 = get_longitude_degree(l)
flash_message("Latidude   ":l:" > ":d1:CRLF:
              "Longitude " :l:" > ":d2,true)
}

{*****************************************************************************}
GLOBAL ROUTINE get_latitude_degree ( VALUE latitude )
{
***   Convert Latitude to decimal degrees
***
***   Modified - none
***   Return   - decimal degrees, EMPTY if empty or illegal values
***
******************************************************************************}

    RETURN ( convert_lat_long ( latitude, "NS" ))

ENDROUTINE


{*****************************************************************************}
GLOBAL ROUTINE get_longitude_degree ( VALUE longitude )
{
***   Convert Longitude to decimal degrees
***
***   Modified - none
***   Return   - decimal degrees, EMPTY if empty or illegal values
***
******************************************************************************}

    RETURN ( convert_lat_long ( longitude, "EW" ))

ENDROUTINE


{*****************************************************************************}
GLOBAL ROUTINE get_easting_degree ( VALUE datum, VALUE zone, VALUE easting )
{
***   Convert GDA Easting to Latitude decimal degrees
***
***   Modified - none
***   Return   - Latitude decimal degrees, EMPTY if empty or illegal values
***
******************************************************************************}

    IF ( datum        = EMPTY ) OR ( zone        = EMPTY ) OR ( easting        = EMPTY ) OR
       ( STRIP(datum) = ""    ) OR ( STRIP(zone) = ""    ) OR ( STRIP(easting) = ""    ) OR
       ( datum        = 0     ) OR ( zone        = 0     ) OR ( easting        = 0     ) THEN

        RETURN ( "" )

    ENDIF

    RETURN ( "" )
    { RETURN ( convert_easting ( easting ))
    }

ENDROUTINE


{*****************************************************************************}
GLOBAL ROUTINE get_northing_degree ( VALUE datum, VALUE zone, VALUE northing )
{
***   Convert GDA Northing to Longitude decimal degrees
***
***   Modified - none
***   Return   - Longitude decimal degrees, EMPTY if empty or illegal values
***
******************************************************************************}

    IF ( datum        = EMPTY ) OR ( zone        = EMPTY ) OR ( northing        = EMPTY ) OR
       ( STRIP(datum) = ""    ) OR ( STRIP(zone) = ""    ) OR ( STRIP(northing) = ""    ) OR
       ( datum        = 0     ) OR ( zone        = 0     ) OR ( northing        = 0     ) THEN

        RETURN ( "" )

    ENDIF

    RETURN ( "" )
    { RETURN ( convert_northing ( northing ))
    }

ENDROUTINE


{*****************************************************************************}
ROUTINE convert_lat_long ( VALUE lat_long_string,   { latitude / longitude string }
                           VALUE cardinal_points )  { legal cardinal points       }
{
******************************************************************************}

    DECLARE lat_long, cardinal, degree, minute, second, decimal_degree

    cardinal_points = TOUPPER ( cardinal_points )
    lat_long        = STRIP ( lat_long_string )
    cardinal = EMPTY

    IF ( lat_long_string = EMPTY ) OR
       ( lat_long        = ""    ) THEN

        decimal_degree = ""

    ELSE
        get_dms ( lat_long, cardinal_points, cardinal, degree, minute, second )

        IF ( degree = EMPTY ) OR
           ( minute = EMPTY ) OR ( minute > 59 ) OR
           ( second = EMPTY ) OR ( second > 59 ) OR
           ((( cardinal = "E" ) OR ( cardinal = "W" )) AND ( degree > 180 )) OR
           ((( cardinal = "N" ) OR ( cardinal = "S" )) AND ( degree > 90  )) THEN

            decimal_degree = ""

        ELSE

            decimal_degree = degree + minute / 60 + second / 3600

            IF ( cardinal = "W" ) OR ( cardinal = "S" ) THEN

                decimal_degree = decimal_degree * (-1)

            ENDIF

            IF ((( cardinal = "E" ) OR ( cardinal = "W" )) AND ( ABS ( decimal_degree ) > 180 )) OR
               ((( cardinal = "N" ) OR ( cardinal = "S" )) AND ( ABS ( decimal_degree ) > 90  )) THEN

                decimal_degree = ""

            ELSE

                decimal_degree = STRIP ( NUMBER_TO_TEXT ( decimal_degree, "9999.99999" ))

            ENDIF
        ENDIF
    ENDIF

    RETURN ( decimal_degree )

ENDROUTINE


{*****************************************************************************}
ROUTINE get_dms ( VALUE lat_long,
                  VALUE cardinal_points,
                        cardinal,
                        degree,
                        minute,
                        second )
{
***   Get and check the degree / minute / second component of string s
***
***   Notes    : Format of Latitude & Longitude string = S-123-12-12
***              . delimitors may be any of < > <.> <-> </>
***              . delimitor is not required after cardinal point
***
***   Modified - degree, minute, second
***   Return   -
***
******************************************************************************}

    CONSTANT delim = " ./-"

    DECLARE dms, c, i, j, p, delim_found

    ARRAY dms ARRAYSIZE (3)

    dms [1] = ""
    dms [2] = ""
    dms [3] = ""
    i = 1
    p = 1

    WHILE i <= LENGTH ( lat_long )
        c = SUBSTRING ( lat_long, i, 1 )
        j = 1
        delim_found = FALSE

        {
            1st char is the cardinal point
        }
        IF i = 1 THEN
            cardinal = get_cardinal (lat_long, cardinal_points )

        {
            2nd char may be a delimiter or the first degree numeral
            Build degree, minute & second strings in array dms
            Increment pointer p only if delimiter is not 2nd char
            Only decode dms if cardinal point is legal
        }
        ELSEIF cardinal <> EMPTY THEN
            WHILE ( j < 5 ) AND ( NOT delim_found )
                IF ( c = SUBSTRING ( delim, j, 1 )) THEN
                    delim_found = TRUE
                    IF i > 2 THEN
                        p = p + 1
                    ENDIF
                ENDIF
                j = j + 1
            ENDWHILE

            IF NOT delim_found THEN
                dms [p] = dms [p] : c
            ENDIF
        ENDIF

        i = i + 1

    ENDWHILE

    degree = check_dms ( cardinal, dms [1] )
    minute = check_dms ( cardinal, dms [2] )
    second = check_dms ( cardinal, dms [3] )

ENDROUTINE


{*****************************************************************************}
ROUTINE get_cardinal ( VALUE lat_long,      { latitude / longitude string  }
                       VALUE card_points )  { allowed cardinal point chars }
{
***   Get and check the cardinal point character in string s
***
***
***   Modified - none
***   Return   - cardinal point N S E W, EMPTY if illegal character
***
******************************************************************************}

    DECLARE c, i, ok

    c  = TOUPPER ( LEFTSTRING ( lat_long, 1 ))
    ok = FALSE
    i  = 1

    WHILE ( i <= LENGTH ( card_points )) AND ( NOT ok )
        IF ( c = SUBSTRING ( card_points, i, 1 )) THEN
            ok = TRUE
        ENDIF
        i = i + 1
    ENDWHILE

    IF NOT ok THEN
        c = EMPTY
    ENDIF

    RETURN ( c )

ENDROUTINE


{*****************************************************************************}
ROUTINE check_dms ( VALUE cardinal,         { cardinal point value   }
                    VALUE dms )             { dms value for checking }
{
***   Check the dms value
***
***
***   Modified - none
***   Return   - dms value, EMPTY if illegal character
***
******************************************************************************}

    IF ( cardinal = EMPTY   ) OR
       ( NOT NUMTEXT ( dms )) OR
       ( dms < 0            ) THEN
        dms = EMPTY
    ENDIF

    RETURN ( dms )

ENDROUTINE
