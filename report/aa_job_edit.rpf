{******************************************************************************}
{                                                                              }
{     MODULE  : AA_JOB_EDIT (Based on AGWA_JOB_EDIT)                           }
{     AUTHOR  : Brett Williams                                                 }
{     DATE	: 22/03/2000                                                   }
{     DESCRIPTION : Manage samples/tests for a job using a grid                }
{                                                                              }
{ AGWA Version     : 3.0                                                            }
{ Date implemented : 2/8/01                                                    }
{                                                                             }                   
{******************************************************************************}
{                                                                              }
{ Modification History	:                                                      }
{                                                                              } 
{ By	Date		Version	Modifications                                  }
{ RAPL  18May2000       1.1 Added changes to allow for the addition tests for  }
{                            job subsamples.                                   }                                         
{ RAPL  1JUN2000        1.2 Code to handle access by Result entry by inst.     }
{ RAPL  23JUN2000       1.3 Code to allow start of MSTE without analyses being }
{                           required.                                          }
{								      	       }
{ PVM   16MAR2001       1.4 Added functionality for Export Rack Jobs	       }
{									       }
{ PVM   20JUN2001       1.5 Modified routine 'agwa_new_job' to call routine    }
{                           directly rather than use MENUPROC 12               }
{ JDB   31AUG2001       1.6 Added look up of test sched id for matching to     }
{                           test sched entries                                 }
{ JDB   07Aug2002       1.7 Change libray to Aust Agencies series              }
{ JDB   12Sep2002       1.8 Exclude authorised samples, as tests cannot be     }
{                           added to authorised samples. NEWTEST will fail.    }
{ PVM   29APR2003       1.9 Exclude X,S,I,R samples									       
{									       }
{                                          � Thermo LabSystems Australia       }
{******************************************************************************}
{******************************************************************************}
{******************************************************************************
* Modification History  : 
*
* Version By     Date      Modifications
* 
*  M01    PVM   09APR2003  Check if any samples in Job.
*  M02    PVM   12MAR2004  Added ability to have a Display only MSTE          
*  M03    PVM   01JUN2004  Allow all sample status to be displayed
*
*******************************************************************************}



{******************************************************************************}
{******************************************************************************}

JOIN STANDARD_LIBRARY STD_BROWSE
JOIN STANDARD_LIBRARY STD_PROMPT
JOIN STANDARD_LIBRARY STD_ARRAY

JOIN STANDARD_LIBRARY std_utils
JOIN STANDARD_LIBRARY std_class
JOIN STANDARD_LIBRARY std_array
JOIN STANDARD_LIBRARY std_user_global

JOIN LIBRARY $LIB_UTILS
JOIN LIBRARY $SAMP_JOB			{PVM 16MAR2001}
JOIN LIBRARY AA_MSTE			{JDB 07Aug2002}

SET NAME "DISPLAY/"
SET FORMAT "99999.99"
SET NOTPROTECTED
ENABLE WINDOWS
SET COMPILE_OPTION DECLARE
SET SAMPLESTATUS "VCU"
SET JOBSTATUS "VCU"	

    DECLARE job_prompt_pos, rack_prompt_pos

    mste_modify ( 1 )

{******************************************************************************}

ROUTINE mste_modify ( VALUE option )

{
*    Modify Multi Sample test Editor
********************************************************************************}

{ Main Program }

	declare form, job_id, mode

        mode   = "MODIFY"

	job_id =""
        job_id = job_id

	set_aa_globals()

	create_msteditor (form)

	if (select_samples_by_job(form,  EMPTY, EMPTY, FALSE)) then

		aa_job_form_show(form, mode)

        ELSE

            FLASH_MESSAGE ( "There must be a minimum of ONE sample logged in for the Job to use this feature" : CHR (13) :
                            "Add a sample to the Job, then select this feature.", 1 )

	endif

	remove_aa_globals()

	EXIT

{ End Main Program }

ENDROUTINE

{******************************************************************************}

ROUTINE mste_display ( VALUE option )

{
*    Display Multi Sample test Editor
********************************************************************************}

      {M02}
	declare form, job_id{, job_prompt_pos, rack_prompt_pos}, mode

        mode   = "DISPLAY"

	job_id =""
        job_id = job_id

	set_aa_globals()

	create_msteditor (form)

	if (select_samples_by_job(form,  EMPTY, EMPTY, FALSE)) then

		aa_job_form_show(form, mode)

        ELSE

            FLASH_MESSAGE ( "There must be a minimum of ONE sample logged in for the Job to use this feature" : CHR (13) :
                            "Add a sample to the Job, then select this feature.", 1 )

	endif

	remove_aa_globals()

	EXIT

{ End Main Program }

ENDROUTINE

{******************************************************************************}

GLOBAL ROUTINE mste_for_job(job_id)

	DECLARE form, mode

        mode = "MODIFY"

	set_aa_globals()

	create_msteditor (form)

	if (select_samples_by_job(form,  EMPTY, EMPTY, FALSE)) then

		aa_job_form_show(form, mode)

        ELSE

            FLASH_MESSAGE ( "There must be a minimum of ONE sample logged in for the Job to use this feature" : CHR (13) :
                            "Add a sample to the Job, then select this feature.", 1 )

	endif

	remove_aa_globals()

 
ENDROUTINE

{******************************************************************************}

GLOBAL ROUTINE mste_for_subsamples(job_id)
	DECLARE form, mode

        mode = "MODIFY"

	set_aa_globals()

	create_msteditor ( form )

	if (select_samples_by_job(form,  EMPTY, EMPTY, FALSE)) then

		aa_job_form_show ( form, mode )

        ELSE

            FLASH_MESSAGE ( "There must be a minimum of ONE sample logged in for the Job to use this feature" : CHR (13) :
                            "Add a sample to the Job, then select this feature.", 1 )

	endif

	remove_aa_globals()

ENDROUTINE

{******************************************************************************}

ROUTINE create_msteditor (form)

	if !(CLASS_DEFINED("AA_MSTE")) then

		aa_define_mste_classes()

	endif

	CREATE OBJECT "AA_MSTE" , form

ENDROUTINE


{******************************************************************************}

ROUTINE select_samples_by_job(form, VALUE name_of_job, VALUE rack_num, VALUE subsamples)
{
	This routine prompts for a job and selects the samples from that job
	it returns true if successful false otherwise or if cancel is pushed
}

    DECLARE samples_logged

    samples_logged = FALSE

	if in_debug_mode() then

		name_of_job = "DEFAULT   .        4"

	else

		{ prompt for a job name if none passed in }

		if name_of_job = EMPTY then

			PROMPT_IN_WINDOW (	"job_header"		,	 
						"Job Id"		,
						"Select a Job"		, 
						""			, 
						name_of_job		)

		endif

	endif

	if name_of_job <> "" then

		form.job_id = name_of_job
		{ RAPL 18 May 2000 - Selection of samples by subsamples of job or standard samples of job.}
		IF subsamples THEN
			form.current_sample.id_numeric = SELECT sample.id_numeric
				WHERE (job_name = name_of_job
				  { AND subsample_desc <> ""  NRM Clause removed}
				  AND status <> "A") {JDB 12Sep2002 Exclude authorised samples }
		{PVM 16MAR2001 Added for rack samples}
		ELSEIF ( rack_num <> EMPTY ) AND ( rack_num > 0 ) THEN
			form.current_sample.id_numeric = SELECT sample.id_numeric
				WHERE (job_name = name_of_job
				  {AND subsample_desc = ""
				  AND rack_number = rack_num   NRM Clause removed}
				  AND status <> "A") {JDB 12Sep2002 Exclude authorised samples }
		{end}
		ELSE
			form.current_sample.id_numeric = SELECT sample.id_numeric
				                           WHERE (job_name = name_of_job )

								   {M03 Allow all sample status to be displayed as other code prevents mods}
				                           {AND ( status <> "A") {JDB 12Sep2002 Exclude authorised samples }
                                                           {AND ( status <> "X" ) {PVM 29APR2003 Exclude cancelled samples}
                                                           {AND ( status <> "I" ) {PVM 29APR2003 Exclude inspected samples}
                                                           {AND ( status <> "R" ) {PVM 29APR2003 Exclude rejected samples}
                                                           {AND ( status <> "S" ) {PVM 29APR2003 Exclude suspended samples}
				                           {AND subsample_desc = "")  NSWAg Clause removed }
                                                           ORDER ON id_text
		ENDIF

		WHILE ( form.current_sample.id_numeric <> EMPTY ) DO
                        samples_logged = TRUE

			form.samples.add(form.current_sample)
			form.current_sample = form.current_sample.reset()
			NEXT sample
			form.current_sample.id_numeric = SELECT sample.id_numeric
		ENDWHILE

	ENDIF

    RETURN ( samples_logged )

ENDROUTINE

{******************************************************************************}

ROUTINE select_analyses_by_schedule(form)

    IF in_debug_mode() then
	DECLARE identifier, unique_analysis
	identifier = "BW_MSTE"
	unique_analysis = TRUE
	unique_analysis = unique_analysis

    ELSE

	PROMPT_IN_WINDOW (	"test_sched_header"	,	 
				"Test Schedule"		,
				"Select a Test Schedule", 
				""			, 
				identifier		)
    ENDIF

ENDROUTINE

{******************************************************************************}

ROUTINE set_aa_globals

	declare cluster_name, 
		global_name1,
		global_name2,
		global_name3,
		mste_mode,
		debug_mode,
		job_id

	cluster_name = "AA"
	global_name1 = "use_multisample"
	global_name2 = "debug"
	global_name3 = "name_of_job"

{Debug: Set to TRUE to enter debug mode }
	debug_mode = FALSE
	mste_mode = TRUE
	job_id = EMPTY


	IF NOT ( user_cluster_exists ( cluster_name ) ) THEN

		ADD_GLOBAL_CLUSTER( cluster_name )

		ADD_USER_GLOBAL( cluster_name, global_name1 )
		USER_GLOBAL_WRITE ( cluster_name, global_name1, mste_mode )

		ADD_USER_GLOBAL( cluster_name, global_name2 )
		USER_GLOBAL_WRITE ( cluster_name, global_name2, debug_mode )

		ADD_USER_GLOBAL( cluster_name, global_name3 )
		USER_GLOBAL_WRITE ( cluster_name, global_name3, job_id )

	else

		USER_GLOBAL_WRITE ( cluster_name, global_name1, mste_mode )
		USER_GLOBAL_WRITE ( cluster_name, global_name2, debug_mode )
		USER_GLOBAL_WRITE ( cluster_name, global_name3, job_id )

	endif

ENDROUTINE

{******************************************************************************}

ROUTINE remove_aa_globals

	declare cluster_name

	cluster_name = "AA"

	IF ( user_cluster_exists ( cluster_name ) ) THEN

		DELETE_GLOBAL_CLUSTER ( cluster_name )

	ENDIF

ENDROUTINE

{******************************************************************************}

ROUTINE in_debug_mode

{  This function returns true if the AA global variable "debug" is TRUE }

	DECLARE cluster_name, global_name

	cluster_name = "AA"
	global_name = "debug"

	IF ( user_cluster_exists ( cluster_name ) ) THEN

		IF ( read_user_global ( cluster_name, global_name ) ) THEN

			RETURN ( TRUE )
		ELSE
			RETURN ( FALSE )

		ENDIF
	ELSE

		RETURN ( FALSE ) {JB from true to false}

	ENDIF


ENDROUTINE

{******************************************************************************}

ROUTINE get_job_name

{  This function returns the job name if set, otherwise it returns EMPTY }

	DECLARE cluster_name, global_name

	cluster_name = "AA"
	global_name = "name_of_job"

	IF ( user_cluster_exists ( cluster_name ) ) THEN

		RETURN ( read_user_global ( cluster_name, global_name ) )

	ELSE

		RETURN ( EMPTY )

	ENDIF


ENDROUTINE

{******************************************************************************}

ROUTINE get_use_mste

{  This function returns the mste mode if set, otherwise it returns EMPTY }

	DECLARE cluster_name, global_name

	cluster_name = "AA"
	global_name  = "use_multisample"

	IF ( user_cluster_exists ( cluster_name ) ) THEN

		RETURN ( read_user_global ( cluster_name, global_name ) )

	ELSE

		RETURN ( EMPTY )

	ENDIF


ENDROUTINE

{******************************************************************************}

ROUTINE get_rack_number

{  This function returns the rack_number if set, otherwise it returns EMPTY }	{PVM 16MAR2001}

	DECLARE cluster_name, global_name

	cluster_name = "AA_RACK"
	global_name = "rack_no"

	IF ( user_cluster_exists ( cluster_name ) ) THEN

		RETURN ( read_user_global ( cluster_name, global_name ) )

	ELSE

		RETURN ( EMPTY )

	ENDIF


ENDROUTINE


{******************************************************************************}

ROUTINE cancel_edit_rack_job ( self)

{Cancel for rack jobs}		{PVM 16MAR2001}

	IF ( LENGTH ( self . parent_prompt . prompt_objects [ job_prompt_pos ] . value ) = 0 ) THEN
		self . parent_prompt . end_prompt ()
		EXIT
	ELSE
		self . parent_prompt . prompt_objects [ job_prompt_pos ] . set_text( "" )
		self . parent_prompt . prompt_objects [ job_prompt_pos ] . repaste ()
	ENDIF

ENDROUTINE

{******************************************************************************}

ROUTINE rack_browse ( self )

{Get valid rack numbers for job}		{PVM 16MAR2001}

	DECLARE rack_array, rack_num, job_id, count, current_rack_num
	ARRAY rack_array ARRAY_SIZE (0) 

	job_id = self . parent_prompt . prompt_objects [ job_prompt_pos ] . value

	rack_num = SELECT sample . rack_number
			WHERE job_name = job_id
			ORDER ON rack_number

	count = 0
	current_rack_num = -1
	WHILE rack_num <> EMPTY 
		IF rack_num <> current_rack_num THEN
			count = count + 1
			rack_array [ count ] = RIGHTSTRING ( "0000" : STRIP ( rack_num ), 4 )
			current_rack_num = rack_num
		ENDIF
		NEXT sample
		rack_num = SELECT sample . rack_number
	ENDWHILE

	BROWSE_ON_ARRAY ( 80, self . text, rack_array )
	self . repaste ()

ENDROUTINE

{******************************************************************************}

ROUTINE get_job_rack (job_id, rack_num)

{  This function returns the job_id and rack_number }		{PVM 16MAR2001}

	DECLARE job_form, job_form_string, rack_form_string, 
		prompt_object, ok_button, button_cancel,
		max_len

	job_id   = EMPTY
	rack_num = EMPTY

	CREATE OBJECT "STD_FORM" , job_form

	job_form . height = 6
	job_form . width  = 40
	job_form . row    = 11
	job_form . column = ROUND ( ( GLOBAL ( "SCREEN_WIDTH" ) -
					job_form . width  ) / 2 )
	job_form . border = TRUE
	job_form . header = "Export Job"
	job_form . proportional = TRUE
	job_form . return_behaviour = FORM_RETURN_STAY
	job_form . button_style = FORM_BUTTON_NONE

	job_form_string  = "Select Job : "
	rack_form_string = "Enter Rack Number : "

	IF STRINGLENGTH ( job_form_string ) > STRINGLENGTH ( rack_form_string ) THEN
		max_len = STRINGLENGTH ( job_form_string )
	ELSE
		max_len = STRINGLENGTH ( rack_form_string )
	ENDIF

	PROMPT OBJECT button_cancel
		CLASS "STD_PROMPT_BUTTON"
		ON LINE job_form . height - 1
		FROM 25 TO 35
		WITH ( caption = "CANCEL",
			mouse_click_routine = "cancel_edit_rack_job",
			VGL_LIBRARY = GLOBAL ( "CURRENT_LIBRARY" ) )

	job_form . add_prompt ( button_cancel )

	PROMPT OBJECT ok_button
		CLASS "STD_PROMPT_BUTTON"
		ON LINE job_form . height - 1
		FROM 7 TO 17
		WITH ( 	caption = "OK",
			send_lastkey = "DO" )

	job_form.add_prompt(ok_button)       		

	PROMPT OBJECT prompt_object						
		FORMAT TEXT
		ON LINE 1 FROM 2
		WITH ( bold   = TRUE                     	,
		       width  = max_len 			,
		       value  = job_form_string                  )

	job_form . add_display ( prompt_object )

        PROMPT OBJECT prompt_object								
                BROWSE ON JOB_HEADER
                THEN select
                ON LINE 1 FROM ( 3 + max_len ) 
		WITH ( WIDTH = 15	)

	job_prompt_pos = job_form . add_prompt ( prompt_object )

	PROMPT OBJECT prompt_object						
		FORMAT TEXT
		ON LINE 3 FROM 2
		WITH ( bold   = TRUE                             ,
		       width  = max_len				 ,
		       value  = rack_form_string                 )

	job_form . add_display ( prompt_object )
	
        	PROMPT OBJECT prompt_object								
                ON LINE 3 FROM ( 3 + max_len )
	       	WITH ( 	browse_routine = "rack_browse",
			width = 15			 )

	rack_prompt_pos = job_form . add_prompt ( prompt_object )

	job_form . add_frame ( "" , 1 , 1, 3 , job_form . width )
 
	job_form . start_prompt ()

        { Now everythings setup do some prompting }
	job_form . wait_prompt ()
	job_form . end_prompt ()

	job_id   = job_form . prompt_objects [ job_prompt_pos ] . value
	rack_num = job_form . prompt_objects [ rack_prompt_pos ] . value


ENDROUTINE


{******************************************************************************}

ROUTINE aa_new_job ( VALUE option )

        DECLARE mode, form

        mode = "MODIFY"

	set_aa_globals()

        CALL_ROUTINE "login_sample_new_job" USING option
             IN LIBRARY "$samp_job"

	create_msteditor(form)

	if ( select_samples_by_job(form, get_job_name(), EMPTY, FALSE ) ) then

		aa_job_form_show(form, mode)

	endif

	remove_aa_globals()

ENDROUTINE

{******************************************************************************}

ROUTINE aa_new_rack_job(self)

{Login samples for new Rack Job}			{PVM 16MAR2001}

{Not required for NRM

	set_aa_globals()

	login_sample_new_rack_job ( "R" )

	remove_aa_globals()
	remove_aa_rack_globals () 
end }

ENDROUTINE

{******************************************************************************}

ROUTINE aa_old_job(self)

	DECLARE form, mode

        mode   = "MODIFY"

	set_aa_globals()

	create_msteditor (form)

	if (select_samples_by_job(form, EMPTY, EMPTY, FALSE)) then

		aa_job_form_show(form, mode)

	endif

	remove_aa_globals()

ENDROUTINE

{******************************************************************************}

ROUTINE login_more_rack ( option )

{Login more samples for existing Export Job or Samples to an existing rack}		{PVM 16MAR2001}

{Not required at NRM
	set_aa_globals()

	login_more_rack_samples ( option )

	remove_aa_globals()

end }

ENDROUTINE

{******************************************************************************}

ROUTINE aa_old_rack_job( VALUE option )

{Edit an existing Export Job}		{PVM 16MAR2001}

	DECLARE form, job_id, rack_num, mode

        mode   = "MODIFY"

	set_aa_globals()

	create_msteditor (form)
	form . is_rack = TRUE

	get_job_rack (job_id, rack_num)

	IF (job_id <> EMPTY) AND (rack_num <> EMPTY) THEN
		if (select_samples_by_job(form, job_id, rack_num, FALSE)) then

			aa_job_form_show(form, mode)

		ENDIF
	ENDIF

	remove_aa_globals()

ENDROUTINE

{******************************************************************************}

ROUTINE aa_job_form_show(form, mode)

	load_analyses_col(form)
	form.show( mode )
	
ENDROUTINE

{******************************************************************************}

ROUTINE load_analyses_col(form)

{ This routine loads the analyses collection for all of the selected samples }
{ Only unique analyses loaded }

	DECLARE counter, anal_id, test_sched, inst_id

	form.analyses.add_index("analysis_id")

	counter = form.samples.size()

	WHILE counter > 0 

		form.samples.set_by_number(counter)

		anal_id = SELECT samp_test_view.analysis
			WHERE id_numeric = form.samples.current.id_numeric

		{ JDB 31AUG2001 Added lookup for test shedule in sample.}
		test_sched = SELECT sample.test_schedule
			WHERE id_numeric = form.samples.current.id_numeric

		while anal_id <> EMPTY

			inst_id = SELECT test_sched_entry.instrument_id
				WHERE (analysis_id = anal_id)
				  AND (identity = test_sched)   {JDB 31AUG2001 Add testsched criteria}

			if inst_id = EMPTY then
				inst_id = ""
			endif

			if (form.analyses.find_index("analysis_id", anal_id) > 0) then

				form.analyses.set_by_index("analysis_id", anal_id)

				if NOT (form.analyses.current.analysis_id = anal_id) then
					{ get new analysis object }
					form.current_analysis = form.current_analysis.reset()
					{ load it }
					form.current_analysis.analysis_id = anal_id
					form.current_analysis.instrument_id = inst_id
					{ add it to analyses collection }	
					form.analyses.add(form.current_analysis)
				endif

			else

				{ get new analysis object }
				form.current_analysis = form.current_analysis.reset()
				{ load it }
				form.current_analysis.analysis_id = anal_id
				form.current_analysis.instrument_id = inst_id				
				{ add it to analyses collection }	
				form.analyses.add(form.current_analysis)

			endif
			NEXT samp_test_view
			anal_id = SELECT samp_test_view.analysis			

		endwhile
		counter = counter - 1

	ENDWHILE
	
ENDROUTINE

{******************************************************************************}
