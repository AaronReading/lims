::
::  #compile_all.bat
::  Compile all rpf files in directory
::  Note that this will nor=t order them, so there may be dependency errors logged
::
@echo off

for %%i in (*.rpf) do ..\..\..\11.2\exe\compiler -instance limt %%~ni
pause
