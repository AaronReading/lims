{******************************************************************************
*
* Module Name   : $SAMP_USER.RPF
*
* Purpose       : User defined action routines for use before the sample is
*                 logged in
*
* Document Ref. : SE/T/TVGL-WORK-REPORTS/1/3
*
* Specification :
*
* Portability   : Not Checked
*
* Re-entrant    :
*
******************************************************************************
* Modification History
*
* Ver     Date         By    Details
* -----   ----------   ----  -------------------------------------------------
* M1.01   APR2003      DFMC  Assign test schedules to new table
* M1.02   APR2003      JAB   Assign default sample location
* M1.03   29APR2003    PVM   Auto assign depths to sample.sample_type for Soils/Sediments
* M1.04   15MAY2003    PVM   Assign default job location. Remove autoassign of sample location
* M1.05   05SEP2003    PVM   Assign bill_to_client if not assigned
* M1.06   08AUG2005    PJW   Update job_header.sample_count with count of samples in job
* M1.07   18JUN2006    PJW   Update year_created with current year in both job_header and sample
* M1.08   20/10/2010   PJW   Update job template_id before new job created
*
******************************************************************************}

SET NOTPROTECTED

JOIN LIBRARY $LIB_MLPS
JOIN LIBRARY $LIB_TEMP
JOIN LIBRARY $LIB_UTILS

JOIN LIBRARY  nrm_const
{JOIN LIBRARY aa_mste         }         {**dfmc**}

CONSTANT STT_MENUNUMBER = 144


{******************************************************************************}
{   Routine called before the Job login screen appears
{******************************************************************************}

GLOBAL ROUTINE job_login_pre_prompt ( field_controls ,
                                      template_details )
{
    flash_message ( "JOB_LOGIN_PRE_PROMPT" , TRUE )
}
ENDROUTINE


{******************************************************************************}
{   Routine called when DO is pressed. Return TRUE to accept the job
{   FALSE to return to the prompt screen.
{******************************************************************************}

GLOBAL ROUTINE job_login_validation ( field_controls ,
                                      template_details )
{
    flash_message ( "JOB_LOGIN_VALIDATION" , TRUE )
}
RETURN ( TRUE )

ENDROUTINE


{******************************************************************************}
{   Routine called after the Job login screen is removed
{******************************************************************************}

GLOBAL ROUTINE job_login_post_prompt ( field_controls ,
                                       template_details )
{
    flash_message ( "JOB_LOGIN_POST_PROMPT" , TRUE )
}
    {M1.04 - Assign the current operators location to the job }
    DECLARE loc_id

    loc_id = SELECT personnel . location_id
        WHERE identity = OPERATOR

    ASSIGN job_header . location_id     = loc_id
    {end M1.04}

    {M1.05 - Assign bill_to_client if not assigned}
    inv_cust = SELECT job_header . bill_to_client

    IF ( inv_cust = EMPTY ) OR ( BLANK ( inv_cust ) ) THEN
        job_cust = SELECT job_header . customer_id
        ASSIGN job_header . bill_to_client = job_cust
    ELSE
        job_cust = inv_cust
    ENDIF

    acc_code = SELECT job_header . iqm_account
    iqm_acc  = SELECT iqm_accounts . identity
               WHERE  identity    = acc_code
               AND    customer_id = job_cust

    IF iqm_acc = EMPTY THEN
        ASSIGN job_header . iqm_account = ""
        ASSIGN job_header . iqm_pricing = ""
    ENDIF
    {end M1.05}

    {M1.07 - Update year_created with current year}
    DECLARE cur_year
    SET DATE FORMAT "YYYY"
    cur_year = STRIP ( TODAY )
    ASSIGN job_header . year_created = cur_year
    RESTORE DATE FORMAT
    {end M1.07}

    {M1.08 - Update job template_id before new job created}
    {        Template defined in nrm_const                }
    ASSIGN job_header . template_id = JOB_TEMPLATE_FINAL
    {end M1.08}

ENDROUTINE


{******************************************************************************}
{   Routine called before any samples are logged in for a job.
{******************************************************************************}

GLOBAL ROUTINE job_login_pre_sample ( VALUE job_identity  ,
                                      VALUE new_job_login )
{
    flash_message ( "JOB_LOGIN_PRE_SAMPLE  " : new_job_login , TRUE )
}

ENDROUTINE


{******************************************************************************}
{   Routine called after all samples are logged in for a job.
{******************************************************************************}

GLOBAL ROUTINE job_login_post_sample ( VALUE job_identity  ,
                                       VALUE new_job_login )
{
    flash_message ( "JOB_LOGIN_POST_SAMPLE  " : new_job_login , TRUE )
}

    { M1.06 - Update job_header . sample_count with count of samples in job }
    CALL_ROUTINE "update_job_sample_count" USING job_identity IN LIBRARY "nrm_lib" NEW TRANSACTION
    { end M1.06 }

ENDROUTINE


{******************************************************************************}
{   Routine called before the Job Modify screen appears
{******************************************************************************}

GLOBAL ROUTINE job_modify_pre_prompt (       field_controls   ,
                                             template_details ,
                                       VALUE is_display       )
{
    flash_message ( "JOB_MODIFY_PRE_PROMPT  " : is_display , TRUE )
}
ENDROUTINE


{******************************************************************************}
{   Routine called when DO is pressed. Return TRUE to accept the job
{   FALSE to return to the prompt screen.
{******************************************************************************}

GLOBAL ROUTINE job_modify_validation ( field_controls ,
                                       template_details )
{
    flash_message ( "JOB_MODIFY_VALIDATION  "  , TRUE )
}
RETURN ( TRUE )

ENDROUTINE


{******************************************************************************}
{   Routine called after the Job modify screen is removed
{******************************************************************************}

GLOBAL ROUTINE job_modify_post_prompt (       field_controls   ,
                                              template_details )
{
    flash_message ( "JOB_MODIFY_POST_PROMPT  " , TRUE )
}

    {M1.05 - Assign bill_to_client if not assigned}
    inv_cust = SELECT job_header . bill_to_client

    IF ( inv_cust = EMPTY ) OR ( BLANK ( inv_cust ) ) THEN
        job_cust = SELECT job_header . customer_id
        ASSIGN job_header . bill_to_client = job_cust
    ELSE
        job_cust = inv_cust
    ENDIF

    acc_code = SELECT job_header . iqm_account
    iqm_acc  = SELECT iqm_accounts . identity
               WHERE  identity    = acc_code
               AND    customer_id = job_cust

    IF iqm_acc = EMPTY THEN
        ASSIGN job_header . iqm_account = ""
        ASSIGN job_header . iqm_pricing = ""
    ENDIF
    {end M1.05}

ENDROUTINE


{******************************************************************************}
{   Called after selecting a new template to login the samples.
{******************************************************************************}

GLOBAL ROUTINE sample_start_login_template ( VALUE template_id  ,
                                             VALUE repeat_count )

{
    flash_message ( "SAMPLE_LOGIN_TEMPLATE  " : template_id : " " :
             repeat_count , TRUE )
}
ENDROUTINE


{******************************************************************************}
{   Called after finishing with a template .
{******************************************************************************}

GLOBAL ROUTINE sample_end_login_template ( VALUE template_id   ,
                                           VALUE session_count )

{
    flash_message ( "SAMPLE_LOGIN_TEMPLATE  " : template_id : " " :
             session_count , TRUE )
}
ENDROUTINE


{******************************************************************************}
{   Called before the sample login screen appears
{******************************************************************************}

GLOBAL ROUTINE sample_login_pre_prompt (       field_controls   ,
                                               template_details ,
                                         VALUE sample_count     )
{
    flash_message ( "SAMPLE_LOGIN_PRE_PROMPT  " : sample_count , TRUE )
}
ENDROUTINE


{******************************************************************************}
{   Routine called when DO is pressed. Return TRUE to accept the sample
{   FALSE to return to the prompt screen.
{******************************************************************************}

GLOBAL ROUTINE sample_login_validation (       field_controls   ,
                                               template_details )

{
    flash_message ( "SAMPLE_LOGIN_VALIDATION  " , TRUE )
}
RETURN ( TRUE )

ENDROUTINE


{******************************************************************************}
{   Called after the sample login screen is removed but before the
{   following fields are set :-
{
{    ID_NUMERIC
{    ID_TEXT
{    STATUS
{    JOB_NAME
{    TEMPLATE_NAME
{******************************************************************************}

GLOBAL ROUTINE sample_login_post_prompt (       field_controls   ,
                                                template_details ,
                                          VALUE sample_count     )
{
    flash_message ( "SAMPLE_LOGIN_POST_PROMPT  " : sample_count , TRUE )
}

    DECLARE bulk, up_depth, lo_depth, sample_name, cur_year

    {---------------------------------------------------------}
    { M1.03 - Assign sample depths to sample_name field for 'Soils' and 'Sediment' samples
    {---------------------------------------------------------}
    samp_type = SELECT sample . sample_type

    IF ( samp_type = "SOI" )OR ( samp_type = "SED" ) THEN

        bulk = SELECT sample . bulk

        IF ( bulk ) THEN
            bulk = "B "
        ELSE
            bulk = ""
        ENDIF

        up_depth    = SELECT sample . upper_depth
        lo_depth    = SELECT sample . lower_depth
        sample_name = bulk : format_real_value ( up_depth, 2 ) : "-" : format_real_value ( lo_depth, 2 )

        ASSIGN sample . sample_name = sample_name

    ENDIF
    {end M1.03}

    {---------------------------------------------------------}
    { M1.07 - Update year_created with current year
    {---------------------------------------------------------}
    SET DATE FORMAT "YYYY"
    cur_year = STRIP ( TODAY )
    ASSIGN sample . year_created = cur_year
    RESTORE DATE FORMAT
    {end M1.07}

ENDROUTINE


{******************************************************************************}
{   Called just before the sample is logged in. All fields are setup.
{******************************************************************************}

GLOBAL ROUTINE sample_login_pre_test_assignment (       field_controls   ,
                                                        template_details ,
                                                  VALUE sample_count     )
{
    flash_message ( "SAMPLE_LOGIN_PRE_TEST_ASSIGNMENT  " : sample_count , TRUE )
}
ENDROUTINE


{******************************************************************************}
{   Called after the sample is logged in and all tests added. NOTE in this
{   case the sample must be selected before use.
{******************************************************************************}

GLOBAL ROUTINE sample_login_post_test_assignment (      field_controls   ,
                                                        template_details ,
                                                  VALUE sample_id        ,
                                                  VALUE sample_count     )

    {**dfmc** 10/02/2003 - Assign a record into the sample_test_sched table
                           when a test schedule is assigne to the sample }

    DECLARE test_schedule, id_text, test_sched_id
    DECLARE job_id

    test_schedule = SELECT sample . test_schedule
                    WHERE  id_numeric = sample_id

    test_sched_id = sample_id : test_schedule

    IF ( test_schedule <> EMPTY ) AND ( NOT BLANK ( test_schedule ) ) THEN

        { assign_to_database (test_sched_id)            }
        {JAB 16Apr2003 Modified to Call routine in 'new transaction'}
        CALL_ROUTINE "assign_to_database" USING test_sched_id IN LIBRARY "aa_mste" NEW TRANSACTION

    ENDIF
    {**dfmc** end}

{
    flash_message ( "SAMPLE_LOGIN_POST_TEST_ASSIGNMENT  " : sample_id :
            " " : sample_count , TRUE )
}
ENDROUTINE


{******************************************************************************}
{   Routine called before the sample Modify screen appears
{******************************************************************************}

GLOBAL ROUTINE sample_modify_pre_prompt (       field_controls   ,
                                                template_details ,
                                          VALUE is_display       )
{
    flash_message ( "SAMPLE_MODIFY_PRE_PROMPT  " : is_display , TRUE )
}

ENDROUTINE


{******************************************************************************}
{   Routine called when DO is pressed. Return TRUE to accept the sample
{   FALSE to return to the prompt screen.
{******************************************************************************}

GLOBAL ROUTINE sample_modify_validation ( field_controls   ,
                                          template_details )
{
    flash_message ( "SAMPLE_MODIFY_VALIDATION  " , TRUE )
}
RETURN ( TRUE )

ENDROUTINE


{******************************************************************************}
{   Routine called after the Sample modify screen is removed
{******************************************************************************}

GLOBAL ROUTINE sample_modify_post_prompt (       field_controls   ,
                                                 template_details )
{
    flash_message ( "SAMPLE_MODIFY_POST_PROMPT  " , TRUE )
}

    DECLARE bulk, up_depth, lo_depth, sample_name

    {---------------------------------------------------------}
    { M1.03 - Assign sample depths to sample_name field for 'Soils' and 'Sediment' samples
    {---------------------------------------------------------}
    samp_type = SELECT sample . sample_type

    IF ( samp_type = "SOI" )OR ( samp_type = "SED" ) THEN


        bulk = SELECT sample . bulk

        IF ( bulk ) THEN
            bulk = "B "
        ELSE
            bulk = ""
        ENDIF

        up_depth    = SELECT sample . upper_depth
        lo_depth    = SELECT sample . lower_depth
        sample_name = bulk : format_real_value ( up_depth, 2 ) : "-" : format_real_value ( lo_depth, 2 )

        ASSIGN sample . sample_name = sample_name

    ENDIF
    {end M1.03}

ENDROUTINE


{******************************************************************************}

ROUTINE update_test_schedule ( VALUE is_modify ,
                               VALUE action_type)

ENDROUTINE


{******************************************************************************}

ROUTINE update_product_level ( VALUE is_modify ,
                               VALUE action_type )

DECLARE product_version

    IF ( NOT blank ( select sample . PRODUCT_NAME ) ) AND
       ( SELECT sample . product_version = 0        ) THEN

        product_version = SELECT MAX mlp_header . product_version
                          WHERE  ( identity = ( SELECT sample . product_name )) AND
                                 ( approval_status = "A" )

        IF product_version <> EMPTY THEN

            ASSIGN sample . product_version = product_version

        ELSE

            ASSIGN sample . product_version = PAD ( " " , " " , 10 )

        ENDIF

    ENDIF

ENDROUTINE


{******************************************************************************}
{ Routine called after a sample has been logged in using the NOINPUT template  }
{ routine in $LIB_SAMP. Modifications should be made in consideration of the   }
{ calling report. current reports used are :                                   }
{        $LIB_STAN - for standard creation and                                 }
{        $RESTXT_V2  - for ad-hoc sample creation.  IPF 5-DEC-1991             }
{ NOTE. The update routine is performed by the original calling routine        }
{ NO updates on the sample table should be peformed in this routine.           }
{******************************************************************************}

GLOBAL ROUTINE user_create_sample_noinput ( VALUE calling_report )

    IF calling_report = "$LIB_STAN" THEN

{ fields not required for standards,
  these may be modified without effecting any functionality }

       assign sample.preparation_id = " "
       assign sample.batch_name     = " "
       assign sample.sampling_point = " "
       assign sample.hazard         = " "
       assign sample.location_id    = " "
       assign sample.customer_id    = " "
       assign sample.project_id     = " "
       assign sample.priority       = 1

{ These fields are assigned in the report $LIB_STAN. the contents of these
  fields are used by other applications. changing their operation may cause
  problems. }

{***       assign sample.group_id     = SELECT standard.group_id }
       assign sample.sample_type  = SELECT standard.standard_type
       assign sample.sample_name  = SELECT standard.standard_name
       assign sample.product_name = SELECT standard.identity
       assign sample.description  = SELECT standard.description
       assign sample.standard     = TRUE
       assign sample.on_wks       = TRUE

       ELSEIF calling_report = "$RESTXT_V2" THEN

    ENDIF

ENDROUTINE


{******************************************************************************}

GLOBAL ROUTINE test_editor_fields (        test_editor_prompt_details ,
                                    VALUE  mode                       )

ENDROUTINE


{******************************************************************************}
{   Routine called when an analysis is added to a test list.
{
{   The new test will be the currently selected test row.
{
{   Test schedule_current is a boolean which indicates if the test
{   comes from a test schedule. The test_sched_entry row will be valid
{   if this flag is TRUE.
{
{   Position is an integer value indicating where the test is in the test
{   list.
{
{   Mode contains one of the following values :
{
{   "ASSIGN_TEST"         - test list is for assignment to the currently
{                           selected sample.
{   "INTERNAL_TEST_LIST"  - test list is the Internal Test List.
{   "USER_TEST_LIST"      - test list is the user defined test list.
{******************************************************************************}

GLOBAL ROUTINE test_add_analysis (       test_list             ,
                                   VALUE test_schedule_current ,
                                   VALUE position              ,
                                   VALUE mode                  )

{
    flash_message ( "Add Analysis" : "Test schedule " : test_schedule_current : " " :
            position : " " : Mode : " " : SELECT test . analysis, TRUE )
}
ENDROUTINE


{******************************************************************************}
{   Routine called before the test assignment screen is pasted. All tests
{   from the test schedule have already been added. The mode parameter contains one
{   of the following values.
{
{   "ASSIGN_TEST"         - test list is for assignment to the currently
{                           selected sample.
{   "INTERNAL_TEST_LIST"  - test list is the Internal Test List.
{   "USER_TEST_LIST"      - test list is the user defined test list.
{
{   Note - if the user presses EXIT from the test assignment screen then
{   this routine will be called again when the test assignment process resets the
{   test list back to the default.
{******************************************************************************}

GLOBAL ROUTINE test_pre_assignment (       test_list ,
                                     VALUE mode      )


{
    flash_message ( "Pre Assignment " : mode , TRUE )
}
ENDROUTINE


{******************************************************************************}
{   Routine called after the user leaves test assignment screen is left.
{******************************************************************************}

GLOBAL ROUTINE test_post_assignment (  test_list )

{
    flash_message ( "Post Assignment " , TRUE )
}
ENDROUTINE


{******************************************************************************}
{   Routine called before the test editor screen is pasted. All tests
{   for the sample have already been added.
{******************************************************************************}

GLOBAL ROUTINE test_pre_edit ( test_list )
{
    flash_message ( "Pre Edit " , TRUE )
}
ENDROUTINE


{******************************************************************************}
{   Routine called after the user presses DO from the test editor screen.
{******************************************************************************}

GLOBAL ROUTINE test_post_edit (  test_list )

{
    flash_message ( "Post Edit " , TRUE )
}

ENDROUTINE


{******************************************************************************}
{   Routine called when the user attempts to add a new analysis.
{
{   The mode parameter contains one of the following values.
{
{   "ASSIGN_TEST"         - test list is for assignment to the currently
{                           selected sample.
{   "INTERNAL_TEST_LIST"  - test list is the Internal Test List.
{   "EDIT_TEST"           - test list is for editing the currently
{                           selected sample.
{
{   The routine returns a boolean value - TRUE to accept analysis
{                                         FALSE to reject analysis.
{******************************************************************************}

GLOBAL ROUTINE test_user_add_analysis (       test_list     ,
                                        VALUE analysis_name ,
                                        VALUE position      ,
                                        VALUE mode          )


 {
    flash_message ( "User Add" : Position : " " : mode , TRUE )
 }
    RETURN ( TRUE )

ENDROUTINE


{******************************************************************************}
{   Routine called when the user attempts to delete an analysis.
{
{   The mode parameter contains one of the following values.
{
{   "ASSIGN_TEST"         - test list is for assignment to the currently
{                           selected sample.
{   "INTERNAL_TEST_LIST"  - test list is the Internal Test List.
{   "EDIT_TEST"           - test list is for editing the currently
{                           selected sample.
{
{   The routine returns a boolean value - TRUE to accept analysis
{                                         FALSE to reject analysis.
{******************************************************************************}

GLOBAL ROUTINE test_user_delete_analysis (       test_list     ,
                                           VALUE position      ,
                                           VALUE mode          )

{
    flash_message ( "User Delete" : Position : " " : mode , TRUE )
}
    RETURN ( TRUE )

ENDROUTINE


{******************************************************************************}
{   Called when moving onto the assign column. Indicates if the user
{   can change the assign code.
{******************************************************************************}

GLOBAL ROUTINE test_user_can_change_assign (       test_list     ,
                                             VALUE position      ,
                                             VALUE mode          )


{
    flash_message ( "Can Change" : Position : " " : mode , TRUE )
}

    RETURN ( TRUE )

ENDROUTINE


{******************************************************************************}

GLOBAL ROUTINE sample_details_menu ( menu_details )

ENDROUTINE


{******************************************************************************}

GLOBAL ROUTINE sample_login_menu (        menu_details ,
                                   VALUE sample_id    )

ENDROUTINE


{******************************************************************************}

GLOBAL ROUTINE sample_modify_menu (       menu_details ,
                                    VALUE sample_id    )

    JOIN STANDARD_LIBRARY std_array

    DECLARE next_option

    CREATE OBJECT "STD_SAMP_JOB_OPTION", next_option

    next_option . menu_number = STT_MENUNUMBER
    next_option . title       = "STT"
    next_option . pass_sample = TRUE
    next_option . sample_id   = sample_id

    menu_details [ size_of_array ( menu_details ) + 1 ] = next_option

ENDROUTINE


{******************************************************************************}

GLOBAL ROUTINE test_editor_create_results ( test_list     ,
                                            order_number  )

{   Allow creation of result records for the currently selected test.
*   The test is also selected in the test list allowing user information
*   to be selected for the test. The order number parameter contain the
*   next order number to be used for the result. This parameter must be
*   correctly maintained.
*
*   Return TRUE if the there are results to be entered; False otherwise.
******************************************************************************}

    RETURN ( TRUE )

ENDROUTINE


{******************************************************************************}
{                                                                              }
{ Routine Name     : component_list_user_create                                }
{                                                                              }
{ Description      : Called when creating a result from a component list.      }
{                    This routine is called after result_user_create and can   }
{                    be used to fill in fields in the result row from the      }
{                    component list.                                           }
{                                                                              }
{ Parameters       :  Object containing the comp_list_entry row used to        }
{                     create the result.                                       }
{                                                                              }
{                     Currently selected result row is the new row             }
{                                                                              }
{ Return Value     :  None.                                                    }
{                                                                              }
{ Globals Modified :  None.                                                    }
{                                                                              }
{******************************************************************************}

ROUTINE component_list_user_create ( component_list_entry )

    ASSIGN result . spike_value =
                    SELECT versioned_c_l_entry . spike_value
                    IN OBJECT component_list_entry

    ASSIGN result . surrogate_mlp =
                    SELECT versioned_c_l_entry . surrogate_mlp
                    IN OBJECT component_list_entry

ENDROUTINE


{******************************************************************************}
{
{  Routine Name     : test_validation
{
{  Description      : Called after the user presses the DO key in the test
{                     assigment / editor screens. The test list is passed
{                     the parameter test_list
{
{                     The mode parameter contains one of the following values.
{
{                     "ASSIGN_TEST"         - test list is for assignment to
{                                             the currently selected sample.
{                     "INTERNAL_TEST_LIST"  - test list is the Internal Test
{                                             List.
{                     "EDIT_TEST"           - test list is for editing the currently
{                                             selected sample.
{
{                     If a given test row fails the validation then after a
{                     message is displayed the user can be placed on that cell
{                     by setting the cell_columnm,cell_row parameters.
{
{                     The Routine should return TRUE is the test list is valid
{                     else it should return FALSE.
{
{******************************************************************************}

GLOBAL ROUTINE test_validation (       test_list   ,
                                 VALUE mode        ,
                                       cell_column ,
                                       cell_row    )


{

    flash_message ( "Test Validation " : mode  , TRUE )
}

{ The following example code will check to see if all the new test records
  has been given a component list

    JOIN STANDARD_LIBRARY STD_LOGIN

    DECLARE count    ,
        analysis

    count = 1

    WHILE count <= test_editor_get_size ( test_list ) DO

        test_editor_make_current ( count , test_list )

        IF ( blank ( SELECT test . component_list       ))  AND
           ( NOT   ( test_editor_get_exists ( test_list ))) THEN

            analysis = SELECT test . analysis


            cell_column = 3
            cell_row    = count

            flash_message ( "Test " : strip ( analysis ) :
                    " does not have a" :
                    " component list" ,
                    TRUE              )

            RETURN ( FALSE )

        ENDIF

        count = count + 1

    ENDWHILE

}

    RETURN ( TRUE )

ENDROUTINE

{-----------------------------------------------------------------------------}

