{*****************************************************************************}
{                                                                             }
{ IIIIII  QQQQQQ    MM      MM  ********************************************* }
{   II   QQ    QQ   MMMM  MMMM  *                SAMPLEMANAGER              * }
{   II   QQ    QQ   MM MMMM MM  *        INVOICE AND QUOTATION MODULE       * }
{   II   QQ  q QQ   MM  MM  MM  *                                           * }
{   II   QQ   qq    MM      MM  *        � Thermo Electron 2005             * }
{ IIIIII  QQQQQ qq  WW      WW  ********************************************* }
{                                                                             }
{*****************************************************************************}

{*****************************************************************************
*                                                                             
*       MODULE  : IQM_QUOTE                                                    
*       VERSION : 4.0                                                         
*       AUTHOR  : Thermo Electron                                            
*       DATE    : 08 April 2005                                                 
{   DESCRIPTION : Quotation Management routines to support IQM 3.0            }
{                                                                             }
{*****************************************************************************}
{ Modification History                                                        
******************************************************************************
*
*****************************************************************************}

join standard_library STD_ARRAY
join standard_library STD_PROMPT

join library $LIB_UTILS

join library IQM_LIB
join library IQM_FORM
join library IQM_QUOED
join library IQM_INVDB

SET COMPILE_OPTION DECLARE
ENABLE WINDOWS
SET NAME "defer/"
SET FORMAT "9999999.99"
SET NOTPROTECTED


main_menu ()

{******************************************************************************}
{			GLOBAL ROUTINES				}
{******************************************************************************}
{ These routines can be called directly from VGSM Menu's using the standard }
{ LTE menu conventions.						}
{******************************************************************************}


GLOBAL ROUTINE Add_option (thequote)


	init_quote_object (thequote)

	thequote . mode = "Add"
	thequote . display_only = FALSE

	quoteform_setup(thequote)

	set_quote_header (thequote)

	thequote . mainform . startprompt()
	thequote . mainform . wait_prompt()
	thequote . mainform . endprompt()
		
	IF thequote . mainform . get_lastkey() <> "EXIT" THEN

		save_quotation (thequote)

	ENDIF

ENDROUTINE

{******************************************************************************}

GLOBAL ROUTINE Display_option (thequote)

	init_quote_object (thequote)

	thequote . mode = "Display"
	thequote . display_only = TRUE

	quoteform_setup(thequote)

	IF get_target_quote (thequote) THEN

		set_quote_header (thequote)

		thequote . mainform . startprompt()
		thequote . mainform . wait_prompt()
		thequote . mainform . endprompt()

	ENDIF

ENDROUTINE

{******************************************************************************}

GLOBAL ROUTINE Modify_option (thequote)

	init_quote_object (thequote)

	thequote . mode = "Modify"
	thequote . display_only = FALSE

	quoteform_setup(thequote)

	IF get_target_quote (thequote) THEN

		set_quote_header (thequote)

		thequote . mainform . startprompt()
		thequote . mainform . wait_prompt()
		thequote . mainform . endprompt()
	
	ELSE

		RETURN

	ENDIF
	
	IF thequote . mainform . get_lastkey() <> "EXIT" THEN

		save_quotation (thequote)

	ENDIF

ENDROUTINE

{******************************************************************************}

GLOBAL ROUTINE Copy_option (thequote)

	init_quote_object (thequote)

	thequote . mode = "Copy"
	thequote . display_only = FALSE

	quoteform_setup(thequote)

	IF get_target_quote (thequote) THEN

		set_quote_header (thequote)

		thequote . copied_from = thequote . entry

		get_quote_data (thequote)

		thequote . mode = "Add"

		thequote . mainform . startprompt()
		thequote . mainform . wait_prompt()
		thequote . mainform . endprompt()
	
	ELSE

		RETURN

	ENDIF
	
	IF thequote . mainform . get_lastkey() <> "EXIT" THEN

		save_quotation (thequote)

	ENDIF

ENDROUTINE

{******************************************************************************}

GLOBAL ROUTINE Issue_option (thequote)

	init_quote_object (thequote)

	thequote . mode = "Issue"
	thequote . display_only = TRUE

	quoteform_setup(thequote)

	IF get_target_quote (thequote) THEN

		START WRITE TRANSACTION "Issue"

		thequote . entry = SELECT iqm_quotes . identity for update
			WHERE identity = thequote . entry

		ASSIGN iqm_quotes . status = "I"
		ASSIGN iqm_quotes . date_issued = TODAY
		ASSIGN iqm_quotes . mod_by = OPERATOR
{ M02 PVM 21Nov2002 }
		ASSIGN iqm_quotes . remove_flag = FALSE
		ASSIGN iqm_quotes . issued_by = OPERATOR
{ END M02 }

		UPDATE iqm_quotes

		COMMIT

		flash_message ("Quotation ":strip(thequote.entry):" issued!",TRUE)

	ENDIF

ENDROUTINE

{******************************************************************************}

GLOBAL ROUTINE Accept_option (thequote)

	init_quote_object (thequote)

	thequote . mode = "Accept"
	thequote . display_only = TRUE

	quoteform_setup(thequote)

	IF get_target_quote (thequote) THEN

		START WRITE TRANSACTION "Accept"

		thequote . entry = SELECT iqm_quotes . identity for update
			WHERE identity = thequote . entry

		ASSIGN iqm_quotes . status = "A"
		ASSIGN iqm_quotes . contract_date = TODAY
		ASSIGN iqm_quotes . mod_by = OPERATOR
		ASSIGN iqm_quotes . remove_flag = FALSE

		UPDATE iqm_quotes

		COMMIT

		flash_message ("Quotation ":strip(thequote . entry):" accepted!", TRUE)

	ENDIF

ENDROUTINE

{******************************************************************************}

GLOBAL ROUTINE Expire_option (thequote)

	init_quote_object (thequote)

	thequote . mode = "Expire"
	thequote . display_only = TRUE

	quoteform_setup(thequote)

	IF get_target_quote (thequote) THEN

		START WRITE TRANSACTION "Expire"

		thequote . entry = SELECT iqm_quotes . identity for update
			WHERE identity = thequote . entry

		ASSIGN iqm_quotes . status = "X"
		ASSIGN iqm_quotes . expiry_date = TODAY
		ASSIGN iqm_quotes . mod_by = OPERATOR
{PVM 19FEB2003		ASSIGN iqm_quotes . remove_flag = TRUE}

		UPDATE iqm_quotes

		COMMIT

		flash_message ("Quotation ":strip(thequote . entry):" Cancelled!",TRUE)

	ENDIF

ENDROUTINE

{******************************************************************************}
{ M01 GJL 12Aug2002 - Added quote status "R" Reviewed }

GLOBAL ROUTINE Review_option (thequote)

	init_quote_object (thequote)

	thequote . mode = "Review"
	thequote . display_only = TRUE

	quoteform_setup(thequote)

	IF get_target_quote (thequote) THEN

		START WRITE TRANSACTION "Review"

		thequote . entry = SELECT iqm_quotes . identity for update
			WHERE identity = thequote . entry

		ASSIGN iqm_quotes . status = "R"
		ASSIGN iqm_quotes . mod_by = OPERATOR
		ASSIGN iqm_quotes . remove_flag = FALSE

		UPDATE iqm_quotes

		COMMIT

		flash_message ("Quotation ":strip(thequote . entry):" Reviewed!",TRUE)

	ENDIF

ENDROUTINE

{ END M01 GJL 12Aug2002 - Added quote status "R" Reviewed }

{******************************************************************************}

GLOBAL ROUTINE Print_option (option)

    CALL_ROUTINE "create_quote_report" USING option IN LIBRARY "nrm_quote_report"

ENDROUTINE

{******************************************************************************}

GLOBAL ROUTINE Email_option (option)

    CALL_ROUTINE "email_quote_report" USING option IN LIBRARY "nrm_quote_report"

ENDROUTINE

{******************************************************************************}

GLOBAL ROUTINE Detail_option (option) 


ENDROUTINE

{******************************************************************************}

GLOBAL ROUTINE Delete_option (thequote)


	IF GLOBAL("BASEAUTH") < 10 THEN

		flash_message ("You do not have authority to delete quotes", TRUE )

		RETURN

	ENDIF

	init_quote_object (thequote)

	thequote . mode = "Accept"
	thequote . display_only = TRUE

	quoteform_setup(thequote)

	IF get_target_quote (thequote) THEN

		thequote . entry = SELECT iqm_quotes . identity for update
			WHERE identity = thequote . entry


		IF confirm_with_text("Delete Quotation ":thequote.entry) THEN

			START WRITE TRANSACTION "retract quote"

			DELETE iqm_quotes

			COMMIT

			flash_message ("Quote Entry deleted" , TRUE)

		ELSE

			flash_message("Quote Entry NOT deleted", TRUE)

		ENDIF

	ENDIF

ENDROUTINE

{******************************************************************************}
{			ROUTINES					}
{******************************************************************************}

ROUTINE main_menu

declare menu_array , option

{ * * Direct Access Routines, calling all Global Routines * * }

	ARRAY menu_array

	menu_array [ 1, 1] = "*"
	menu_array [ 1, 2] = "Quotations/Contracts"

	menu_array [ 2, 1] = "A"
	menu_array [ 2, 2] = "Add Quote" 
	menu_array [ 2, 3] = "Add_option"

	menu_array [ 3, 1] = "M"
	menu_array [ 3, 2] = "Modify Quote" 
	menu_array [ 3, 3] = "Modify_option"

	menu_array [ 4, 1] = "D"
	menu_array [ 4, 2] = "Display Quote" 
	menu_array [ 4, 3] = "Display_option"

	menu_array [ 5, 1] = "C"
	menu_array [ 5, 2] = "Copy Quote" 
	menu_array [ 5, 3] = "Copy_option"

	menu_array [ 6, 1] = "I"
	menu_array [ 6, 2] = "Issue Quote" 
	menu_array [ 6, 3] = "Issue_option"

	menu_array [ 7, 1] = "t"
	menu_array [ 7, 2] = "Accept Quote" 
	menu_array [ 7, 3] = "Accept_option"

	menu_array [ 8, 1] = "x"
	menu_array [ 8, 2] = "Expire Quote" 
	menu_array [ 8, 3] = "Expire_option"

	menu_array [ 9, 1] = "P"
	menu_array [ 9, 2] = "Print Quote" 
	menu_array [ 9, 3] = "Print_option"

	menu_array [10, 1] = "r"
	menu_array [10, 2] = "Print Quote Detail" 
	menu_array [10, 3] = "Detail_option"

	menu_array [11, 1] = "r"
	menu_array [11, 2] = "Delete Quote" 
	menu_array [11, 3] = "Delete_option"
	

	REPEAT

		option = "" 

		CHOOSE option OUTOF menu_array AT 58,3

		if option = "" then EXIT endif

		CALL_ROUTINE option USING option

	UNTIL FALSE

ENDROUTINE

{***************************************************************************************************}


