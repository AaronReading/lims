{*****************************************************************************}
{                                                                             }
{ IIIIII  QQQQQQ    MM      MM  ********************************************* }
{   II   QQ    QQ   MMMM  MMMM  *                SAMPLEMANAGER              * }
{   II   QQ    QQ   MM MMMM MM  *        INVOICE AND QUOTATION MODULE       * }
{   II   QQ  q QQ   MM  MM  MM  *                                           * }
{   II   QQ   qq    MM      MM  *        � Thermo Electron 2005             * }
{ IIIIII  QQQQQ v3  WW      WW  ********************************************* }
{                                                                             }
{*****************************************************************************}

{*****************************************************************************
*                                                                             
*       MODULE  : IQM_CONFIG_CALLS   
*                                         
*       VERSION : 4.0                                                         
*
*       AUTHOR  : Thermo Electron                                             
*
*       DATE    : 08 April 2005                                               
*
*       DESCRIPTION : Configuration support routines to support IQM               
*
*       Purpose       : Library of configuration editor routines for reading and
*	          	  saving configuration item lists from the various editors.
*
* Document Ref. :
*
* Specification : Changed identity selection criteria to LIKE "IQM%"
*
* Portability   : Not Checked
*
*******************************************************************************
*
* Modification History                                                        
*
*******************************************************************************}

SET NOTPROTECTED

{* Library joins***************************************************************}

JOIN STANDARD_LIBRARY STD_ARRAY
JOIN STANDARD_LIBRARY STD_GLOBAL
JOIN STANDARD_LIBRARY STD_MESSAGE
JOIN STANDARD_LIBRARY STD_PROMPT
JOIN STANDARD_LIBRARY STD_BLOCK
JOIN STANDARD_LIBRARY STD_UTILS
JOIN STANDARD_LIBRARY STD_WINDOW
JOIN LIBRARY $LIB_UTILS

{* Allow windows to be used****************************************************}

ENABLE WINDOWS


{*******************************************************************************
*
* Lock out the user or the group.  This stops other users modifying the
* same config data.
*
*******************************************************************************}

ROUTINE lock_config ( VALUE context, VALUE context_id )

	DECLARE ok
	DECLARE field_given, data_given, field_given_msg
	DECLARE lock_id
	DECLARE msg_top_line, msg_lines, msg_bottom_line

	ARRAY msg_lines

	IF ( ( context = "user" ) OR
	     ( context = "any_user" ) ) THEN

		field_given = "OPERATOR"
		data_given  = STRIP ( context_id )
		field_given_msg = "user"

	ELSEIF ( context = "system" ) THEN

		field_given = "OPERATOR"
		data_given  = "SYSTEM"
		field_given_msg = "system"

	ELSEIF ( context = "group" ) THEN

		field_given = "GROUP_NAME"
		data_given  = STRIP ( context_id )
		field_given_msg = "group"
	ENDIF

	lock_id = SELECT config_item . identity
			FOR UPDATE
			WHERE 'field_given' = data_given

	IF ( lock_id <> LOCKED ) THEN
		ok = TRUE
	ELSE
		ok = FALSE

		msg_top_line    = GET_USER_MESSAGE( "CONFIG_EDITOR_LOCKED", 1 )

		message_fetch ( "CONFIG_EDITOR_LOCKED", msg_ptr )
		message_add_parameter ( msg_ptr , field_given_msg )
		message_add_parameter ( msg_ptr , data_given )
		message_add_parameter ( msg_ptr, field_given_msg )

		msg_lines[1]  = message_get_text ( msg_ptr, 2 )
		msg_lines[2]  = message_get_text ( msg_ptr, 3 )
		msg_lines[3]  = " "
		msg_lines[4]  = message_get_text ( msg_ptr, 4 )

		msg_bottom_line = GET_USER_MESSAGE( "CONFIG_EDITOR_LOCKED", 5 )

		flash_messages ( msg_top_line,
				 msg_bottom_line,
				 msg_lines,
				 TRUE )

	ENDIF

	RETURN ( ok )

ENDROUTINE
{*******************************************************************************
*
* Reads all values valid for current context ( user, group or system )
* and stores them in an array
*
*******************************************************************************}

GLOBAL ROUTINE read_profile ( VALUE context , context_id , config_array )

	DECLARE no_row , id_prompt , cnf_exists , continue , help_context
	DECLARE ok

	SET GLOBAL "LASTKEY" TO "CONTINUE"

	IF ( context = "any_user" ) THEN

		window_header = GET_USER_MESSAGE("CONFIG_EDITOR_GET_ID_HEADER",1)
		window_prompt = GET_USER_MESSAGE("CONFIG_EDITOR_GET_ID_PROMPT",1)
		window_browse = "personnel"
		help_context  = "CONFIG_GET_USER_ID"

	ELSEIF ( context = "user" ) THEN

		context_id = OPERATOR

	ELSEIF ( context = "group" ) THEN

		window_header = GET_USER_MESSAGE("CONFIG_EDITOR_GET_ID_HEADER",2 )
		window_prompt = GET_USER_MESSAGE("CONFIG_EDITOR_GET_ID_PROMPT",2)
		window_browse = "group_header"
		help_context  = "CONFIG_GET_GROUP_ID"

	ENDIF

	IF ( context = "any_user" ) OR ( context = "group" ) THEN

		ok = get_identity ( context_id,
			            window_prompt,
			            window_header,
			            window_browse,
			            help_context )
	ELSE
		ok = TRUE
	ENDIF

	IF ( ok ) THEN

		ok = lock_config ( context, context_id )

		IF ( ok ) THEN

			load_config_array ( )

		ENDIF
	ENDIF

	RETURN ( ok )

ENDROUTINE

{*******************************************************************************
*
* Reads all values for a single configuration item. Can be performed for all
* users or for all groups.
*
*******************************************************************************}

GLOBAL ROUTINE read_items ( VALUE context , global_id , name_block )

	DECLARE loop , item_prompt , global_owner , check_ok ,
		size_of_block, help_context, items_read

	IF context = "user" THEN
		window_header = GET_USER_MESSAGE("CONFIG_ITEM_GET_ID_HEADER", 1)
		help_context = "CONFIG_GET_ITEM_USER"
	ELSE
		window_header = GET_USER_MESSAGE("CONFIG_ITEM_GET_ID_HEADER", 2)
		help_context = "CONFIG_GET_ITEM_GROUP"
	ENDIF

	window_prompt = GET_USER_MESSAGE("CONFIG_ITEM_GET_ITEM_PROMPT" , 1 )

	finished   = FALSE
	items_read = FALSE

	IF ( get_identity ( global_id,
			    window_prompt,
		       	    window_header,
		            "config_header",
		            help_context ) ) THEN

		IF ( context = "user" ) THEN
			field_name = "operator"
		ELSEIF ( context = "group" ) THEN
			field_name = "group_name"
		ENDIF

		ARRAY select_array

		array_select_add ( select_array    ,
				   ARRAY_SELECT_EQ ,
				   "IDENTITY"      ,
				   global_id       )

		array_select_add ( select_array    ,
				   ARRAY_SELECT_NE ,
				   field_name      ,
				   "          "    )

		check_ok = block_row_initialise ( name_block ,
						  "config_item" )

		check_ok = block_row_select ( name_block ,
					      select_array , TRUE )

		size_of_block = block_row_size ( name_block )

		IF ( size_of_block = 0 ) AND ( NOT finished ) THEN

			flash_message ( STRIP ( global_id ) :
					" is not assigned to any " :
					context:"s" , TRUE )

		ELSE
			items_read = TRUE
			finished = TRUE
		ENDIF
	ENDIF

	RETURN ( items_read )

ENDROUTINE

{*******************************************************************************
*
* Reads all values for the current process which are user_modifiable
*
*******************************************************************************}

GLOBAL ROUTINE read_process_globals ( config_values )

	DECLARE global_id , marker

	marker = 0

	global_id = SELECT config_header . identity
			WHERE ( identity LIKE "IQM%" )
			AND user_modifiable = TRUE
			ORDER ON identity

	WHILE global_id <> EMPTY DO

		IF global_can_write ( STRIP ( global_id ) ) THEN

			marker = marker + 1

			config_values [ marker , 1 ] = "Current"

			config_values [ marker , 2 ] = STRIP ( global_id )

			config_values [ marker , 3 ] =
				GLOBAL ( config_values [ marker , 2 ] )

			config_values [ marker , 4 ] =
				SELECT config_header . description

			config_values [ marker , 5 ] =
				SELECT config_header . data_type

			config_values [ marker , 7 ] = "Current"

		ENDIF

		NEXT config_header

		global_id = SELECT config_header . identity

	ENDWHILE

ENDROUTINE

{*******************************************************************************
*
* Reads all values assigned to operator in the config_item table
*
*******************************************************************************}

GLOBAL ROUTINE read_operator_globals ( loop , config_array )

	DECLARE global_id , the_description

	global_id = SELECT config_item . identity
		WHERE ( identity LIKE "IQM%" )
		AND ( operator = context_id )

	WHILE global_id <> EMPTY DO

		loop = loop + 1

		config_array [ loop , 1 ] = "User" { Original context }

		config_array [ loop , 2 ] = global_id

		config_array [ loop , 3 ] = SELECT config_item . value

		the_description = SELECT config_header . description
			WHERE identity = global_id

		IF the_description = EMPTY THEN
			config_array [ loop , 4 ] = ""
			config_array [ loop , 5 ] = "TEXT"
			config_array [ loop , 6 ] = TRUE
			config_array [ loop , 8 ] = ""
			config_array [ loop , 9 ] = ""
		ELSE
			config_array [ loop , 4 ] = the_description
			config_array [ loop , 5 ] = SELECT config_header .
			                                        data_type
			config_array [ loop , 6 ] = SELECT config_header .
			                                        user_modifiable
			config_array [ loop , 8 ] = SELECT config_header .
			                                        library
			config_array [ loop , 9 ] = SELECT config_header .
			                                        routine
		ENDIF

		config_array [ loop , 7 ] = "User" { New context used for display }

		NEXT config_item

		global_id = SELECT config_item . identity

	ENDWHILE

ENDROUTINE

{*******************************************************************************
*
* Reads all values assigned to the group stored in the config_item table.
* Ignores values already read.
*
*******************************************************************************}

GLOBAL ROUTINE read_group_globals ( loop , config_array )

	DECLARE global_id , group_id , the_description

	IF context = "group" THEN
		group_id = context_id
	ELSEIF context = "any_user" THEN
		group_id = SELECT personnel . default_group
			WHERE identity = context_id
		IF BLANK ( group_id ) THEN
			RETURN
		ENDIF
	ELSE
		group_id = GLOBAL ( "DEFAULT_GROUP" )
		IF BLANK ( group_id ) THEN
			RETURN
		ENDIF
	ENDIF

	global_id = SELECT config_item . identity
		WHERE ( identity LIKE "IQM%" )
		AND ( group_name = group_id )

	list_size = size_of_array ( config_array )

	WHILE global_id <> EMPTY DO

		IF NOT in_list ( global_id , list_size , config_array ) THEN

			loop = loop + 1

			config_array [ loop , 1 ] = "Group"

			config_array [ loop , 2 ] = global_id

			config_array [ loop , 3 ] = SELECT config_item . value

			the_description = SELECT config_header . description
				WHERE identity = global_id


			IF the_description = EMPTY THEN
				config_array [ loop , 4 ] = ""
				config_array [ loop , 5 ] = "TEXT"
				config_array [ loop , 6 ] = TRUE
				config_array [ loop , 8 ] = ""
				config_array [ loop , 9 ] = ""
			ELSE
				config_array [ loop , 4 ] = the_description
				config_array [ loop , 5 ] =
				              SELECT config_header . data_type
				config_array [ loop , 6 ] =
				         SELECT config_header . user_modifiable
				config_array [ loop , 8 ] =
				         SELECT config_header . library
				config_array [ loop , 9 ] =
				              SELECT config_header . routine
			ENDIF

			config_array [ loop , 7 ] = "Group"

		ENDIF

		NEXT config_item

		global_id = SELECT config_item . identity

	ENDWHILE

ENDROUTINE

{*******************************************************************************
*
* Reads all values from the config_header table. Ignores all values read
* for the operator or group.
*
*******************************************************************************}

GLOBAL ROUTINE read_system_globals ( loop , config_array )

	{ Select from config_header if identity not already in list -
	  the list could be made up of user or group and user items   }

	DECLARE global_id , the_description

	global_id = SELECT config_header . identity
		WHERE identity LIKE "IQM%"

	list_size = size_of_array ( config_array )

	WHILE global_id <> EMPTY DO

		IF NOT in_list ( global_id , list_size , config_array ) THEN

			loop = loop + 1

			config_array [ loop , 1 ] = "System"

			config_array [ loop , 2 ] = global_id

			config_array [ loop , 3 ] = SELECT config_header . value

			the_description = SELECT config_header . description

			IF the_description = EMPTY THEN
				config_array [ loop , 4 ] = ""
				config_array [ loop , 5 ] = "TEXT"
				config_array [ loop , 6 ] = TRUE
				config_array [ loop , 8 ] = ""
				config_array [ loop , 9 ] = ""
			ELSE
				config_array [ loop , 4 ] = the_description
				config_array [ loop , 5 ] =
				              SELECT config_header . data_type
				config_array [ loop , 6 ] =
				         SELECT config_header . user_modifiable
				config_array [ loop , 8 ] =
				         SELECT config_header . library
				config_array [ loop , 9 ] =
				              SELECT config_header . routine
			ENDIF

			config_array [ loop , 7 ] = "System"

		ENDIF

		NEXT config_header

		global_id = SELECT config_header . identity

	ENDWHILE

ENDROUTINE

{*******************************************************************************
*
* Writes any globals modified in the config spreadsheet to the config_item and
* config_header tables.
*
*******************************************************************************}

GLOBAL ROUTINE save_config_grid ( VALUE context        ,
				  VALUE context_id     ,
				        globals_array  ,
					original_array )

	DECLARE no_globals , loop , id , item_has_changed

	no_globals = size_of_array ( original_array )
	loop = 0
	item_has_changed = FALSE

	WHILE loop < no_globals DO
		loop = loop + 1

		IF loop = 39 THEN
			dummy = 1
		ENDIF

	        { Compare with original value, if same do not save }

		IF NOT is_same ( loop ) THEN
			item_has_changed = TRUE
			IF ( context = "user" ) OR
		 	   ( context = "any_user" ) THEN
				write_user_global ( globals_array ,
						    loop          ,
						    context_id    )
			ELSEIF context = "group" THEN
				write_group_global ( globals_array ,
					 	     loop          ,
						     context_id    )
			ELSEIF context = "system" THEN
				write_system_global ( globals_array ,
						      loop          )
			ENDIF
		ENDIF

	ENDWHILE

	{ Activate the configuration items }

	IF item_has_changed THEN
		global_flush ( )
	ENDIF
ENDROUTINE

{*******************************************************************************
*
* Writes any globals modified in the config item spreadsheet to the config_item
* table.
*
*******************************************************************************}

GLOBAL ROUTINE save_item_grid ( context    ,
			 	global_id  ,
			 	name_block )

	DECLARE check_ok

	START WRITE TRANSACTION "Item update"
	check_ok = block_row_update ( name_block )
	COMMIT

	check_ok = block_row_release ( name_block )

ENDROUTINE

{*******************************************************************************
*
* Updates global settings for current session
*
*******************************************************************************}

GLOBAL ROUTINE save_process_grid ( config_values )

	DECLARE counter , no_globals

	no_globals = size_of_array ( config_values )
	counter = 0

	WHILE counter < no_globals
		counter = counter + 1
		SET GLOBAL config_values [ counter, 2 ]
			TO config_values [ counter, 3 ]
	ENDWHILE

ENDROUTINE

{*******************************************************************************
*
* Writes globals from the user spreadsheet to the database
*
*******************************************************************************}

ROUTINE write_user_global ( globals_array , VALUE which_row , VALUE user_id )

	DECLARE id

	{ If a user item was changed but context unchanged }

	IF ( globals_array [ loop , 7 ] = "User" ) AND
	   ( globals_array [ loop , 1 ] = "User" ) THEN

	        id = SELECT config_item . identity FOR UPDATE
			WHERE ( identity = globals_array [ which_row , 2 ] )
			AND   ( operator = user_id )

		ASSIGN config_item . value = globals_array [ which_row , 3 ]

		START WRITE TRANSACTION "Write user"
		UPDATE config_item
		COMMIT

	{ If a user item removed and context now group }
	{ or if a user item removed and context now system }

	ELSEIF ( globals_array [ loop , 1 ]   = "User "  ) AND
	       ( ( globals_array [ loop , 7 ] = "Group"  ) OR
	       ( globals_array [ loop , 7 ]   = "System" ) ) THEN

	        id = SELECT config_item . identity FOR UPDATE
			WHERE ( identity = globals_array [ which_row , 2 ] )
			AND   ( operator = user_id )

		START WRITE TRANSACTION "Remove user global"

		DELETE config_item
		COMMIT

	{ If a user item inserted by pressing <Insert> on a group or
	  system item }

	ELSEIF ( ( globals_array [ loop , 1 ] = "System"  ) OR
	       ( globals_array [ loop , 1 ]   = "Group" ) ) AND
	       ( globals_array [ loop , 7 ]   = "User"  )   THEN

		RESERVE ENTRY config_item,  JUSTIFY (
				    PAD ( globals_array[ which_row,2 ],
				    " ", 30 ),
				    "LEFT" ) :
				    JUSTIFY ( PAD ( user_id, " ", 10 ),
				    "LEFT" ):
				    PAD ( " " , " " , 10 )
		ASSIGN config_item . value = globals_array[which_row,3]
		START WRITE TRANSACTION "Add user"
		UPDATE config_item
 		COMMIT

	{ If a system item changed and context now user }
	{ or if a group item changed and context now user }

	ELSEIF ( ( globals_array [ loop , 1 ] = "System" ) OR
	       ( globals_array [ loop , 1 ] = "Group" ) ) AND
	       ( globals_array [ loop , 7 ] = "User" ) THEN

		RESERVE ENTRY config_item , JUSTIFY (
					    PAD ( globals_array[ which_row,2 ],
					    " ", 30 ),
					    "LEFT" ) :
					    JUSTIFY ( PAD ( user_id, " ", 10 ),
					    "LEFT" ):
					    PAD ( " " , " " , 10 )

		ASSIGN config_item . value       = globals_array [ which_row,3 ]
		START WRITE TRANSACTION "Add user"
		UPDATE config_item
		COMMIT

	ENDIF

ENDROUTINE

{*******************************************************************************
*
* Writes globals from the group spreadsheet to the database
*
*******************************************************************************}

ROUTINE write_group_global ( globals_array , VALUE which_row , VALUE group_id )

	DECLARE id, key0, status

	{ If a global item was changed but context unchanged }

	IF ( globals_array [ loop , 7 ] = "Group" ) AND
	   ( globals_array [ loop , 1 ] = "Group" ) THEN

	        id = SELECT config_item . identity FOR UPDATE
			WHERE ( identity = globals_array [ which_row , 2 ] )
			AND   ( group_name = group_id )

		ASSIGN config_item . value = globals_array [ which_row , 3 ]

		START WRITE TRANSACTION "Write group"
		UPDATE config_item
		COMMIT

	{ If a group item removed and context now system }

	ELSEIF ( globals_array [ loop , 1 ] = "Group" )  AND
	       ( globals_array [ loop , 7 ] = "System" ) THEN

	        id = SELECT config_item . identity FOR UPDATE
			WHERE ( identity = globals_array [ which_row , 2 ] )
			AND   ( group_name = group_id )

		START WRITE TRANSACTION "Remove group global"
		DELETE config_item
		COMMIT

	{ If a group item inserted by pressing <Insert> on a system item }

	ELSEIF ( globals_array [ loop , 1 ] = "System" ) AND
	       ( globals_array [ loop , 7 ] = "Group" )  THEN

		key0 = PAD ( globals_array[ which_row,2 ], " ", 30 ) :
		       PAD ( " " , " " , 10 ) :
		       PAD ( group_id, " ", 10 )

 		RESERVE ENTRY config_item , key0, status
		IF ( status = EMPTY ) THEN
			ASSIGN config_item . value = globals_array[which_row,3]
			START WRITE TRANSACTION "Add user"
			UPDATE config_item
		ENDIF
		COMMIT

	{ If a system item changed and context now group }
	{ or if a group item changed and context now user }

	ELSEIF ( globals_array [ loop , 1 ] = "System" ) AND
	       ( globals_array [ loop , 7 ] = "Group" ) THEN

		key0 = PAD ( globals_array[ which_row,2 ], " ", 30 ) :
		       PAD ( " " , " " , 10 ) :
		       PAD ( group_id, " ", 10 )

		RESERVE ENTRY config_item , key0, status
		IF ( status = EMPTY ) THEN
			ASSIGN config_item . value = globals_array[which_row,3]
			START WRITE TRANSACTION "Add group"
			UPDATE config_item
			COMMIT
		ENDIF
	ENDIF

ENDROUTINE

{*******************************************************************************
*
* Writes globals from the system spreadsheet to the database
*
*******************************************************************************}

ROUTINE write_system_global ( globals_array , VALUE which_row )

	DECLARE id

        id = SELECT config_header . identity FOR UPDATE
			WHERE ( identity = globals_array [ which_row , 2 ] )

	ASSIGN config_header . value       = globals_array [ which_row , 3 ]
	ASSIGN config_header . description = globals_array [ which_row , 4 ]
	ASSIGN config_header . data_type   = globals_array [ which_row , 5 ]
	ASSIGN config_header . user_modifiable  = globals_array [ which_row , 6 ]
	ASSIGN config_header . library     = globals_array [ which_row , 8 ]
	ASSIGN config_header . routine     = globals_array [ which_row , 9 ]

	START WRITE TRANSACTION "Write system"
	UPDATE config_header
	COMMIT

ENDROUTINE

{*******************************************************************************
*
* Compares global values before entering spreadsheet with values after leaving
* spreadsheet. If they are the same, returns TRUE. Used to decide whether save
* option needs to do any work to this record.
*
*******************************************************************************}

ROUTINE is_same ( VALUE which_row  )

	DECLARE loop , no_columns

	no_columns = 9
	loop = 1

	WHILE loop < no_columns DO

		loop = loop + 1

		IF globals_array [ which_row , loop ] <>
			original_array [ which_row , loop ] THEN
			RETURN ( FALSE )
		ENDIF

	ENDWHILE

	RETURN ( TRUE )

ENDROUTINE

{*******************************************************************************
*
* Checks whether globals has already been read in, returns TRUE if found
*
*******************************************************************************}

ROUTINE in_list ( the_id , list_size , config_array )

	DECLARE loop

	loop = 0

	WHILE loop < list_size DO

		loop = loop + 1

		IF config_array [ loop , 2 ] = the_id THEN
			RETURN ( TRUE )
		ENDIF

	ENDWHILE

	RETURN ( FALSE )

ENDROUTINE

{*******************************************************************************
*
* Checks whether any configuration items are held for the user in the
* config_item table . Returns TRUE if configuration items found for user.
*
*******************************************************************************}

ROUTINE check_for_user_cnf ( VALUE user_id )

	DECLARE check_id

	check_id = SELECT config_item . identity
		WHERE operator = user_id

	IF check_id = EMPTY THEN
		RETURN ( FALSE )
	ELSE
		RETURN ( TRUE )
	ENDIF

ENDROUTINE

{*******************************************************************************
*
* Checks whether any configuration items are held for the group in the
* config_item table . Returns TRUE if configuration items found for group.
*
*******************************************************************************}

ROUTINE check_for_group_cnf ( VALUE group_id )

	DECLARE check_id

	check_id = SELECT config_item . identity
		WHERE group_name = group_id

	IF check_id = EMPTY THEN
		RETURN ( FALSE )
	ELSE
		RETURN ( TRUE )
	ENDIF

ENDROUTINE

{*******************************************************************************
*
* Checks whether any configuration items are held for the system in the
* config_header table . Returns TRUE if configuration items found for system.
*
*******************************************************************************}

ROUTINE check_for_system_cnf

	DECLARE check_id

	check_id = SELECT config_header . identity
		WHERE identity > ""

	IF check_id = EMPTY THEN
		RETURN ( FALSE )
	ELSE
		RETURN ( TRUE )
	ENDIF

ENDROUTINE

{*******************************************************************************
*
* Sort globals in globals array, so that they are in alphabetic order
*
*******************************************************************************}

ROUTINE sort_globals ( sort_array , last )

	DECLARE first

	first = 1

	quick_sort ( sort_array , first , last )

ENDROUTINE

ROUTINE quick_sort ( sort_array , left , right )
	DECLARE i , j , x , y , no_cols , loop

       	no_cols = 9
	ARRAY y ARRAYSIZE ( no_cols )
	i = left
	j = right
	x = sort_array [ ( left + right ) / 2 , 2 ]

	WHILE i <= j DO

		WHILE ( sort_array [ i , 2 ] < x ) AND ( i < right ) DO
			i = i + 1
		ENDWHILE

		WHILE ( x < sort_array [ j , 2 ] ) AND ( j > left ) DO
			j = j - 1
		ENDWHILE

		IF i <= j THEN
			loop = 0
			WHILE loop < no_cols DO
				loop = loop + 1
      				y [ loop ] = sort_array [ i , loop ]
				sort_array [ i , loop ] = sort_array [ j, loop ]
				sort_array [ j , loop ] = y [ loop ]
			ENDWHILE
	       		i = i + 1
			j = j - 1
		ENDIF

	ENDWHILE

	IF left < j THEN
		quick_sort ( sort_array , left , j )
	ENDIF
	IF i < right THEN
		quick_sort ( sort_array , i , right )
	ENDIF
ENDROUTINE

ROUTINE load_config_array

	ARRAY config_array ARRAYSIZE ( 0 , 9 )

	no_row = 0

	IF ( context = "user" ) OR ( context = "any_user" ) THEN
		read_operator_globals ( no_row , config_array )
	ENDIF

	IF ( context = "user" ) OR ( context = "group" ) OR
	   ( context = "any_user" ) THEN
		read_group_globals ( no_row , config_array )
	ENDIF

	read_system_globals ( no_row , config_array )

	sort_globals ( config_array , no_row )

ENDROUTINE
