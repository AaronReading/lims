{*****************************************************************************}
{                                                                             }
{ IIIIII  QQQQQQ    MM      MM  ********************************************* }
{   II   QQ    QQ   MMMM  MMMM  *                SAMPLEMANAGER              * }
{   II   QQ    QQ   MM MMMM MM  *        INVOICE AND QUOTATION MODULE       * }
{   II   QQ  q QQ   MM  MM  MM  *                                           * }
{   II   QQ   qq    MM      MM  *        � Thermo Electron 2005             * }
{ IIIIII  QQQQQ qq  WW      WW  ********************************************* }
{                                                                             }
{*****************************************************************************}

{*****************************************************************************
*                                                                             
*       MODULE  : IQM_FORM                                                    
*       VERSION : 4.0                                                         
*       AUTHOR  : Thermo Electron                                            
*       DATE    : 08 April 2005                                                 
{   DESCRIPTION : Data formatting routines to support IQM 3.0                 }
{                                                                             }
{*****************************************************************************}
{ Modification History                                                        }
{                                                                             }
{*****************************************************************************
  M01	GJL	06Aug2002 - Added comments & contact fields
  M02	GJL	06Aug2002 - Reordered prompt objects
  M03	GJL	12Aug2002 - Added quote status "R" Reviewed
  M04   GJL	04Nov2002 - Set default charge period as current year
  M05   GJL	04Nov2002 - Added iqm_pricing field
  M06	GJL	18Nov2002 - Show total amount from quote item screen
******************************************************************************}

join standard_library STD_PROMPT


join library $LIB_UTILS

join library IQM_LIB
join library IQM_DEBUG
join library IQM_CNF

SET COMPILE_OPTION DECLARE
ENABLE WINDOWS



{-----------------------------------------------------------------------------}

GLOBAL ROUTINE set_invoice_header (theinvoice , VALUE counter)

declare client, account, order_num , project_id, quote_num, pricing

	theinvoice .header . reference = 
		   tolower (theinvoice.fieldname:" ":strip(theinvoice.entry))

	theinvoice . header . job_name = SELECT sample . job_name
			WHERE id_numeric = theinvoice . sampleset [counter]

	theinvoice . header . client = SELECT sample . customer_id
	theinvoice . header . account = SELECT sample . iqm_account
	theinvoice . header . order_num = SELECT sample . iqm_order_no
	theinvoice . header . project_id = SELECT sample . project_id
	

	IF !BLANK(theinvoice . header . job_name) THEN

		theinvoice . header . quote_num = SELECT job_header . iqm_quote
			WHERE job_name = theinvoice . header . job_name

		theinvoice . header . pricing = SELECT job_header . iqm_pricing

		IF GLOBAL("IQM_GET_DATA_FROM_JOB") THEN
		
			client = SELECT job_header . customer_id
			account = SELECT job_header . iqm_account
			order_num = SELECT job_header . iqm_order_no
			project_id = SELECT job_header . project_id

			IF !blank(client) THEN
				theinvoice . header . client = client
			ENDIF

			IF !blank(account) THEN
				theinvoice . header . account = account
			ENDIF

			IF !blank(order_num) THEN
				theinvoice . header . order_num = order_num
			ENDIF

			IF !blank(project_id) THEN
				theinvoice . header . project_id = project_id
			ENDIF

		ENDIF

	ENDIF

	IF !BLANK(theinvoice . header . project_id) THEN

		IF GLOBAL("IQM_GET_DATA_FROM_PROJECT") THEN
		
			client = SELECT project_info . customer_id
				WHERE project_id = 
					theinvoice . header . project_id

			account = SELECT project_info . iqm_account
			order_num = SELECT project_info . iqm_order_no
			quote_num = SELECT project_info . iqm_quote
			pricing = SELECT project_info . iqm_pricing

			IF !blank(client) THEN
				theinvoice . header . client = client
			ENDIF

			IF !blank(account) THEN
				theinvoice . header . account = account
			ENDIF

			IF !blank(order_num) THEN
				theinvoice . header . order_num = order_num
			ENDIF

			IF !blank(quote_num) THEN
				theinvoice . header . quote_num = quote_num
			ENDIF

			IF !blank(pricing) THEN
				theinvoice . header . pricing = pricing
			ENDIF

		ENDIF

	ENDIF


	IF GLOBAL("IQM_ACCOUNT_BASED") THEN

		theinvoice . header . client = SELECT iqm_accounts . customer_id
			WHERE identity = theinvoice . header . account

	ELSE

		theinvoice . header . account = SELECT customer . iqm_account
			WHERE identity = theinvoice . header . client

	ENDIF


	IF (theinvoice . header . client != "") AND (theinvoice . header . client != EMPTY) THEN

		theinvoice . header . company = SELECT customer . company_name
				WHERE identity = theinvoice . header . client

		theinvoice . header . attention = SELECT customer . inv_contact
		theinvoice . header . company  = SELECT customer . company_name
		theinvoice . header . address1 = SELECT customer . inv_address1
		theinvoice . header . address2 = SELECT customer . inv_address2
		theinvoice . header . address3 = SELECT customer . inv_address3
		theinvoice . header . address4 = SELECT customer . inv_address4
		theinvoice . header . inv_type = SELECT customer . inv_type

		theinvoice . header . xmpt_fee = SELECT customer . exempt_rep_fee
		theinvoice . header . xmpt_min = SELECT customer . exempt_minimum
		theinvoice . header . xmpt_gst = SELECT customer . exempt_gst
		


	ENDIF

	IF GLOBAL ("IQM_DEBUG") AND (index(GLOBAL("IQM_DEBUG_FLAG") , "B") > 0) THEN

		debug_B (theinvoice.header, "First Genereated")

	ENDIF

ENDROUTINE

{-----------------------------------------------------------------------------}
{-----------------------------------------------------------------------------}

GLOBAL ROUTINE generate_files ( theinvoice )


	theinvoice . entry = get_next_temp_filename ()

	write_inv_file ( theinvoice )
	write_fms_file ( theinvoice )
{
	rename_invoice_files ( theinvoice . header . invoice_num , newfile )
}
ENDROUTINE


{-----------------------------------------------------------------------------}

ROUTINE write_inv_file ( theinvoice )

declare running_total, section_total , rtax_total , stax_total , filename
declare anitem , base_samp , base_job , base_proj , ptr , lineout , sampnom , stat


	running_total = 0.00
	section_total = 0.00
	rtax_total    = 0.00
	stax_total    = 0.00

	base_samp = EMPTY
	base_job  = EMPTY
	base_proj = EMPTY

	filename = "$SMP_INVOICES/":theinvoice . entry:".ivf"

	file create filename , stat

	IF stat != EMPTY THEN

		flash_message ("Failed to create invoice file : " : stat , TRUE)

	ENDIF

	file write filename , "Invoiced Charges - Breakdown by job/Sample" 
	file write filename , ""

	file write filename , "Invoice generated for ":theinvoice . header .reference
	file write filename , ""


	IF theinvoice . header . quote_num != "" then

		file write filename , "Based on Quotation : "
			:theinvoice . header . quote_num:" ":theinvoice . header .pricing
		file write filename , ""

	ENDIF


	file write filename , "*****************************************"
	file write filename , "* Customer code  : " : 
					  theinvoice . header  . client:"           *"
	file write filename , "* Purchase order : " : 
						 theinvoice . header  . order_num:" *"
	file write filename , "* Dated of issue : " : 
				leftstring(theinvoice . header .issue_date,17):"    *"
	file write filename , "*****************************************"
	file write filename , ""


	ptr = 1

	anitem = theinvoice . items . get_by_index_number ("entry_order" , ptr)
	
	WHILE anitem != EMPTY DO

		lineout = ""

		IF (anitem . project_id != base_proj) OR
		   (anitem . job_name != base_job) OR
		   (anitem . sample_id != base_samp) THEN

			IF section_total != 0 THEN

				file write filename , 	
				"                          sub-total : $ ":
					section_total : " + " : stax_total

				file write filename , ""

				flag_if_invalid ( filename , 
					  theinvoice . header  , section_total)

			ENDIF

			IF (anitem . project_id != base_proj) THEN

	                        file write filename , "Project Name : " :
        	                        anitem . project_id

				base_proj = anitem . project_id

			ENDIF

			IF (anitem . job_name != base_job) THEN

	                        file write filename , "Job Name : " :
        	                        anitem . job_name

				base_job = anitem . job_name

			ENDIF

			IF (anitem . sample_id != base_samp) THEN

				IF anitem . sample_id != EMPTY THEN

					sampnom = SELECT sample . id_text
						WHERE id_numeric = anitem . sample_id

				ELSE

					sampnom = "-------------------------------"
		
				ENDIF

				file write filename , "Sample Number : " :
					anitem . sample_id :" (":strip(sampnom):")"

				base_samp = anitem . sample_id

			ENDIF

			section_total = 0.00
			stax_total = 0.00

		ENDIF

		IF     anitem . source = "M" then
			
			lineout = "      Miscellaneous Charge"

		ELSEIF anitem . source = "D" then

			lineout = "    Discount : ":anitem.schedule   

		ELSEIF anitem . source = "T" then

			lineout = "    Schedule : ":anitem.schedule

		ELSEIF anitem . source = "P" then

			lineout = "      Preparation : ":anitem.preparation

		ELSEIF anitem . source = "A" then

			lineout = "    Analysis : ":anitem . analysis_id			

		ELSEIF anitem . source = "S" then

			lineout = "    Sample Charge : "

		ELSEIF anitem . source = "J" then

			lineout = "   Job Charge : "

		ELSEIF anitem . source = "R" then

			lineout = "Project Charge : "

		ENDIF


		IF lineout != "" then

			lineout = pad(lineout," ",30) : anitem . cost_centre 

			lineout = pad(lineout," ",40) : anitem . value : " + " :
				anitem . added_tax : " " : anitem . charge_basis :" "
				
			lineout = lineout : strip(anitem . invoice_text)

			file write filename , lineout

			section_total = section_total + anitem . value
			running_total = running_total + anitem . value
			rtax_total = rtax_total + anitem . added_tax
			stax_total = stax_total + anitem . added_tax

		ENDIF

		ptr = ptr + 1

		anitem = theinvoice . items . get_by_index_number ("entry_order", ptr)

	ENDWHILE

	file write filename , "                          sub-total : $ ":
					section_total : " + " : stax_total

	flag_if_invalid ( filename , theinvoice . header  , section_total)

	file write filename , ""

	file write filename , "                      Invoice Total : $ ":
					running_total : " + " : rtax_total

	running_total = running_total + rtax_total

	file write filename , ""

	file write filename , "                      Invoice Total : $ ":
					running_total

	flag_if_invalid ( filename , theinvoice . header  , running_total)

	file close filename

	RETURN 

ENDROUTINE

{-----------------------------------------------------------------------------}

ROUTINE write_fms_file (theinvoice)

declare running_total, section_total , stax_total , rtax_total , filename , stat
declare anitem , base_centre , ptr , lineout , oldsamp , sampnom

	running_total = 0.00
	section_total = 0.00
	rtax_total    = 0.00
	stax_total    = 0.00

	base_centre = EMPTY
	old_samp = ""

	filename = "$SMP_INVOICES/":theinvoice . entry:".fms"

	file create filename , stat

	IF stat != EMPTY THEN

		flash_message ("Failed to create cost allocation file : " : stat , TRUE)

	ENDIF

	file write filename , "Invoice Charges - Activity Centre breakdown"
	file write filename , ""

	file write filename , "Invoice generated for ":theinvoice . header .reference
	file write filename , ""

	IF theinvoice . header .quote_num != "" THEN

		file write filename , "Based on Quotation : ":theinvoice . header .quote_num
		file write filename , " Pricing structure : ":theinvoice . header .pricing
		file write filename , ""

	ENDIF

	ptr = 1

	anitem = theinvoice .items . get_by_index_number ("cost_centre" , ptr)

	IF anitem != EMPTY then

		IF anitem . cost_centre = EMPTY THEN

			file write filename , "Activity Centre : Unassigned" 

		ELSE

			file write filename , "Activity Centre : " :
				anitem . cost_centre

		ENDIF

		base_centre = anitem . cost_centre 

	ENDIF

	WHILE anitem != EMPTY DO

		lineout = ""
		 
		IF (anitem . cost_centre != base_centre) THEN

			IF base_centre = EMPTY then

				base_centre = ""

			ENDIF

			file write filename , "                   " : base_centre :
				"      Total : $ ":section_total : " + " : stax_total

			flag_if_invalid ( filename , theinvoice . header  , section_total)

			file write filename , ""

			file write filename , "Activity Centre : " :
				anitem . cost_centre
			base_centre = anitem . cost_centre 
			section_total = 0.00
			stax_total = 0.00

		ENDIF

		IF anitem . sample_id != oldsamp THEN

			oldsamp = anitem . sample_id

			IF oldsamp != EMPTY THEN

	                        sampnom = SELECT sample . id_text
                                        WHERE id_numeric = anitem . sample_id
				sampnom = " ":strip(sampnom) : "(" : strip(anitem . sample_id) : ")"
			ELSE

				sampnom = ""

			ENDIF

			sampnom = pad (sampnom , " " , 30)

		ENDIF

		IF     anitem . source = "M" THEN

			lineout = "      Miscellaneous Charge"

		ELSEIF anitem . source = "P" THEN

			lineout = "      Prep.  : ":anitem.preparation

		ELSEIF anitem . source = "A" THEN

			lineout = "    Analysis : ":anitem.analysis_id

		ELSEIF anitem . source = "D" THEN

			lineout = "    Discount : ":anitem.schedule   
		
		ELSEIF anitem . source = "T" THEN

			lineout = "    Schedule : ":anitem.schedule

		ELSEIF anitem . source = "S" THEN

			lineout = "    Sample Charge : "

		ELSEIF anitem . source = "J" THEN

			lineout = "    Job Charge :    ":anitem . job_name


		ELSEIF anitem . source = "R" THEN

			lineout = "Project Charge :    ":anitem . project_id

		ENDIF


		if lineout != "" then

			lineout = pad(lineout," ",25) :
		  	   sampnom : anitem . value

			lineout = lineout : " " : anitem . charge_basis

			lineout = lineout : " " : strip(anitem . invoice_text)

			file write filename , lineout

			section_total = section_total + anitem . value
			running_total = running_total + anitem . value
			stax_total = stax_total + anitem . added_tax
			rtax_total = rtax_total + anitem . added_tax

		endif

		ptr = ptr + 1

		anitem = theinvoice . items . get_by_index_number ("cost_centre", ptr)

	endwhile

	file write filename , "                   ":base_centre:"      Total : $ ":
					section_total : " + " : stax_total

	flag_if_invalid ( filename , theinvoice . header  , section_total)

	file write filename , ""

	file write filename , "                           Invoice Total : $ ":
					running_total : " + " : rtax_total

	running_total = running_total + rtax_total

	file write filename , ""

	file write filename , "                           Invoice Total : $ ":
					running_total

	flag_if_invalid ( filename , theinvoice . header  , running_total)

	file close filename

	RETURN

ENDROUTINE

{=============================================================================}

ROUTINE flag_if_invalid ( filename , invheader , VALUE item_total )


        IF round(item_total * 100) < 0 THEN

                IF invheader . is_valid = FALSE THEN

                        flash_message
                    ("another item less than zero - review file!" , true)

                ELSE
                        flash_message
                    ("Invalid - an item value less than zero - review file!", 
                           true)

                ENDIF

		file write filename, ""
		file write filename , 
	">>>  * INVALID INVOICE! * Item value is less than zero - not permitted"

		invheader . is_valid = FALSE

        ENDIF

ENDROUTINE

{-----------------------------------------------------------------------------}

GLOBAL ROUTINE rename_invoice_files (newfile , VALUE invoice_num)

declare afilename , bfilename , stat , outline

	outline = "Issued as invoice ":strip(invoice_num):" on ":NOW

	afilename = "$SMP_INVOICES/":newfile:".ivf"
	bfilename = "$SMP_INVOICES/":invoice_num:".inv"


	FILE EXTEND afilename

	FILE WRITE afilename , ""
	FILE WRITE afilename , outline
	FILE CLOSE afilename


	FILE COPY afilename , bfilename , stat

	IF stat = EMPTY THEN

		FILE DELETE afilename

	ENDIF

	afilename = "$SMP_INVOICES/":newfile:".fms"
	bfilename = "$SMP_INVOICES/":invoice_num:".fms"

	FILE COPY afilename , bfilename , stat

	IF stat = EMPTY THEN

		FILE DELETE afilename

	ENDIF

ENDROUTINE

{-----------------------------------------------------------------------------}

GLOBAL ROUTINE delete_tmp_files (newfile)

declare fname

	fname = "$SMP_INVOICES/" : newfile : ".ivf"

	FILE DELETE fname

	fname = "$SMP_INVOICES/" : newfile : ".fms"

	FILE DELETE fname

ENDROUTINE

{-----------------------------------------------------------------------------}

GLOBAL ROUTINE show_inv_file (ident , nothing , VALUE option)

declare fname

	IF (option = "DISPLAY") OR (option = "MODIFY") THEN

		fname = tolower("$SMP_INVOICES/" : ident : ".inv")

		IF (FILE EXISTS(fname)) THEN

			FILE SEND fname , "DISPLAY/"

		ELSE

			flash_message ("File cannot be found" , TRUE)

		ENDIF

	ENDIF

ENDROUTINE

{-----------------------------------------------------------------------------}

GLOBAL ROUTINE show_trans_file (ident , nothing , VALUE option)

declare fname

	IF (option = "DISPLAY") OR (option = "MODIFY") THEN

		fname = tolower("$SMP_INVOICES/" : ident : ".fms")

		IF (FILE EXISTS(fname)) THEN

			FILE SEND fname , "DISPLAY/"

		ELSE

			flash_message ("File cannot be found" , TRUE)

		ENDIF

	ENDIF

ENDROUTINE

{-----------------------------------------------------------------------------}

GLOBAL ROUTINE set_quote_header (thequote)

declare status, amount, val, gst_exempt

	IF thequote . mode = "Add" then

		thequote . entry = ""
                { M02 GJL 06Aug2002 - Reordered prompt objects }
                SET DATE FORMAT "DD-Mon-YYYY"		
		thequote . mainform . prompt_objects[10]  . set_text (NOW)   { M04 GJL 04Nov2002 }
		RESTORE DATE FORMAT
		RETURN

	ELSEIF thequote . mode = "Modify" THEN

		thequote . entry = SELECT iqm_quotes . identity FOR UPDATE
					WHERE identity = thequote . entry

	ELSE

		thequote . entry = SELECT iqm_quotes . identity
					WHERE identity = thequote . entry

	ENDIF

	{ M06 GJL 18Nov2002 - Show total amount from quote item screen } 	
	val = SELECT iqm_quote_item . value
			WHERE iqm_quote = thequote . entry
			
	amount = 0
			
	WHILE val <> EMPTY DO
	
	   amount = amount + val
	   NEXT iqm_quote_item
	   val = SELECT iqm_quote_item . value
	
	ENDWHILE

	thequote . mainform . display_objects[1] . set_text (leftstring(SELECT iqm_quotes . mod_on ,17 ))
	thequote . mainform . display_objects[2] . set_text (SELECT iqm_quotes . mod_by)
	thequote . mainform . display_objects[3] . set_text (SELECT iqm_quotes . status)
        gst_exempt = SELECT iqm_quotes . gst_exempt
        IF gst_exempt THEN
            gst_exempt = "Yes"
        ELSE
            gst_exempt = "No"
        ENDIF
	thequote . mainform . display_objects[4] . set_text ( gst_exempt )
	thequote . mainform . display_objects[5] . set_text (SELECT iqm_quotes . iqm_account)
	thequote . mainform . display_objects[6] . set_text (thequote . entry)
	{thequote . mainform . display_objects[6] . user_info = thequote . entry}


	thequote . mainform . prompt_objects[1]  . set_text (SELECT iqm_quotes . description)
	thequote . mainform . prompt_objects[2]  . set_text (SELECT iqm_quotes . customer_id)
	thequote . mainform . prompt_objects[3]  . set_text (SELECT iqm_quotes . iqm_contact_name){ M01 GJL 06Aug2002 - Added comments & contact fields }
	thequote . mainform . prompt_objects[4]  . set_text (SELECT iqm_quotes . iqm_pricing)    { M05 GJL 04Nov2002 }
	thequote . mainform . prompt_objects[5]  . set_text (leftstring(SELECT iqm_quotes . date_issued , 17))
	thequote . mainform . prompt_objects[6]  . set_text (SELECT iqm_quotes . fixed)
	thequote . mainform . prompt_objects[7]  . set_text (SELECT iqm_quotes . quote_value)
	thequote . mainform . prompt_objects[8]  . set_text (SELECT iqm_quotes . per_sample)
	thequote . mainform . prompt_objects[9]  . set_text (leftstring(SELECT iqm_quotes . contract_date , 17))	
	thequote . mainform . prompt_objects[10] . set_text (leftstring(SELECT iqm_quotes . expiry_date , 17))
	thequote . mainform . prompt_objects[11] . set_text (SELECT iqm_quotes . period_id)
	thequote . mainform . prompt_objects[12] . set_text (SELECT iqm_quotes . gst_inclusive)
	thequote . mainform . prompt_objects[13] . set_text (SELECT iqm_quotes . group_id)
	thequote . mainform . prompt_objects[14] . set_text (SELECT iqm_quotes . iqm_comments)	{ M01 GJL 06Aug2002 - Added comments & contact fields }

	IF thequote . mainform . display_objects[3] . value = "I" THEN 

		thequote . mainform . display_objects[3] . foreground_colour = PROMPT_COLOUR_YELLOW
		
	{ M03 GJL 12Aug2002 - Added quote status "R" Reviewed }

	ELSEIF thequote . mainform . display_objects[3] . value = "R" THEN 

		thequote . mainform . display_objects[3] . foreground_colour = PROMPT_COLOUR_MAGENTA

	{ END M03 GJL 12Aug2002 - Added quote status "R" Reviewed }

	ELSEIF thequote . mainform . display_objects[3] . value = "A" THEN 

		thequote . mainform . display_objects[3] . foreground_colour = PROMPT_COLOUR_BLUE

	ELSEIF (thequote . mainform . display_objects[3] . value = "X") OR
		(thequote . mainform . display_objects[3] . value = "L") THEN 

		thequote . mainform . display_objects[3] . foreground_colour = PROMPT_COLOUR_RED

	ELSE

		thequote . mainform . display_objects[3] . foreground_colour = PROMPT_COLOUR_CYAN

	ENDIF

	status = SELECT phrase . phrase_text
		WHERE (phrase_type = "IQM_QUSTAT")
		AND (phrase_id = thequote . mainform . display_objects[3] . value)

	thequote . mainform . display_objects[3] . set_text(status)
	
	thequote . fixed                                       = thequote . mainform . prompt_objects[6] . value		{ M02 GJL 06Aug2002 - Reordered prompt objects }
	thequote . mainform . prompt_objects[7] . display_only = !thequote . fixed
	thequote . mainform . prompt_objects[8] . display_only = !thequote . fixed

	IF (thequote . mode = "Display") THEN

		thequote . mainform . prompt_objects[1] . display_only = TRUE
		thequote . mainform . prompt_objects[2] . display_only = TRUE
		thequote . mainform . prompt_objects[3] . display_only = TRUE
		thequote . mainform . prompt_objects[4] . display_only = TRUE		{ M05 GJL 04Nov2002 }
		thequote . mainform . prompt_objects[5] . display_only = TRUE
		thequote . mainform . prompt_objects[6] . display_only = TRUE
		thequote . mainform . prompt_objects[7] . display_only = TRUE
		thequote . mainform . prompt_objects[8] . display_only = TRUE
		thequote . mainform . prompt_objects[9] . display_only = TRUE
		thequote . mainform . prompt_objects[10] . display_only = TRUE
		thequote . mainform . prompt_objects[11] . display_only = TRUE
		thequote . mainform . prompt_objects[12] . display_only = TRUE
		thequote . mainform . prompt_objects[13] . display_only = TRUE{ M01 GJL 06Aug2002 - Added comments & contact fields }
		thequote . mainform . prompt_objects[14] . display_only = TRUE

	ENDIF

ENDROUTINE

